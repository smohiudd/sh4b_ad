#define Nominal_cxx
#define PY_SSIZE_T_CLEAN
#include <python3.9/Python.h>

// A.X.: set the trigger list
#define trig13167

#include "Nominal.h"
#include <TH2.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <TSystemDirectory.h>
#include <TSystem.h>

#include <sstream>
#include <stdlib.h>

//********* Definition section *********
void Nominal::Begin(TTree * /*tree*/)
{  
  // Create all the histograms and set up features
  TString option = GetOption();

  //********* Initialisation section *********
 
  lj1_mass = -999.;
  lj1_pT = -999.;
  lj1_phi = -999.;
  lj1_eta = -999.;
  lj2_mass = -999.;
  lj2_pT = -999.;
  lj2_phi = -999.;
  lj2_eta = -999.;
  lj1_truthS_mass = -999.;
  lj1_truthS_pt = -999.;
  lj1_truthS_phi = -999.;
  lj1_truthS_eta = -999.;
  lj2_truthS_mass = -999.;
  lj2_truthS_pt = -999.;
  lj2_truthS_phi = -999.;
  lj2_truthS_eta = -999.;
  lj1_truthH_mass = -999.;
  lj1_truthH_pt = -999.;
  lj1_truthH_phi = -999.;
  lj1_truthH_eta = -999.;
  lj2_truthH_mass = -999.;
  lj2_truthH_pt = -999.;
  lj2_truthH_phi = -999.;
  lj2_truthH_eta = -999.;
  
  dR_truthS_reco_lrj = -1;
  dR_truthH_reco_lrj = -1;
   rapidity_SH = -999.;
   DR_SH = -1.;//id_truthS_reco_lrj = -1;
   DPhi_SH = -999.;//id_truthS_reco_lrj = -1;
   DEta_SH = -999.;
   Ratio_2m_pt = -999.;
/*
  bool is2015 = *RunNumber>= 276262 && *RunNumber<=284484;
  bool is2016 = *RunNumber>= 296939 && *RunNumber<=311481;
  bool is2017 = *RunNumber>= 324320 && *RunNumber<=341649;
  bool is2018 = *RunNumber>= 348197;
*/
 
  Jet_mass = makeHist("Jet_mass",100,10.,1500.);
  Jet_pt = makeHist("Jet_pt",800,20.,1000.);
  Jet_eta = makeHist("Jet_eta",60,-3.,3.);
  Jet_phi = makeHist("Jet_phi",80,-4.,4.);
  
  Xcand_mass_b77 = makeHist("Xcand_mass_b77",550,100.,6500.);

  leadjetMass_beforeTrig_pT250_M100 = makeHist("leadjetMass_beforeTrig_pT250_M100",100,20.,1000.);
  leadjetPt_beforeTrig_pT250_M100 = makeHist("leadjetPt_beforeTrig_pT250_M100",325,250.,3500.);

  leadjetMass_beforeTrig_pT500_M40 = makeHist("leadjetMass_beforeTrig_pT500_M40",100,20.,1000.);
  leadjetPt_beforeTrig_pT500_M40 = makeHist("leadjetPt_beforeTrig_pT500_M40",325,250.,3500.);

  leadjetMass_afterTrig_pT250_M100 = makeHist("leadjetMass_afterTrig_pT250_M100",100,20.,1000.);
  leadjetPt_afterTrig_pT250_M100 = makeHist("leadjetPt_afterTrig_pT250_M100",325,250.,3500.);
  leadjetMass_afterTrig390_pT250_M100 = makeHist("leadjetMass_afterTrig390_pT250_M100",100,20.,1000.);
  leadjetPt_afterTrig390_pT250_M100 = makeHist("leadjetPt_afterTrig390_pT250_M100",325,250.,3500.);
  
  leadjetMass_afterTrig_pT500_M40 = makeHist("leadjetMass_afterTrig_pT500_M50",100,20.,1000.);
  leadjetPt_afterTrig_pT500_M40 = makeHist("leadjetPt_afterTrig_pT500_M40",325,250.,3500.);
  leadjetMass_afterTrig390_pT500_M40 = makeHist("leadjetMass_afterTrig390_pT500_M50",100,20.,1000.);
  leadjetPt_afterTrig390_pT500_M40 = makeHist("leadjetPt_afterTrig390_pT500_M40",325,250.,3500.);
  subleadjetMass_beforeTrig = makeHist("subleadjetMass_beforeTrig",100,20.,1000.);
  subleadjetPt_beforeTrig = makeHist("subleadjetPt_beforeTrig",325,250.,3500.);
  mJJ_beforeTrig_pT450_M50 = makeHist("mJJ_beforeTrig_pT450_M50",600,500.,6500.);
  mJJ_afterTrig420_L1J100_pT450_M50 = makeHist("mJJ_afterTrig420_L1J100_pT450_M50",600,500.,6500.);
  mJJ_afterTrig390_L1J100_pT450_M50 = makeHist("mJJ_afterTrig390_L1J100_pT450_M50",600,500.,6500.);
  mJJ_afterTrig420_L1SC111_pT450_M50 = makeHist("mJJ_afterTrig420_L1SC111_pT450_M50",600,500.,6500.);
  mJJ_afterTrig460_a10r_L1SC111_pT450_M50 = makeHist("mJJ_afterTrig460_a10r_L1SC111_pT450_M50",600,500.,6500.);
  mJJ_afterTrig460_a10r_L1J100_pT450_M50 = makeHist("mJJ_afterTrig460_a10r_L1J100_pT450_M50",600,500.,6500.);
  mJJ_afterTrig460_a10_subjes_L1SC111_pT450_M50 = makeHist("mJJ_afterTrig460_a10_subjes_L1SC111_pT450_M50",600,500.,6500.);
  mJJ_afterTrig460_a10_subjes_L1J100_pT450_M50 = makeHist("mJJ_afterTrig460_a10_subjes_L1J100_pT450_M50",600,500.,6500.);
  mJJ_afterTrig460_a10t_L1SC111_pT450_M50 = makeHist("mJJ_afterTrig460_a10t_L1SC111_pT450_M50",600,500.,6500.);
  mJJ_afterTrig460_a10t_L1J100_pT450_M50 = makeHist("mJJ_afterTrig460_a10t_L1J100_pT450_M50",600,500.,6500.);
 //After kinematic cuts for large-R jets 
  truthleadfatjet_mass = makeHist("truthleadfatjet_mass",475,50.,1000.);
  truthleadfatjet_pT = makeHist("truthleadfatjet_pT",305,450.,3500.);
  truthleadfatjet_eta = makeHist("truthleadfatjet_eta",40,-2.,2.);
  truthleadfatjet_phi = makeHist("truthleadfatjet_phi",80,-M_PI,M_PI);

  truthsubleadfatjet_mass = makeHist("truthsubleadfatjet_mass",475,50.,1000.);
  truthsubleadfatjet_pT = makeHist("truthsubleadfatjet_pT",325,250.,3500.);
  truthsubleadfatjet_eta = makeHist("truthsubleadfatjet_eta",40,-2.,2.);
  truthsubleadfatjet_phi = makeHist("truthsubleadfatjet_phi",80,-M_PI,M_PI);

  LRJet1_mass = makeHist("LRJet1_mass",475,50.,1000.);
  LRJet1_pt = makeHist("LRJet1_pt",305,450.,3500.);
  LRJet1_eta = makeHist("LRJet1_eta",40,-2.,2.);
  LRJet1_phi = makeHist("LRJet1_phi",80,-M_PI,M_PI);
  
  LRJet2_mass = makeHist("LRJet2_mass",475,50.,1000.);
  LRJet2_pt = makeHist("LRJet2_pt",325,250.,3500.);
  LRJet2_eta = makeHist("LRJet2_eta",40,-2.,2.);
  LRJet2_phi = makeHist("LRJet2_phi",80,-M_PI,M_PI);
  
  Xcand_mass = makeHist("Xcand_mass",390,550.,6500.);
  Xcand_pT = makeHist("Xcand_pT",300,50.,3500.);
  Xcand_eta = makeHist("Xcand_eta",60,-3.,3.);
  Xcand_phi = makeHist("Xcand_phi",60,-M_PI,M_PI);
  Xcand_rapidity = makeHist("Xcand_rapidity",60,-M_PI,M_PI);
  
  //After b-tagging without S and H assignment
  LRJet1_mass_btag = makeHist("LRJet1_mass_btag",475,50.,1000.);
  LRJet1_pt_btag = makeHist("LRJet1_pt_btag",305,450.,3500.);
  LRJet1_eta_btag = makeHist("LRJet1_eta_btag",40,-2.,2.);
  LRJet1_phi_btag = makeHist("LRJet1_phi_btag",80,-M_PI,M_PI);
  
  LRJet2_mass_btag = makeHist("LRJet2_mass_btag",475,50.,1000.);
  LRJet2_pt_btag = makeHist("LRJet2_pt_btag",325,250.,3500.);
  LRJet2_eta_btag = makeHist("LRJet2_eta_btag",40,-2.,2.);
  LRJet2_phi_btag = makeHist("LRJet2_phi_btag",80,-M_PI,M_PI);
  
  LRJet1mass_vs_LRJet2mass_btag = make2DHist("LRJet1mass_vs_LRJet2mass_btag",750,50.,800.,750,50.,800.);
  Xcand_mass_btag = makeHist("Xcand_mass_btag",550,100.,6500.);
  Xcand_pT_btag = makeHist("Xcand_pT_btag",300,50.,3500.);
  Xcand_eta_btag = makeHist("Xcand_eta_btag",60,-3.,3.);
  Xcand_phi_btag = makeHist("Xcand_phi_btag",60,-M_PI,M_PI);
  ratio_2m_pt_btag = makeHist("ratio_2m_pt_btag",60,0,3.);
  
  
  //After assigning S and H candidates without b-tagging
  Scand_mass = makeHist("Scand_mass",145,50.,1500.);
  Scand_pT = makeHist("Scand_pT",305,450.,3500.);
  Scand_eta = makeHist("Scand_eta",40,-2.,2.);
  Scand_phi = makeHist("Scand_phi",80,-M_PI,M_PI);
  Hcand_mass = makeHist("Hcand_mass",200,50.,250.);
  Hcand_pT = makeHist("Hcand_pT",305,450.,3500.);
  Hcand_eta = makeHist("Hcand_eta",40,-2.,2.);
  Hcand_phi = makeHist("Hcand_phi",80,-M_PI,M_PI);
  Xcand_mass_afterSorH = makeHist("Xcand_mass_afterSorH",550,100.,6500.);
  Xcand_pT_afterSorH = makeHist("Xcand_pT_afterSorH",300,50.,3500.);
  Xcand_eta_afterSorH = makeHist("Xcand_eta_afterSorH",60,-3.,3.);
  Xcand_phi_afterSorH = makeHist("Xcand_phi_afterSorH",60,-M_PI,M_PI);
  mHvsmX = make2DHist("mHvsmX",550,100.,6500.,200,50.,250.);
  mSvsmX = make2DHist("mSvsmX",550,100.,6500.,145,50.,1500.);
  mSvsmH = make2DHist("mSvsmH",950,50.,1000.,250,50.,300.);
  dR_SH_afterSorH = makeHist("dR_SH_afterSorH",60,0.,6.);
  dRapidity_SH_afterSorH = makeHist("dRapidity_SH_afterSorH",60,0.,6.);
  dEta_SH_afterSorH = makeHist("dEta_SH_afterSorH",60,0.,3.);
  dPhi_SH_afterSorH = makeHist("dPhi_SH_afterSorH",60,0.,M_PI);
  dR_SHvsmS = make2DHist("dR_SHvsmS",60,0.,6.,145,50.,1500.);
  dR_SHvsmH = make2DHist("dR_SHvsmH",60,0.,6.,200,50.,250.);
  dEta_SHvsmS = make2DHist("dEta_SHvsmS",60,0.,3.,145,50.,1500.);
  dEta_SHvsmH = make2DHist("dEta_SHvsmH",60,0.,3.,200,50.,250.);
  dPhi_SHvsmS = make2DHist("dPhi_SHvsmS",60,0.,M_PI,145,50.,1500.);
  dPhi_SHvsmH = make2DHist("dPhi_SHvsmH",60,0.,M_PI,200,50.,250.);
  dRapidity_SHvsmS = make2DHist("dRapidity_SHvsmS",60,0.,6.,145,50.,1500.);
  dRapidity_SHvsmH = make2DHist("dRapidity_SHvsmH",60,0.,6.,200,50.,250.);
  ratio_2m_pt_afterSorH = makeHist("ratio_2m_pt_afterSorH",60,0,3.);

  //After assigning S and H candidates with 2B region
  Scand_mass_2b = makeHist("Scand_mass_2b",145,50.,1500.);
  Scand_pT_2b = makeHist("Scand_pT_2b",305,450.,3500.);
  Scand_eta_2b = makeHist("Scand_eta_2b",40,-2.,2.);
  Scand_phi_2b = makeHist("Scand_phi_2b",80,-M_PI,M_PI);
  Scand_lrjlabel = makeHist("Scand_lrjlabel",11,0,11);
  Hcand_mass_2b = makeHist("Hcand_mass_2b",200,50.,250.);
  Hcand_pT_2b = makeHist("Hcand_pT_2b",305,450.,3500.);
  Hcand_eta_2b = makeHist("Hcand_eta_2b",40,-2.,2.);
  Hcand_phi_2b = makeHist("Hcand_phi_2b",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_2b = makeHist("Xcand_mass_afterSorH_2b",550,100.,6500.);
  Xcand_pT_afterSorH_2b = makeHist("Xcand_pT_afterSorH_2b",300,50.,3500.);
  Xcand_eta_afterSorH_2b = makeHist("Xcand_eta_afterSorH_2b",60,-3.,3.);
  Xcand_phi_afterSorH_2b = makeHist("Xcand_phi_afterSorH_2b",60,-M_PI,M_PI);
  mHvsmX_2b = make2DHist("mHvsmX_2b",550,100.,6500.,200,50.,250.);
  mSvsmX_2b = make2DHist("mSvsmX_2b",550,100.,6500.,145,50.,1500.);
  mSvsmH_2b = make2DHist("mSvsmH_2b",950,50.,1000.,250,50.,300.);
  dR_SH_afterSorH_2b = makeHist("dR_SH_afterSorH_2b",60,0.,6.);
  dRapidity_SH_afterSorH_2b = makeHist("dRapidity_SH_afterSorH_2b",60,0.,6.);
  dEta_SH_afterSorH_2b = makeHist("dEta_SH_afterSorH_2b",60,0.,3.);
  dPhi_SH_afterSorH_2b = makeHist("dPhi_SH_afterSorH_2b",60,0.,M_PI);
  dR_SHvsmS_2b = make2DHist("dR_SHvsmS_2b",60,0.,6.,145,50.,1500.);
  dR_SHvsmH_2b = make2DHist("dR_SHvsmH_2b",60,0.,6.,200,50.,250.);
  dEta_SHvsmS_2b = make2DHist("dEta_SHvsmS_2b",60,0.,3.,145,50.,1500.);
  dEta_SHvsmH_2b = make2DHist("dEta_SHvsmH_2b",60,0.,3.,200,50.,250.);
  dPhi_SHvsmS_2b = make2DHist("dPhi_SHvsmS_2b",60,0.,M_PI,145,50.,1500.);
  dPhi_SHvsmH_2b = make2DHist("dPhi_SHvsmH_2b",60,0.,M_PI,200,50.,250.);
  dRapidity_SHvsmS_2b = make2DHist("dRapidity_SHvsmS_2b",60,0.,6.,145,50.,1500.);
  dRapidity_SHvsmH_2b = make2DHist("dRapidity_SHvsmH_2b",60,0.,6.,200,50.,250.);
  //After assigning S and H candidates with 4B region
  Scand_mass_4b = makeHist("Scand_mass_4b",145,50.,1500.);
  Scand_pT_4b = makeHist("Scand_pT_4b",305,450.,3500.);
  Scand_eta_4b = makeHist("Scand_eta_4b",40,-2.,2.);
  Scand_phi_4b = makeHist("Scand_phi_4b",80,-M_PI,M_PI);
  Hcand_mass_4b = makeHist("Hcand_mass_4b",200,50.,250.);
  Hcand_pT_4b = makeHist("Hcand_pT_4b",305,450.,3500.);
  Hcand_eta_4b = makeHist("Hcand_eta_4b",40,-2.,2.);
  Hcand_phi_4b = makeHist("Hcand_phi_4b",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_4b = makeHist("Xcand_mass_afterSorH_4b",550,100.,6500.);
  Xcand_pT_afterSorH_4b = makeHist("Xcand_pT_afterSorH_4b",300,50.,3500.);
  Xcand_eta_afterSorH_4b = makeHist("Xcand_eta_afterSorH_4b",60,-3.,3.);
  Xcand_phi_afterSorH_4b = makeHist("Xcand_phi_afterSorH_4b",60,-M_PI,M_PI);
  mHvsmX_4b = make2DHist("mHvsmX_4b",550,100.,6500.,200,50.,250.);
  mSvsmX_4b = make2DHist("mSvsmX_4b",550,100.,6500.,145,50.,1500.);
  mSvsmH_4b = make2DHist("mSvsmH_4b",950,50.,1000.,250,50.,300.);
  dR_SH_afterSorH_4b = makeHist("dR_SH_afterSorH_4b",60,0.,6.);
  dRapidity_SH_afterSorH_4b = makeHist("dRapidity_SH_afterSorH_4b",60,0.,6.);
  dEta_SH_afterSorH_4b = makeHist("dEta_SH_afterSorH_4b",60,0.,3.);
  dPhi_SH_afterSorH_4b = makeHist("dPhi_SH_afterSorH_4b",60,0.,M_PI);
  dR_SHvsmS_4b = make2DHist("dR_SHvsmS_4b",60,0.,6.,145,50.,1500.);
  dR_SHvsmH_4b = make2DHist("dR_SHvsmH_4b",60,0.,6.,200,50.,250.);
  dEta_SHvsmS_4b = make2DHist("dEta_SHvsmS_4b",60,0.,3.,145,50.,1500.);
  dEta_SHvsmH_4b = make2DHist("dEta_SHvsmH_4b",60,0.,3.,200,50.,250.);
  dPhi_SHvsmS_4b = make2DHist("dPhi_SHvsmS_4b",60,0.,M_PI,145,50.,1500.);
  dPhi_SHvsmH_4b = make2DHist("dPhi_SHvsmH_4b",60,0.,M_PI,200,50.,250.);
  dRapidity_SHvsmS_4b = make2DHist("dRapidity_SHvsmS_4b",60,0.,6.,145,50.,1500.);
  dRapidity_SHvsmH_4b = make2DHist("dRapidity_SHvsmH_4b",60,0.,6.,200,50.,250.);
  ratio_2m_pt_afterSorH_4b = makeHist("ratio_2m_pt_afterSorH_4b",60,0,3.);
  Num_largeRj_ufo = makeHist("Num_largeRj_ufo",8,-1.0,7.0);
  Num_largeRj = makeHist("Num_largeRj",8,-1.0,7.0);
  Num_smallRj = makeHist("Num_smallRj",16,-1.0,15.0);
  Num_largeRj_2btag_77 = makeHist("Num_largeRj_2btag_77",8,-1.0,7.0);
  Num_largeRj_2btag_85 = makeHist("Num_largeRj_2btag_85",8,-1.0,7.0);
  Num_largeRj_2btag_85_77 = makeHist("Num_largeRj_2btag_85_77",8,-1.0,7.0);
  Num_largeRj_ufo_2btag_77 = makeHist("Num_largeRj_ufo_2btag_77",8,-1.0,7.0);
  Num_largeRj_ufo_2btag_85 = makeHist("Num_largeRj_ufo_2btag_85",8,-1.0,7.0);
  Num_smallRj_b_77 = makeHist("Num_smallRj_b_77",16,-1.0,15.0);
  Num_smallRj_b_85 = makeHist("Num_smallRj_b_85",16,-1.0,15.0);
  Num_smallRj_b_GN1_70 = makeHist("Num_smallRj_b_GN1_70",16,-1.0,15.0);
  Num_smallRj_b_GN1_77 = makeHist("Num_smallRj_b_GN1_77",16,-1.0,15.0);
  Num_smallRj_b_GN1_85 = makeHist("Num_smallRj_b_GN1_85",16,-1.0,15.0);
  Num_VRjets = makeHist("Num_VRjets",5,0.0,5.0);
  Num_VRjets_ufo = makeHist("Num_VRjets_ufo",5,0.0,5.0);
  Num_bjets_Higgs = makeHist("Num_bjets_Higgs",5,0.0,5.0);
  Num_bjets_S = makeHist("Num_bjets_S",5,0.0,5.0);
  Num_VRjets_DL1r_77 = makeHist("Num_VRjets_DL1r_77",5,0.0,5.0);
  Num_VRjets_DL1r_85 = makeHist("Num_VRjets_DL1r_85",5,0.0,5.0);
  Num_VRjets_DL1r_77_ufo = makeHist("Num_VRjets_DL1r_77_ufo",5,0.0,5.0);
  Num_VRjets_DL1r_85_ufo = makeHist("Num_VRjets_DL1r_85_ufo",5,0.0,5.0);
  //After Deta cut ;assigning S and H candidates with 4B region
  Scand_mass_deta_4b_SR1 = makeHist("Scand_mass_deta_4b_SR1",145,50.,1500.);
  Scand_pT_deta_4b_SR1 = makeHist("Scand_pT_deta_4b_SR1",305,450.,3500.);
  Scand_eta_deta_4b_SR1 = makeHist("Scand_eta_deta_4b_SR1",40,-2.,2.);
  Scand_phi_deta_4b_SR1 = makeHist("Scand_phi_deta_4b_SR1",80,-M_PI,M_PI);
  Hcand_mass_deta_4b_SR1 = makeHist("Hcand_mass_deta_4b_SR1",200,50.,250.);
  Hcand_pT_deta_4b_SR1 = makeHist("Hcand_pT_deta_4b_SR1",305,450.,3500.);
  Hcand_eta_deta_4b_SR1 = makeHist("Hcand_eta_deta_4b_SR1",40,-2.,2.);
  Hcand_phi_deta_4b_SR1 = makeHist("Hcand_phi_deta_4b_SR1",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_deta_4b_SR1 = makeHist("Xcand_mass_afterSorH_deta_4b_SR1",550,100.,6500.);
  Xcand_pT_afterSorH_deta_4b_SR1 = makeHist("Xcand_pT_afterSorH_deta_4b_SR1",300,50.,3500.);
  Xcand_eta_afterSorH_deta_4b_SR1 = makeHist("Xcand_eta_afterSorH_deta_4b_SR1",60,-3.,3.);
  Xcand_phi_afterSorH_deta_4b_SR1 = makeHist("Xcand_phi_afterSorH_deta_4b_SR1",60,-M_PI,M_PI);
  mHvsmX_deta_4b_SR1 = make2DHist("mHvsmX_deta_4b_SR1",550,100.,6500.,200,50.,250.);
  mSvsmX_deta_4b_SR1 = make2DHist("mSvsmX_deta_4b_SR1",550,100.,6500.,145,50.,1500.);
  mSvsmH_deta_4b_SR1 = make2DHist("mSvsmH_deta_4b_SR1",950,50.,1000.,250,50.,300.);
  dR_SH_afterSorH_deta_4b_SR1 = makeHist("dR_SH_afterSorH_deta_4b_SR1",60,0.,6.);
  dRapidity_SH_afterSorH_deta_4b_SR1 = makeHist("dRapidity_SH_afterSorH_deta_4b_SR1",60,0.,6.);
  dEta_SH_afterSorH_deta_4b_SR1 = makeHist("dEta_SH_afterSorH_deta_4b_SR1",60,0.,3.);
  dPhi_SH_afterSorH_deta_4b_SR1 = makeHist("dPhi_SH_afterSorH_deta_4b_SR1",60,0.,M_PI);
  //After CR, VR regions using method 1 which is similar to CMS
  Scand_mass_CR_FP = makeHist("Scand_mass_CR_FP",145,50.,1500.);
  Scand_pT_CR_FP = makeHist("Scand_pT_CR_FP",305,450.,3500.);
  Scand_eta_CR_FP = makeHist("Scand_eta_CR_FP",40,-2.,2.);
  Scand_phi_CR_FP = makeHist("Scand_phi_CR_FP",80,-M_PI,M_PI);
  Hcand_mass_CR_FP = makeHist("Hcand_mass_CR_FP",200,50.,250.);
  Hcand_pT_CR_FP = makeHist("Hcand_pT_CR_FP",305,450.,3500.);
  Hcand_eta_CR_FP = makeHist("Hcand_eta_CR_FP",40,-2.,2.);
  Hcand_phi_CR_FP = makeHist("Hcand_phi_CR_FP",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_CR_FP = makeHist("Xcand_mass_afterSorH_CR_FP",550,100.,6500.);
  Xcand_pT_afterSorH_CR_FP = makeHist("Xcand_pT_afterSorH_CR_FP",300,50.,3500.);
  Xcand_eta_afterSorH_CR_FP = makeHist("Xcand_eta_afterSorH_CR_FP",60,-3.,3.);
  Xcand_phi_afterSorH_CR_FP = makeHist("Xcand_phi_afterSorH_CR_FP",60,-M_PI,M_PI);
  mHvsmX_CR_FP = make2DHist("mHvsmX_CR_FP",550,100.,6500.,200,50.,250.);
  mSvsmX_CR_FP = make2DHist("mSvsmX_CR_FP",550,100.,6500.,145,50.,1500.);
  mSvsmH_CR_FP = make2DHist("mSvsmH_CR_FP",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 1 which is similar to CMS
  Scand_mass_CR_PF = makeHist("Scand_mass_CR_PF",145,50.,1500.);
  Scand_pT_CR_PF = makeHist("Scand_pT_CR_PF",305,450.,3500.);
  Scand_eta_CR_PF = makeHist("Scand_eta_CR_PF",40,-2.,2.);
  Scand_phi_CR_PF = makeHist("Scand_phi_CR_PF",80,-M_PI,M_PI);
  Hcand_mass_CR_PF = makeHist("Hcand_mass_CR_PF",200,50.,250.);
  Hcand_pT_CR_PF = makeHist("Hcand_pT_CR_PF",305,450.,3500.);
  Hcand_eta_CR_PF = makeHist("Hcand_eta_CR_PF",40,-2.,2.);
  Hcand_phi_CR_PF = makeHist("Hcand_phi_CR_PF",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_CR_PF = makeHist("Xcand_mass_afterSorH_CR_PF",550,100.,6500.);
  Xcand_pT_afterSorH_CR_PF = makeHist("Xcand_pT_afterSorH_CR_PF",300,50.,3500.);
  Xcand_eta_afterSorH_CR_PF = makeHist("Xcand_eta_afterSorH_CR_PF",60,-3.,3.);
  Xcand_phi_afterSorH_CR_PF = makeHist("Xcand_phi_afterSorH_CR_PF",60,-M_PI,M_PI);
  mHvsmX_CR_PF = make2DHist("mHvsmX_CR_PF",550,100.,6500.,200,50.,250.);
  mSvsmX_CR_PF = make2DHist("mSvsmX_CR_PF",550,100.,6500.,145,50.,1500.);
  mSvsmH_CR_PF = make2DHist("mSvsmH_CR_PF",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 1 which is similar to CMS
  Scand_mass_CR_FF = makeHist("Scand_mass_CR_FF",145,50.,1500.);
  Scand_pT_CR_FF = makeHist("Scand_pT_CR_FF",305,450.,3500.);
  Scand_eta_CR_FF = makeHist("Scand_eta_CR_FF",40,-2.,2.);
  Scand_phi_CR_FF = makeHist("Scand_phi_CR_FF",80,-M_PI,M_PI);
  Hcand_mass_CR_FF = makeHist("Hcand_mass_CR_FF",200,50.,250.);
  Hcand_pT_CR_FF = makeHist("Hcand_pT_CR_FF",305,450.,3500.);
  Hcand_eta_CR_FF = makeHist("Hcand_eta_CR_FF",40,-2.,2.);
  Hcand_phi_CR_FF = makeHist("Hcand_phi_CR_FF",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_CR_FF = makeHist("Xcand_mass_afterSorH_CR_FF",550,100.,6500.);
  Xcand_pT_afterSorH_CR_FF = makeHist("Xcand_pT_afterSorH_CR_FF",300,50.,3500.);
  Xcand_eta_afterSorH_CR_FF = makeHist("Xcand_eta_afterSorH_CR_FF",60,-3.,3.);
  Xcand_phi_afterSorH_CR_FF = makeHist("Xcand_phi_afterSorH_CR_FF",60,-M_PI,M_PI);
  mHvsmX_CR_FF = make2DHist("mHvsmX_CR_FF",550,100.,6500.,200,50.,250.);
  mSvsmX_CR_FF = make2DHist("mSvsmX_CR_FF",550,100.,6500.,145,50.,1500.);
  mSvsmH_CR_FF = make2DHist("mSvsmH_CR_FF",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 1 which is similar to CMS
  Scand_mass_VR_FP = makeHist("Scand_mass_VR_FP",145,50.,1500.);
  Scand_pT_VR_FP = makeHist("Scand_pT_VR_FP",305,450.,3500.);
  Scand_eta_VR_FP = makeHist("Scand_eta_VR_FP",40,-2.,2.);
  Scand_phi_VR_FP = makeHist("Scand_phi_VR_FP",80,-M_PI,M_PI);
  Hcand_mass_VR_FP = makeHist("Hcand_mass_VR_FP",200,50.,250.);
  Hcand_pT_VR_FP = makeHist("Hcand_pT_VR_FP",305,450.,3500.);
  Hcand_eta_VR_FP = makeHist("Hcand_eta_VR_FP",40,-2.,2.);
  Hcand_phi_VR_FP = makeHist("Hcand_phi_VR_FP",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_VR_FP = makeHist("Xcand_mass_afterSorH_VR_FP",550,100.,6500.);
  Xcand_pT_afterSorH_VR_FP = makeHist("Xcand_pT_afterSorH_VR_FP",300,50.,3500.);
  Xcand_eta_afterSorH_VR_FP = makeHist("Xcand_eta_afterSorH_VR_FP",60,-3.,3.);
  Xcand_phi_afterSorH_VR_FP = makeHist("Xcand_phi_afterSorH_VR_FP",60,-M_PI,M_PI);
  mHvsmX_VR_FP = make2DHist("mHvsmX_VR_FP",550,100.,6500.,200,50.,250.);
  mSvsmX_VR_FP = make2DHist("mSvsmX_VR_FP",550,100.,6500.,145,50.,1500.);
  mSvsmH_VR_FP = make2DHist("mSvsmH_VR_FP",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 1 which is similar to CMS
  Scand_mass_VR_PF = makeHist("Scand_mass_VR_PF",145,50.,1500.);
  Scand_pT_VR_PF = makeHist("Scand_pT_VR_PF",305,450.,3500.);
  Scand_eta_VR_PF = makeHist("Scand_eta_VR_PF",40,-2.,2.);
  Scand_phi_VR_PF = makeHist("Scand_phi_VR_PF",80,-M_PI,M_PI);
  Hcand_mass_VR_PF = makeHist("Hcand_mass_VR_PF",200,50.,250.);
  Hcand_pT_VR_PF = makeHist("Hcand_pT_VR_PF",305,450.,3500.);
  Hcand_eta_VR_PF = makeHist("Hcand_eta_VR_PF",40,-2.,2.);
  Hcand_phi_VR_PF = makeHist("Hcand_phi_VR_PF",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_VR_PF = makeHist("Xcand_mass_afterSorH_VR_PF",550,100.,6500.);
  Xcand_pT_afterSorH_VR_PF = makeHist("Xcand_pT_afterSorH_VR_PF",300,50.,3500.);
  Xcand_eta_afterSorH_VR_PF = makeHist("Xcand_eta_afterSorH_VR_PF",60,-3.,3.);
  Xcand_phi_afterSorH_VR_PF = makeHist("Xcand_phi_afterSorH_VR_PF",60,-M_PI,M_PI);
  mHvsmX_VR_PF = make2DHist("mHvsmX_VR_PF",550,100.,6500.,200,50.,250.);
  mSvsmX_VR_PF = make2DHist("mSvsmX_VR_PF",550,100.,6500.,145,50.,1500.);
  mSvsmH_VR_PF = make2DHist("mSvsmH_VR_PF",950,50.,1000.,250,50.,300.);
 // Truth matching 
  Truth_b_S_deltaR = makeHist("Truth_b_S_deltaR",60,0.,6.);
  Truth_b_H_deltaR = makeHist("Truth_b_H_deltaR",60,0.,6.);
  deltaR_truthS_reco_lrj = makeHist("deltaR_truthS_reco_lrj",60,0.,6.);
  deltaR_truthH_reco_lrj = makeHist("deltaR_truthH_reco_lrj",60,0.,6.);
  deltaR_truthS_reco_lrj_btag = makeHist("deltaR_truthS_reco_lrj_btag",60,0.,6.);
  deltaR_truthH_reco_lrj_btag = makeHist("deltaR_truthH_reco_lrj_btag",60,0.,6.);
  //After truth match
  LRJet_mass_tmatch_S = makeHist("LRJet_mass_tmatch_S",100,20.,1000.);
  LRJet_pt_tmatch_S = makeHist("LRJet_pt_tmatch_S",375,250.,3500.);
  //LRJet1_pt_tmatch_S = makeHist("LRJet1_pt_tmatch_S",455,450000.,5000000.);
  LRJet_eta_tmatch_S = makeHist("LRJet_eta_tmatch_S",60,-3.,3.);
  LRJet_phi_tmatch_S = makeHist("LRJet_phi_tmatch_S",80,-4.,4.);
  
  LRJet_mass_tmatch_S_lrjlabel = makeHist("LRJet_mass_tmatch_S_lrjlabel",100,20.,1000.);
  LRJet_pt_tmatch_S_lrjlabel = makeHist("LRJet_pt_tmatch_S_lrjlabel",375,250.,3500.);
  LRJet_eta_tmatch_S_lrjlabel = makeHist("LRJet_eta_tmatch_S_lrjlabel",60,-3.,3.);
  LRJet_phi_tmatch_S_lrjlabel = makeHist("LRJet_phi_tmatch_S_lrjlabel",80,-4.,4.);
  /*LRJet2_mass_tmatch_S = makeHist("LRJet2_mass_tmatch_S",100,20.,1000.);
  LRJet2_pt_tmatch_S = makeHist("LRJet2_pt_tmatch_S",375,250.,4000.);
  LRJet2_eta_tmatch_S = makeHist("LRJet2_eta_tmatch_S",60,-3.,3.);
  LRJet2_phi_tmatch_S = makeHist("LRJet2_phi_tmatch_S",80,-4.,4.);
*/
  Xcand_mass_tmatch = makeHist("Xcand_mass_tmatch",550,100.,6500.);
  Xcand_pT_tmatch = makeHist("Xcand_pT_tmatch",300,50.,3500.);
  Xcand_eta_tmatch = makeHist("Xcand_eta_tmatch",60,-3.,3.);
  Xcand_phi_tmatch = makeHist("Xcand_phi_tmatch",60,-M_PI,M_PI);
  Xcand_rapidity_tmatch = makeHist("Xcand_rapidity_tmatch",60,-M_PI,M_PI);
  
  LRJet_mass_tmatch_H_lrjlabel = makeHist("LRJet_mass_tmatch_H_lrjlabel",100,20.,1000.);
  LRJet_pt_tmatch_H_lrjlabel = makeHist("LRJet_pt_tmatch_H_lrjlabel",375,250.,3500.);
  //LRJet1_pt_tmatch_H_lrjlabel = makeHist("LRJet1_pt_tmatch",455,450000.,5000000.);
  LRJet_eta_tmatch_H_lrjlabel = makeHist("LRJet_eta_tmatch_H_lrjlabel",60,-3.,3.);
  LRJet_phi_tmatch_H_lrjlabel = makeHist("LRJet_phi_tmatch_H_lrjlabel",80,-4.,4.);
  
  LRJet_mass_tmatch_H = makeHist("LRJet_mass_tmatch_H",100,20.,1000.);
  LRJet_pt_tmatch_H = makeHist("LRJet_pt_tmatch_H",375,250.,3500.);
  LRJet_eta_tmatch_H = makeHist("LRJet_eta_tmatch_H",60,-3.,3.);
  LRJet_phi_tmatch_H = makeHist("LRJet_phi_tmatch_H",80,-4.,4.);
  
  mSvsmX_tmatch = make2DHist("mSvsmX_tmatch",550,100.,6500.,145,50.,1500.);
  mHvsmX_tmatch = make2DHist("mHvsmX_tmatch",550,100.,6500.,200,50.,250.);
  mSvsmH_tmatch = make2DHist("mSvsmH_tmatch",145,50.,1500,200,50.,250.);
//b-tagged after truth matching
  
  LRJet_mass_tmatch_btag_S = makeHist("LRJet_mass_tmatch_btag_S",100,20.,1000.);
  LRJet_pt_tmatch_btag_S = makeHist("LRJet_pt_tmatch_btag_S",375,250.,3500.);
  //LRJet1_pt_tmatch_btag_S = makeHist("LRJet1_pt_tmatch_btag_S",455,450000.,5000000.);
  LRJet_eta_tmatch_btag_S = makeHist("LRJet_eta_tmatch_btag_S",60,-3.,3.);
  LRJet_phi_tmatch_btag_S = makeHist("LRJet_phi_tmatch_btag_S",80,-4.,4.);
  LRJet_mass_tmatch_btag_H = makeHist("LRJet_mass_tmatch_btag_H",100,20.,1000.);
  LRJet_pt_tmatch_btag_H = makeHist("LRJet_pt_tmatch_btag_H",375,250.,3500.);
  LRJet_eta_tmatch_btag_H = makeHist("LRJet_eta_tmatch_btag_H",60,-3.,3.);
  LRJet_phi_tmatch_btag_H = makeHist("LRJet_phi_tmatch_btag_H",80,-4.,4.);
  Xcand_mass_tmatch_btag = makeHist("Xcand_mass_tmatch_btag",550,100.,6500.);
  Xcand_pT_tmatch_btag = makeHist("Xcand_pT_tmatch_btag",300,50.,3500.);
  Xcand_eta_tmatch_btag = makeHist("Xcand_eta_tmatch_btag",60,-3.,3.);
  Xcand_phi_tmatch_btag = makeHist("Xcand_phi_tmatch_btag",60,-M_PI,M_PI);
  Xcand_rapidity_tmatch_btag = makeHist("Xcand_rapidity_tmatch_btag",60,-M_PI,M_PI);
  mSvsmX_tmatch_btag = make2DHist("mSvsmX_tmatch_btag",550,100.,6500.,145,50.,1500.);
  mHvsmX_tmatch_btag = make2DHist("mHvsmX_tmatch_btag",550,100.,6500.,200,50.,250.);
  mSvsmH_tmatch_btag = make2DHist("mSvsmH_tmatch_btag",145,50.,1500,200,50.,250.);
  //After CR, VR regions using method 2 which is similar to Y->X(qq)H(bb)
  Scand_mass_SR2 = makeHist("Scand_mass_SR2",145,50.,1500.);
  Scand_pT_SR2 = makeHist("Scand_pT_SR2",305,450.,3500.);
  Scand_eta_SR2 = makeHist("Scand_eta_SR2",40,-2.,2.);
  Scand_phi_SR2 = makeHist("Scand_phi_SR2",80,-M_PI,M_PI);
  Hcand_mass_SR2 = makeHist("Hcand_mass_SR2",200,50.,250.);
  Hcand_pT_SR2 = makeHist("Hcand_pT_SR2",305,450.,3500.);
  Hcand_eta_SR2 = makeHist("Hcand_eta_SR2",40,-2.,2.);
  Hcand_phi_SR2 = makeHist("Hcand_phi_SR2",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_SR2 = makeHist("Xcand_mass_afterSorH_SR2",550,100.,6500.);
  Xcand_pT_afterSorH_SR2 = makeHist("Xcand_pT_afterSorH_SR2",300,50.,3500.);
  Xcand_eta_afterSorH_SR2 = makeHist("Xcand_eta_afterSorH_SR2",60,-3.,3.);
  Xcand_phi_afterSorH_SR2 = makeHist("Xcand_phi_afterSorH_SR2",60,-M_PI,M_PI);
  mHvsmX_SR2 = make2DHist("mHvsmX_SR2",550,100.,6500.,200,50.,250.);
  mSvsmX_SR2 = make2DHist("mSvsmX_SR2",550,100.,6500.,145,50.,1500.);
  mSvsmH_SR2 = make2DHist("mSvsmH_SR2",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 2 which is similar to Y->X(qq)H(bb)
  Scand_mass_CR0 = makeHist("Scand_mass_CR0",145,50.,1500.);
  Scand_pT_CR0 = makeHist("Scand_pT_CR0",305,450.,3500.);
  Scand_eta_CR0 = makeHist("Scand_eta_CR0",40,-2.,2.);
  Scand_phi_CR0 = makeHist("Scand_phi_CR0",80,-M_PI,M_PI);
  Hcand_mass_CR0 = makeHist("Hcand_mass_CR0",200,50.,250.);
  Hcand_pT_CR0 = makeHist("Hcand_pT_CR0",305,450.,3500.);
  Hcand_eta_CR0 = makeHist("Hcand_eta_CR0",40,-2.,2.);
  Hcand_phi_CR0 = makeHist("Hcand_phi_CR0",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_CR0 = makeHist("Xcand_mass_afterSorH_CR0",550,100.,6500.);
  Xcand_pT_afterSorH_CR0 = makeHist("Xcand_pT_afterSorH_CR0",300,50.,3500.);
  Xcand_eta_afterSorH_CR0 = makeHist("Xcand_eta_afterSorH_CR0",60,-3.,3.);
  Xcand_phi_afterSorH_CR0 = makeHist("Xcand_phi_afterSorH_CR0",60,-M_PI,M_PI);
  mHvsmX_CR0 = make2DHist("mHvsmX_CR0",550,100.,6500.,200,50.,250.);
  mSvsmX_CR0 = make2DHist("mSvsmX_CR0",550,100.,6500.,145,50.,1500.);
  mSvsmH_CR0 = make2DHist("mSvsmH_CR0",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 2 which is similar to Y->X(qq)H(bb)
  Scand_mass_CR_HSB1 = makeHist("Scand_mass_CR_HSB1",145,50.,1500.);
  Scand_pT_CR_HSB1 = makeHist("Scand_pT_CR_HSB1",305,450.,3500.);
  Scand_eta_CR_HSB1 = makeHist("Scand_eta_CR_HSB1",40,-2.,2.);
  Scand_phi_CR_HSB1 = makeHist("Scand_phi_CR_HSB1",80,-M_PI,M_PI);
  Hcand_mass_CR_HSB1 = makeHist("Hcand_mass_CR_HSB1",200,50.,250.);
  Hcand_pT_CR_HSB1 = makeHist("Hcand_pT_CR_HSB1",305,450.,3500.);
  Hcand_eta_CR_HSB1 = makeHist("Hcand_eta_CR_HSB1",40,-2.,2.);
  Hcand_phi_CR_HSB1 = makeHist("Hcand_phi_CR_HSB1",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_CR_HSB1 = makeHist("Xcand_mass_afterSorH_CR_HSB1",550,100.,6500.);
  Xcand_pT_afterSorH_CR_HSB1 = makeHist("Xcand_pT_afterSorH_CR_HSB1",300,50.,3500.);
  Xcand_eta_afterSorH_CR_HSB1 = makeHist("Xcand_eta_afterSorH_CR_HSB1",60,-3.,3.);
  Xcand_phi_afterSorH_CR_HSB1 = makeHist("Xcand_phi_afterSorH_CR_HSB1",60,-M_PI,M_PI);
  mHvsmX_CR_HSB1 = make2DHist("mHvsmX_CR_HSB1",550,100.,6500.,200,50.,250.);
  mSvsmX_CR_HSB1 = make2DHist("mSvsmX_CR_HSB1",550,100.,6500.,145,50.,1500.);
  mSvsmH_CR_HSB1 = make2DHist("mSvsmH_CR_HSB1",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 2 which is similar to Y->X(qq)H(bb)
  Scand_mass_CR_HSB2 = makeHist("Scand_mass_CR_HSB2",145,50.,1500.);
  Scand_pT_CR_HSB2 = makeHist("Scand_pT_CR_HSB2",305,450.,3500.);
  Scand_eta_CR_HSB2 = makeHist("Scand_eta_CR_HSB2",40,-2.,2.);
  Scand_phi_CR_HSB2 = makeHist("Scand_phi_CR_HSB2",80,-M_PI,M_PI);
  Hcand_mass_CR_HSB2 = makeHist("Hcand_mass_CR_HSB2",200,50.,250.);
  Hcand_pT_CR_HSB2 = makeHist("Hcand_pT_CR_HSB2",305,450.,3500.);
  Hcand_eta_CR_HSB2 = makeHist("Hcand_eta_CR_HSB2",40,-2.,2.);
  Hcand_phi_CR_HSB2 = makeHist("Hcand_phi_CR_HSB2",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_CR_HSB2 = makeHist("Xcand_mass_afterSorH_CR_HSB2",550,100.,6500.);
  Xcand_pT_afterSorH_CR_HSB2 = makeHist("Xcand_pT_afterSorH_CR_HSB2",300,50.,3500.);
  Xcand_eta_afterSorH_CR_HSB2 = makeHist("Xcand_eta_afterSorH_CR_HSB2",60,-3.,3.);
  Xcand_phi_afterSorH_CR_HSB2 = makeHist("Xcand_phi_afterSorH_CR_HSB2",60,-M_PI,M_PI);
  mHvsmX_CR_HSB2 = make2DHist("mHvsmX_CR_HSB2",550,100.,6500.,200,50.,250.);
  mSvsmX_CR_HSB2 = make2DHist("mSvsmX_CR_HSB2",550,100.,6500.,145,50.,1500.);
  mSvsmH_CR_HSB2 = make2DHist("mSvsmH_CR_HSB2",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 2 which is similar to Y->X(qq)H(bb)
  Scand_mass_VR_LSB1 = makeHist("Scand_mass_VR_LSB1",145,50.,1500.);
  Scand_pT_VR_LSB1 = makeHist("Scand_pT_VR_LSB1",305,450.,3500.);
  Scand_eta_VR_LSB1 = makeHist("Scand_eta_VR_LSB1",40,-2.,2.);
  Scand_phi_VR_LSB1 = makeHist("Scand_phi_VR_LSB1",80,-M_PI,M_PI);
  Hcand_mass_VR_LSB1 = makeHist("Hcand_mass_VR_LSB1",200,50.,250.);
  Hcand_pT_VR_LSB1 = makeHist("Hcand_pT_VR_LSB1",305,450.,3500.);
  Hcand_eta_VR_LSB1 = makeHist("Hcand_eta_VR_LSB1",40,-2.,2.);
  Hcand_phi_VR_LSB1 = makeHist("Hcand_phi_VR_LSB1",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_VR_LSB1 = makeHist("Xcand_mass_afterSorH_VR_LSB1",550,100.,6500.);
  Xcand_pT_afterSorH_VR_LSB1 = makeHist("Xcand_pT_afterSorH_VR_LSB1",300,50.,3500.);
  Xcand_eta_afterSorH_VR_LSB1 = makeHist("Xcand_eta_afterSorH_VR_LSB1",60,-3.,3.);
  Xcand_phi_afterSorH_VR_LSB1 = makeHist("Xcand_phi_afterSorH_VR_LSB1",60,-M_PI,M_PI);
  mHvsmX_VR_LSB1 = make2DHist("mHvsmX_VR_LSB1",550,100.,6500.,200,50.,250.);
  mSvsmX_VR_LSB1 = make2DHist("mSvsmX_VR_LSB1",550,100.,6500.,145,50.,1500.);
  mSvsmH_VR_LSB1 = make2DHist("mSvsmH_VR_LSB1",950,50.,1000.,250,50.,300.);
  //After CR, VR regions using method 2 which is similar to Y->X(qq)H(bb)
  Scand_mass_VR_LSB2 = makeHist("Scand_mass_VR_LSB2",145,50.,1500.);
  Scand_pT_VR_LSB2 = makeHist("Scand_pT_VR_LSB2",305,450.,3500.);
  Scand_eta_VR_LSB2 = makeHist("Scand_eta_VR_LSB2",40,-2.,2.);
  Scand_phi_VR_LSB2 = makeHist("Scand_phi_VR_LSB2",80,-M_PI,M_PI);
  Hcand_mass_VR_LSB2 = makeHist("Hcand_mass_VR_LSB2",200,50.,250.);
  Hcand_pT_VR_LSB2 = makeHist("Hcand_pT_VR_LSB2",305,450.,3500.);
  Hcand_eta_VR_LSB2 = makeHist("Hcand_eta_VR_LSB2",40,-2.,2.);
  Hcand_phi_VR_LSB2 = makeHist("Hcand_phi_VR_LSB2",80,-M_PI,M_PI);
  Xcand_mass_afterSorH_VR_LSB2 = makeHist("Xcand_mass_afterSorH_VR_LSB2",550,100.,6500.);
  Xcand_pT_afterSorH_VR_LSB2 = makeHist("Xcand_pT_afterSorH_VR_LSB2",300,50.,3500.);
  Xcand_eta_afterSorH_VR_LSB2 = makeHist("Xcand_eta_afterSorH_VR_LSB2",60,-3.,3.);
  Xcand_phi_afterSorH_VR_LSB2 = makeHist("Xcand_phi_afterSorH_VR_LSB2",60,-M_PI,M_PI);
  mHvsmX_VR_LSB2 = make2DHist("mHvsmX_VR_LSB2",550,100.,6500.,200,50.,250.);
  mSvsmX_VR_LSB2 = make2DHist("mSvsmX_VR_LSB2",550,100.,6500.,145,50.,1500.);
  mSvsmH_VR_LSB2 = make2DHist("mSvsmH_VR_LSB2",950,50.,1000.,250,50.,300.);
  //Let's get cutflows!!
  Cutflow = makeHist("Cutflow",8,0.0,8.0);
  Cutflow->GetXaxis()->SetBinLabel(1,"Total Events");
  Cutflow->GetXaxis()->SetBinLabel(2,"LRJ trigger 420");
  Cutflow->GetXaxis()->SetBinLabel(3,"At least 2 lrj");
  Cutflow->GetXaxis()->SetBinLabel(4,"pT(Jet1/Jet2) > 450(250) GeV");
  Cutflow->GetXaxis()->SetBinLabel(5,"S and H candidate assignment");
  Cutflow->GetXaxis()->SetBinLabel(6,"2B region");
  Cutflow->GetXaxis()->SetBinLabel(7,"4B region");
  Cutflow->GetXaxis()->SetBinLabel(8,"#Delta #eta < 1.1");
  /*Cutflow->GetXaxis()->SetBinLabel(5,"X_HH< 1.6");
  Cutflow->GetXaxis()->SetBinLabel(6,"4B_DL1r_77");
  Cutflow->GetXaxis()->SetBinLabel(7,"4B_Xbb60");
  Cutflow->GetXaxis()->SetBinLabel(8,"3B_DL1r_77");
  Cutflow->GetXaxis()->SetBinLabel(9,"3B_Xbb");
  Cutflow->GetXaxis()->SetBinLabel(10,"2B_DL1r_77");
  Cutflow->GetXaxis()->SetBinLabel(11,"2B_Xbb70");*/
  TotalEvents = makeHist("TotalEvents",1,0.0,1.0);
  Trig_420 = makeHist("Trig_420",1,0.0,1.0);
  AtLeast2lrj = makeHist("AtLeast2lrj",1,0.0,1.0);
  AtLeast2Bs_lrj = makeHist("AtLeast2Bs_lrj",1,0.0,1.0);
  //Hcand_mass_SR2 = makeHist("Hcand_mass_SR2",550,50.,600.);
  //Scand_mass_SR2 = makeHist("Scand_mass_SR2",550,50.,600.);
}

Bool_t Nominal::Process(Long64_t entry,int index)
{
  // Don't delete this line! Without it the program will crash.
  fReader.SetEntry(entry);
 
  //Here I am converting the ULong64_t to int.
  //This step is needed to use random number generator seed.
  //Because srand(unsigned int seed) seed must be int not long long int, okay? 
  long long a = *EventNumber;
  std::stringstream ss;
  ss << a;
  std::string str = ss.str();
  int evtNumber = atoi(str.c_str());
  // ** Finished conversion ** //
  //********* Loop section *********
  float totalWeight = 1;
  // if mc, change weight to be the mc weight
#ifndef processdata
  if (index==0) {
    totalWeight = (EventWeight[0])*(*PRWEventWeight);
  }
#endif
  
    //std::cout<<"Run Number: "<<*RunNumber<<std::endl;
  is2015 = *RunNumber>= 276262 && *RunNumber<=284484;
  is2016 = *RunNumber>= 296939 && *RunNumber<=311481;
  is2017 = *RunNumber>= 324320 && *RunNumber<=341649;
  is2018 = *RunNumber>= 348197 && *RunNumber<=364292; // Check the run numbers are correct!!!!!!!
  #ifndef processdata
  is2015 = *RunNumber == 284500;
  is2016 = *RunNumber == 284500;
  is2017 = *RunNumber == 300000;
  is2018 = *RunNumber == 310000;
  //bool is2022 = *RunNumber == 410000; // Check the run numbers are correct!!!!!!!
#endif

  Cutflow->Fill(0.0,totalWeight);
  TotalEvents->Fill(0.0,totalWeight);
  double totevents = TotalEvents->GetSumOfWeights();
  double passtrig= -999.;
  //std::cout<<"tot events: "<<totevents<<std::endl;
#ifdef trig13167
  if(*trigPassed_HLT_j360_a10_lcw_sub_L1J100 == 1 || *trigPassed_HLT_j420_a10_lcw_L1J100 == 1){ // TRIG MC20A
#endif
#ifdef trig13144
  if(*trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100 == 1){ // TRIG MC20D
#endif
#ifdef trig13145
  if(is2018 && *trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100 == 1){ // TRIG MC20E
#endif
    Cutflow->Fill(1.0,totalWeight);
    Trig_420->Fill(0.0,totalWeight);
    passtrig = Trig_420->GetSumOfWeights();
  }
  std::vector<fatjet> largeRjets;

  int ntrackjets =0;
  int nlargeRjets = LargeRJetPt.GetSize();
  int njets = JetPt.GetSize();
  int num_lrjets = 0; 
  int num_lrjets_2b_77 = 0; 
  int num_lrjets_2b_85 = 0; 
  int num_lrjets_2b_85_77 = 0; 
  for (int i=0;i<nlargeRjets; ++i) { 
    if(LargeRJetPt[i]/1000.>250. && fabs(LargeRJetEta[i]) <2.0 ) {
      ++num_lrjets;
      fatjet temp;
      fatjet temp_ufo;
      temp.SetPtEtaPhiM(LargeRJetPt[i]/1000.,LargeRJetEta[i],LargeRJetPhi[i],LargeRJetMass[i]/1000.);
      #ifndef processdata
      temp.truthlabel = LargeRJetTruthLabelID[i];
      temp.truthmass_S = *TruthMass_S/1000.;
      temp.truthpt_S = *TruthPt_S/1000.;
      temp.trutheta_S = *TruthEta_S;
      temp.truthphi_S = *TruthPhi_S;
      temp.truthmass_H = *TruthMass_H/1000.;
      temp.truthpt_H = *TruthPt_H/1000.;
      temp.trutheta_H = *TruthEta_H;
      temp.truthphi_H = *TruthPhi_H;
      #endif
      ntrackjets = TrackJetPt[i].size();
      int num_VRB_77 = 0; 
      int num_VRB_85 = 0; 
      int num_VRB_85_77 = 0; 
      int ntracks =0;
      for (int j=0;j<ntrackjets; ++j){
        if(*passDRcut ==1 ){ 
        ++ntracks; 
        temp.vrPt.push_back(TrackJetPt[i][j]/1000.);
        temp.vrMass.push_back(TrackJetMass[i][j]/1000.);
        temp.vrPhi.push_back(TrackJetPhi[i][j]);
        temp.vrEta.push_back(TrackJetEta[i][j]);
        temp.DL1r_77.push_back(TrackJetIsBTagged_DL1r_FixedCutBEff_77[i][j]);
        temp.DL1r_85.push_back(TrackJetIsBTagged_DL1r_FixedCutBEff_85[i][j]);
#ifndef processdata
        temp.TrackJetTruthLabel.push_back(TrackJetHadronConeExclTruthLabelID[i][j]);
#endif
        if (TrackJetIsBTagged_DL1r_FixedCutBEff_77[i][j] == 1){
          ++num_VRB_77;
        }
        if (TrackJetIsBTagged_DL1r_FixedCutBEff_85[i][j] == 1){
          ++num_VRB_85;
        }
        if ( (TrackJetIsBTagged_DL1r_FixedCutBEff_85[i][j] == 0 && TrackJetIsBTagged_DL1r_FixedCutBEff_77[i][j] == 1) || (TrackJetIsBTagged_DL1r_FixedCutBEff_85[i][j] == 1 && TrackJetIsBTagged_DL1r_FixedCutBEff_77[i][j] == 0) ){
          ++num_VRB_85_77;
        }
        
    }//Close for passDR cut
  }//Close VR loop
    temp.numtrackjets = ntracks; 
    temp.n_b_DL1r77 = num_VRB_77; 
    temp.n_b_DL1r85 = num_VRB_85;
    temp.n_b_DL1r85_77 = num_VRB_85_77;
    Num_VRjets->Fill(ntracks,totalWeight);
    Num_VRjets_DL1r_77->Fill(num_VRB_77,totalWeight);
    Num_VRjets_DL1r_85->Fill(num_VRB_85,totalWeight);
    if (num_VRB_77>=2)  ++num_lrjets_2b_77;
    if (num_VRB_85>=2)  ++num_lrjets_2b_85;
    if (num_VRB_85_77>=2)  ++num_lrjets_2b_85_77;

    largeRjets.push_back(temp);
 }//If of lrj selection
}//Close for lrj loop
    
    Num_largeRj->Fill(num_lrjets,totalWeight);
    Num_largeRj_2btag_77->Fill(num_lrjets_2b_77,totalWeight);
    Num_largeRj_2btag_85->Fill(num_lrjets_2b_85,totalWeight);
    Num_largeRj_2btag_85_77->Fill(num_lrjets_2b_85_77,totalWeight);

//It's repetition. Anyways here is the ufo jet
  std::vector<fatjet> largeRUFOjets;
  
  int ntrackjets_ufo =0;
  int nlargeRUFOjets = LargeRUFOJetPt.GetSize();
  int num_lrjets_ufo = 0; 
  int num_lrjets_ufo_2b_77 = 0; 
  int num_lrjets_ufo_2b_85 = 0; 

  for (int i=0;i<nlargeRUFOjets; ++i) { 
    if(LargeRUFOJetPt[i]/1000.>250. && fabs(LargeRUFOJetEta[i]) <2.0 ) {
      ++num_lrjets_ufo;
      fatjet temp;
      temp.SetPtEtaPhiM(LargeRUFOJetPt[i]/1000.,LargeRUFOJetEta[i],LargeRUFOJetPhi[i],LargeRUFOJetMass[i]/1000.);
      #ifndef processdata
      temp.truthlabel_ufo = LargeRUFOJetTruthLabelID[i];
      #endif
      ntrackjets_ufo = UFOTrackJetPt[i].size();
      int num_VRB_77_ufo = 0; 
      int num_VRB_85_ufo = 0; 
      int ntracks_ufo =0;
        
      for (int j=0;j<ntrackjets_ufo; ++j){
        if(*passDRcut ==1 ){ 
        ++ntracks_ufo; 
        temp.vrPt_ufo.push_back(UFOTrackJetPt[i][j]/1000.);
        temp.vrMass_ufo.push_back(UFOTrackJetMass[i][j]/1000.);
        temp.vrPhi_ufo.push_back(UFOTrackJetPhi[i][j]);
        temp.vrEta_ufo.push_back(UFOTrackJetEta[i][j]);
        temp.DL1r_77_ufo.push_back(UFOTrackJetIsBTagged_DL1r_FixedCutBEff_77[i][j]);
        temp.DL1r_85_ufo.push_back(UFOTrackJetIsBTagged_DL1r_FixedCutBEff_85[i][j]);
#ifndef processdata
        temp.UFOTrackJetTruthLabel.push_back(TrackJetHadronConeExclTruthLabelID_ufo[i][j]);
#endif
        if (UFOTrackJetIsBTagged_DL1r_FixedCutBEff_77[i][j] == 1){
          ++num_VRB_77_ufo;
        }
        if (UFOTrackJetIsBTagged_DL1r_FixedCutBEff_85[i][j] == 1){
          ++num_VRB_85_ufo;
        }
        
    }//Close for passDR cut
  }//Close VR loop
    temp.numtrackjets_ufo = ntracks_ufo; 
    temp.n_b_DL1r77_ufo = num_VRB_77_ufo; 
    temp.n_b_DL1r85_ufo = num_VRB_85_ufo;
    Num_VRjets_ufo->Fill(ntracks_ufo,totalWeight);
    Num_VRjets_DL1r_77_ufo->Fill(num_VRB_77_ufo,totalWeight);
    Num_VRjets_DL1r_85_ufo->Fill(num_VRB_85_ufo,totalWeight);
    if (num_VRB_77_ufo>=2)  ++num_lrjets_ufo_2b_77;
    if (num_VRB_85_ufo>=2)  ++num_lrjets_ufo_2b_85;

    largeRUFOjets.push_back(temp);
 }//If of lrj selection
}//Close for lrj loop
    
    Num_largeRj_ufo->Fill(num_lrjets_ufo,totalWeight);
    Num_largeRj_ufo_2btag_77->Fill(num_lrjets_ufo_2b_77,totalWeight);
    Num_largeRj_ufo_2btag_85->Fill(num_lrjets_ufo_2b_85,totalWeight);


 // Truth fat jets
 #ifndef processdata
 std::vector<truthfatjet> truthfatjets;
 int ntruthfatjets = TruthLargeRJetPt.GetSize();
 for(int i=0;i<ntruthfatjets;++i){
   truthfatjet temp;
   temp.SetPtEtaPhiM(TruthLargeRJetPt[i]/1000.,TruthLargeRJetEta[i],TruthLargeRJetPhi[i],TruthLargeRJetMass[i]/1000.);
   truthfatjets.push_back(temp);
 }//End of the truth fat jet loop
 
 truthfatjet * truth_lrj1 = 0;
 truthfatjet * truth_lrj2 = 0;
 // Finding highest pt truth largeR jets
 double leadPt = 0;
 double subleadPt = 0;

 if(ntruthfatjets > 0){
   for(auto tlrj = truthfatjets.begin();tlrj != truthfatjets.end();++tlrj){
     truthfatjet & truthlargeR = (*tlrj);
     if(truthlargeR.isSelected() && truthlargeR.Pt() > leadPt){
       truth_lrj1 = &truthlargeR;
       leadPt = truth_lrj1->Pt();
     }
     else continue;
   }//End for tlrj loop
 }//End of ntruthfatjets > 0
 if(ntruthfatjets > 1){
   for(auto tlrj = truthfatjets.begin();tlrj!=truthfatjets.end();++tlrj){
     truthfatjet & truthlargeR = (*tlrj);
     if(truthlargeR.isSelected() && truthlargeR.Pt() < leadPt){
       if(truthlargeR.Pt() > subleadPt){
         truth_lrj2 = &truthlargeR;
         subleadPt = truth_lrj2->Pt();
       }
       else continue;
     }//end of second if
     else continue;
   }//end of second truth large R jet
 }//End of ntruthfatjets>1

 #endif
 int num_jets = 0; 
 int num_bjets_77 = 0; 
 int num_bjets_85 = 0; 
 int num_bjets_GN1_70 = 0; 
 int num_bjets_GN1_77 = 0; 
 int num_bjets_GN1_85 = 0; 
  std::vector<smallrjet> smallRjets;
  for (int i=0;i<njets;++i) {  
    smallrjet temp;
    if(JetPt[i]/1000.>25. && abs(JetEta[i])<2.5){
    ++num_jets;
    temp.DL1d_77 = SmallRJetIsBTagged_DL1d_FixedCutBEff_77[i];
    temp.DL1d_85 = SmallRJetIsBTagged_DL1d_FixedCutBEff_85[i];
    temp.GN1_70 = SmallRJetIsBTagged_GN1_FixedCutBEff_70[i];
    temp.GN1_77 = SmallRJetIsBTagged_GN1_FixedCutBEff_77[i];
    temp.GN1_85 = SmallRJetIsBTagged_GN1_FixedCutBEff_85[i];
    temp.SetPtEtaPhiM(JetPt[i]/1000.,JetEta[i],JetPhi[i],JetMass[i]/1000.);
      if (SmallRJetIsBTagged_DL1d_FixedCutBEff_77[i] == 1){
          ++num_bjets_77;
      }
      if (SmallRJetIsBTagged_DL1d_FixedCutBEff_85[i] == 1){
          ++num_bjets_85;
      }
      if (SmallRJetIsBTagged_GN1_FixedCutBEff_70[i] == 1){
          ++num_bjets_GN1_70;
      }
      if (SmallRJetIsBTagged_GN1_FixedCutBEff_77[i] == 1){
          ++num_bjets_GN1_77;
      }
      if (SmallRJetIsBTagged_GN1_FixedCutBEff_85[i] == 1){
          ++num_bjets_GN1_85;
      }
    
    }
  //std::cout<<"number of small R jets: "<<num_jets<<" tagged b-jets: "<<num_bjets_77<<std::endl;
    smallRjets.push_back(temp);
  }//Close smallR

      Num_smallRj->Fill(num_jets,totalWeight);
      Num_smallRj_b_77->Fill(num_bjets_77,totalWeight);
      Num_smallRj_b_85->Fill(num_bjets_85,totalWeight);
      Num_smallRj_b_GN1_70->Fill(num_bjets_GN1_70,totalWeight);
      Num_smallRj_b_GN1_77->Fill(num_bjets_GN1_77,totalWeight);
      Num_smallRj_b_GN1_85->Fill(num_bjets_GN1_85,totalWeight);
      //Num_VRjets->Fill(largeRjets.numtrackjets,totalWeight);
      //std::cout<<"test of num track jets per large-R jet: "<<ntrackjets<<std::endl;

#ifndef processdata  
  std::vector<truth_b> truthBjets;
  int ntruthbjets = TruthBJetPt_S.GetSize();
  for (int i=0;i<ntruthbjets;++i) {  
    truth_b temp;
    //if(TruthBJetPt_S[i]>25000. && abs(TruthBJetEta_S[i])<2.5){
    temp.SetPtEtaPhiM(TruthBJetPt_S[i]/1000,TruthBJetEta_S[i],TruthBJetPhi_S[i],TruthBJetMass_S[i]/1000);
    
    truthBjets.push_back(temp);
  }//Close truth_b
  
  std::vector<truth_b> truthBjets_H;
  int ntruthbjets_h = TruthBJetPt_H.GetSize();
  for (int i=0;i<ntruthbjets_h;++i) {  
    truth_b temp;
    //if(TruthBJetPt_S[i]>25000. && abs(TruthBJetEta_S[i])<2.5){
    temp.SetPtEtaPhiM(TruthBJetPt_H[i]/1000,TruthBJetEta_H[i],TruthBJetPhi_H[i],TruthBJetMass_H[i]/1000);
    
    truthBjets_H.push_back(temp);
  }//Close truth_b

  //test of the truth Higgs
  TLorentzVector truthHiggs, truthScalar;
  //std::vector<truthfatjet> truthScalar;
  int ntruth_h = 1;
    truthHiggs.SetPtEtaPhiM(*TruthPt_H/1000.,*TruthEta_H,*TruthPhi_H,*TruthMass_H/1000.);
    truthScalar.SetPtEtaPhiM(*TruthPt_S/1000.,*TruthEta_S,*TruthPhi_S,*TruthMass_S/1000.);
#endif

  fatjet * leading_lrj = 0;
  fatjet * sub_leading_lrj = 0;

      // Find the highest pt large R jet that is a Hcand
   double highestPt = 0;
   double secondPt = 0;
    
   if(nlargeRjets > 0) {
     for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
      fatjet & largeR = (*lrj);
      if (largeR.isSelected() && largeR.numtrackjets >= 2 && largeR.Pt() > highestPt) {
        //std::cout<<"NUM TRACK jets: "<<largeR.numtrackjets<<std::endl;
          leading_lrj = &largeR;
          highestPt = leading_lrj->Pt();
          if(leading_lrj->Pt()>250. && leading_lrj->M()>100.){
            leadjetMass_beforeTrig_pT250_M100->Fill(leading_lrj->M(),totalWeight);
            leadjetPt_beforeTrig_pT250_M100->Fill(leading_lrj->Pt(),totalWeight);
          }
          if(leading_lrj->Pt()>500. && leading_lrj->M()>40.){
            leadjetMass_beforeTrig_pT500_M40->Fill(leading_lrj->M(),totalWeight);
            leadjetPt_beforeTrig_pT500_M40->Fill(leading_lrj->Pt(),totalWeight);
          }

#ifdef trig13167
          if(is2015 && is2016 && *trigPassed_HLT_j360_a10_lcw_sub_L1J100 == 1 || *trigPassed_HLT_j420_a10_lcw_L1J100 == 1){ // TRIG MC20A
#endif
#ifdef trig13144
         if(is2017 && *trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100 == 1 && leading_lrj->Pt()>250. && leading_lrj->M()>100.){ //mc20d
#endif
#ifdef trig13145
         if(is2018 && *trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100 == 1 && leading_lrj->Pt()>250. && leading_lrj->M()>100.){ //mc20e
#endif
            leadjetPt_afterTrig_pT250_M100->Fill(leading_lrj->Pt(),totalWeight);
            leadjetMass_afterTrig_pT250_M100->Fill(leading_lrj->M(),totalWeight);

          }
#ifdef trig13167
          if(is2015 && is2016 && *trigPassed_HLT_j360_a10_lcw_sub_L1J100 == 1 || *trigPassed_HLT_j420_a10_lcw_L1J100 == 1){ // TRIG MC20A
#endif
#ifdef trig13144
          if(is2017 && *trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100 == 1 && leading_lrj->Pt()>500. && leading_lrj->M()>40.){ //mc20d
#endif
#ifdef trig13145
          if(is2018 && *trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100 == 1 && leading_lrj->Pt()>500. && leading_lrj->M()>40.){ //mc20e
#endif
            leadjetPt_afterTrig_pT500_M40->Fill(leading_lrj->Pt(),totalWeight);
            leadjetMass_afterTrig_pT500_M40->Fill(leading_lrj->M(),totalWeight);

          }
#ifdef trig13145
          if(is2018 && *trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100 == 1 && leading_lrj->Pt()>250. && leading_lrj->M()>100.){ //mc20e
            leadjetPt_afterTrig390_pT250_M100->Fill(leading_lrj->Pt(),totalWeight);
            leadjetMass_afterTrig390_pT250_M100->Fill(leading_lrj->M(),totalWeight);
          }
          if(is2018 && *trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100 == 1 && leading_lrj->Pt()>500. && leading_lrj->M()>40.){ //mc20e
            leadjetPt_afterTrig390_pT500_M40->Fill(leading_lrj->Pt(),totalWeight);
            leadjetMass_afterTrig390_pT500_M40->Fill(leading_lrj->M(),totalWeight);
          }
#endif
        }
        else continue;
      }
    }
    if(nlargeRjets > 1) {
      // find sub-leading lrj
      for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
        fatjet & largeR = (*lrj);
        if (largeR.isSelected() && largeR.numtrackjets >= 2 && largeR.Pt() < highestPt ) {
          if (largeR.Pt() > secondPt) {
            sub_leading_lrj = &largeR;
            secondPt = sub_leading_lrj->Pt();
          subleadjetPt_beforeTrig->Fill(sub_leading_lrj->Pt(),totalWeight);
          subleadjetMass_beforeTrig->Fill(sub_leading_lrj->M(),totalWeight);
          }
          else continue;
          }
        else continue;
      }
    }
/// Let's do the UFO large-R jets
  fatjet * leading_lrj_ufo = 0;
  fatjet * sub_leading_lrj_ufo = 0;

   if(nlargeRUFOjets > 0) {
     for (auto lrj_ufo = largeRUFOjets.begin();lrj_ufo != largeRUFOjets.end();++lrj_ufo) {
      fatjet & largeRUFO = (*lrj_ufo);
      if (largeRUFO.isSelected() && largeRUFO.numtrackjets_ufo >= 2 && largeRUFO.Pt() > highestPt) {
        //std::cout<<"NUM TRACK jets: "<<largeRUFO.numtrackjets_ufo<<std::endl;
          leading_lrj_ufo = &largeRUFO;
          highestPt = leading_lrj_ufo->Pt();
        }
        else continue;
      }
    }
    if(nlargeRUFOjets > 1) {
      // find sub-leading lrj_ufo
      for (auto lrj_ufo = largeRUFOjets.begin();lrj_ufo != largeRUFOjets.end();++lrj_ufo) {
        fatjet & largeRUFO = (*lrj_ufo);
        if (largeRUFO.isSelected() && largeRUFO.numtrackjets_ufo >= 2 && largeRUFO.Pt() < highestPt ) {
          if (largeRUFO.Pt() > secondPt) {
            sub_leading_lrj_ufo = &largeRUFO;
            secondPt = sub_leading_lrj_ufo->Pt();
          subleadjetPt_beforeTrig->Fill(sub_leading_lrj_ufo->Pt(),totalWeight);
          subleadjetMass_beforeTrig->Fill(sub_leading_lrj_ufo->M(),totalWeight);
          }
          else continue;
          }
        else continue;
      }
    }
// *** End of UfO here *** //

  TLorentzVector leading_boson;
  TLorentzVector subleading_boson;
  TLorentzVector X_cand;
  TLorentzVector H_cand;
  TLorentzVector S_cand;
  TLorentzVector truthS, truthH;

  // Leading Large-R jet

  if(leading_lrj != 0 ){//start if leading_lrj!=0
    lj1_mass = leading_lrj->M();
    //int lj1_truthlabel = leading_lrj->truthlabel;
    lj1_pT = leading_lrj->Pt();
    lj1_eta = leading_lrj->Eta();
    lj1_phi = leading_lrj->Phi();
    leading_boson.SetPtEtaPhiM(lj1_pT,lj1_eta,lj1_phi,lj1_mass);

    #ifndef processdata
    lj1_truthS_pt = leading_lrj->truthpt_S;
    lj1_truthS_mass = leading_lrj->truthmass_S;
    lj1_truthS_eta = leading_lrj->trutheta_S;
    lj1_truthS_phi = leading_lrj->truthphi_S;
    lj1_truthH_pt = leading_lrj->truthpt_H;
    lj1_truthH_mass = leading_lrj->truthmass_H;
    lj1_truthH_eta = leading_lrj->trutheta_H;
    lj1_truthH_phi = leading_lrj->truthphi_H;

    truthS.SetPtEtaPhiM(lj1_truthS_pt,lj1_truthS_eta,lj1_truthS_phi,lj1_truthS_mass);
    truthH.SetPtEtaPhiM(lj1_truthH_pt,lj1_truthH_eta,lj1_truthH_phi,lj1_truthH_mass);
    #endif
    //std::cout<<"delta R between truth S and reco lrj: "<<dR_truthS_reco_lrj<<std::endl;
  }// End of leading_lrj!=0

  if(sub_leading_lrj != 0 ){ // Start of sub_leading_lrj!=0
    lj2_mass = sub_leading_lrj->M();
    //float lj2_trk = sub_leading_lrj->vrPt;
    lj2_pT = sub_leading_lrj->Pt();
    lj2_eta = sub_leading_lrj->Eta();
    lj2_phi = sub_leading_lrj->Phi();
    subleading_boson.SetPtEtaPhiM(lj2_pT,lj2_eta,lj2_phi,lj2_mass);

    #ifndef processdata
    lj2_truthS_pt = sub_leading_lrj->truthpt_S;
    lj2_truthS_mass = sub_leading_lrj->truthmass_S;
    lj2_truthS_eta = sub_leading_lrj->trutheta_S;
    lj2_truthS_phi = sub_leading_lrj->truthphi_S;

    lj2_truthH_pt = sub_leading_lrj->truthpt_H;
    lj2_truthH_mass = sub_leading_lrj->truthmass_H;
    lj2_truthH_eta = sub_leading_lrj->trutheta_H;
    lj2_truthH_phi = sub_leading_lrj->truthphi_H;

    truthS.SetPtEtaPhiM(lj2_truthS_pt,lj2_truthS_eta,lj2_truthS_phi,lj2_truthS_mass);
    truthH.SetPtEtaPhiM(lj2_truthH_pt,lj2_truthH_eta,lj2_truthH_phi,lj2_truthH_mass);
    #endif

  }// End of sub_leading_lrj!=0

  // Find closest jets to each truth particle
  //truthS.SetPtEtaPhiM(lj2_truthS_pt,lj2_truthS_eta,lj2_truthS_phi,lj2_truthS_mass);
  //truthH.SetPtEtaPhiM(lj2_truthH_pt,lj2_truthH_eta,lj2_truthH_phi,lj2_truthH_mass);

  // Check additional triggers for both large-R jets
  bool lrj1IsSelected = leading_lrj != 0 && leading_lrj->Pt()>450. && leading_lrj->M()>50.;
  bool lrj2IsSelected = sub_leading_lrj != 0 && sub_leading_lrj->Pt()>250. && leading_lrj->M()>50.;
  double mJJ = -999.;
  if(lrj1IsSelected && lrj2IsSelected){
    mJJ = (leading_boson + subleading_boson).M();
    mJJ_beforeTrig_pT450_M50->Fill(mJJ,totalWeight);
#ifdef trig13145
    if(is2018 && *trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig420_L1J100_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig390_L1J100_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig420_L1SC111_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j460_a10r_L1SC111 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig460_a10r_L1SC111_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j460_a10r_L1J100 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig460_a10r_L1J100_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j460_a10_lcw_subjes_L1SC111 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig460_a10_subjes_L1SC111_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j460_a10_lcw_subjes_L1J100 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig460_a10_subjes_L1J100_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j460_a10t_lcw_jes_L1SC111 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig460_a10t_L1SC111_pT450_M50->Fill(mJJ,totalWeight);
    }
    if(is2018 && *trigPassed_HLT_j460_a10t_lcw_jes_L1J100 == 1){ //Beginning of the trigger MC20E
      mJJ_afterTrig460_a10t_L1J100_pT450_M50->Fill(mJJ,totalWeight);
    }
#endif
    
  }//end of largeR selected

  DeltaR_truthB_S = -1;
  DeltaR_truthB_H = -1;


  //Event Selection
  //1. Trigger selection
#ifdef trig13167
  if(*trigPassed_HLT_j360_a10_lcw_sub_L1J100 == 1 || *trigPassed_HLT_j420_a10_lcw_L1J100 == 1){ // TRIG MC20A
#endif
#ifdef trig13144
  if(*trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100 == 1){ //Beginning of the trigger MC20D
#endif
#ifdef trig13145
  if(is2018 && *trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100 == 1){ //Beginning of the trigger MC20E
#endif


bool isSignal = false;
//// ***** here just ufo jets truth match such that we could compare it to the lctopo jets ****** /////
/*if(leading_lrj_ufo != 0 && sub_leading_lrj_ufo != 0){
  if(leading_lrj_ufo->Pt()>450. && sub_leading_lrj_ufo->Pt()>250. && leading_lrj_ufo->M()>50. && sub_leading_lrj_ufo->M()>50.){
// ***** Truth matching for UFO jets! ********* //
        int lrj1_ufo_truthlabel =-1;
        int lrj2_ufo_truthlabel =-1;
        #ifndef processdata
        // Truth matching part where not needed for data. I only care for this SH->4b signals
        isSignal = (*DSID>=801577 && *DSID<=801648) || (*DSID>=801888 && *DSID<=801911) ;
        if(isSignal){
          DeltaR_truthB_S = truthBjets[0].DeltaR(truthBjets[1]);
          Truth_b_S_deltaR->Fill(DeltaR_truthB_S,totalWeight);
          DeltaR_truthB_H = truthBjets_H[0].DeltaR(truthBjets_H[1]);
          Truth_b_H_deltaR->Fill(DeltaR_truthB_H,totalWeight);
        }
          lrj1_ufo_truthlabel = leading_lrj_ufo->truthlabel; 
          lrj2_ufo_truthlabel = sub_leading_lrj_ufo->truthlabel; 
          bool leading_lrj_ufo_isS = false;
          bool sub_leading_lrj_ufo_isS = false;
          //std::cout <<"dr:"<<leading_boson.DeltaR(truthS)<<" dr1: "<<leading_lrj_ufo->DeltaR(truthS)<<std::endl;
          if (leading_lrj_ufo->DeltaR(truthS) < sub_leading_lrj_ufo->DeltaR(truthS)){ //leading is closer to truth-S
            dR_truthS_reco_lrj = leading_lrj_ufo->DeltaR(truthS);
            leading_lrj_ufo_isS = true;
          } else {//subleading is closer to truth-S
            dR_truthS_reco_lrj = sub_leading_lrj_ufo->DeltaR(truthS);
            sub_leading_lrj_ufo_isS = true;
          }
          deltaR_truthS_reco_lrj->Fill(dR_truthS_reco_lrj,totalWeight);

          bool leading_lrj_ufo_isH = false;
          bool sub_leading_lrj_ufo_isH = false;
          //std::cout <<"dr:"<<leading_boson.DeltaR(truthS)<<std::endl;
  	  if (leading_lrj_ufo->DeltaR(truthH) < sub_leading_lrj_ufo->DeltaR(truthH)){ //leading is closer to truth-S
     	    dR_truthH_reco_lrj = leading_lrj_ufo->DeltaR(truthH);
     	    leading_lrj_ufo_isH = true;
  	  } else {//subleading is closer to truth-S
     	    dR_truthH_reco_lrj = sub_leading_lrj_ufo->DeltaR(truthH);
     	    sub_leading_lrj_ufo_isH = true;
  	  }
          deltaR_truthH_reco_lrj->Fill(dR_truthH_reco_lrj,totalWeight);
          
          TLorentzVector Scand_tmatch, Hcand_tmatch, Xcand_tmatch;
          TLorentzVector Scand_tmatch_btag, Hcand_tmatch_btag, Xcand_tmatch_btag;

          if(dR_truthS_reco_lrj < 0.5 && isSignal){//Truth matching for S
            if (leading_lrj_ufo_isS){
              Scand_tmatch.SetPtEtaPhiM(leading_lrj_ufo->Pt(),leading_lrj_ufo->Eta(),leading_lrj_ufo->Phi(),leading_lrj_ufo->M());
              LRJet_mass_tmatch_S->Fill(Scand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_S->Fill(Scand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_S->Fill(Scand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_S->Fill(Scand_tmatch.Phi(),totalWeight);

              if(leading_lrj_ufo->n_b_DL1r77>=2){
                Scand_tmatch_btag.SetPtEtaPhiM(leading_lrj_ufo->Pt(),leading_lrj_ufo->Eta(),leading_lrj_ufo->Phi(),leading_lrj_ufo->M());
                LRJet_mass_tmatch_btag_S->Fill(Scand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_S->Fill(Scand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_S->Fill(Scand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_S->Fill(Scand_tmatch_btag.Phi(),totalWeight);
              }
              //if(lrj1_truthlabel!=8){// I could not apply truth largeR jet label for S boson because, truth label has 8=qcd, 9=Hbb(with pdgid=25 and ghostbhadronfinalcount being 2) and 10=other_From_H(Hqq).
              //Therefore when label is applied for the large-R jets matched to S is empty!!!
              if(leading_lrj_ufo->truthlabel ==9 || leading_lrj_ufo->truthlabel==10){
                LRJet_mass_tmatch_S_lrjlabel->Fill(leading_lrj_ufo->truthlabel,totalWeight);
                //LRJet_mass_tmatch_S_lrjlabel->Fill(leading_lrj_ufo->M(),totalWeight);
                LRJet_pt_tmatch_S_lrjlabel->Fill(leading_lrj_ufo->Pt(),totalWeight);
                LRJet_eta_tmatch_S_lrjlabel->Fill(leading_lrj_ufo->Eta(),totalWeight);
                LRJet_phi_tmatch_S_lrjlabel->Fill(leading_lrj_ufo->Phi(),totalWeight);
              }
            }else if (sub_leading_lrj_ufo_isS){
              Scand_tmatch.SetPtEtaPhiM(sub_leading_lrj_ufo->Pt(),sub_leading_lrj_ufo->Eta(),sub_leading_lrj_ufo->Phi(),sub_leading_lrj_ufo->M());
              LRJet_mass_tmatch_S->Fill(Scand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_S->Fill(Scand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_S->Fill(Scand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_S->Fill(Scand_tmatch.Phi(),totalWeight);

              if(sub_leading_lrj_ufo->n_b_DL1r77>=2){
                Scand_tmatch_btag.SetPtEtaPhiM(sub_leading_lrj_ufo->Pt(),sub_leading_lrj_ufo->Eta(),sub_leading_lrj_ufo->Phi(),sub_leading_lrj_ufo->M());
                LRJet_mass_tmatch_btag_S->Fill(Scand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_S->Fill(Scand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_S->Fill(Scand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_S->Fill(Scand_tmatch_btag.Phi(),totalWeight);
              }
              if(lrj2_truthlabel!=8){
              //if(lrj2_truthlabel){
                LRJet_mass_tmatch_S_lrjlabel->Fill(sub_leading_lrj_ufo->M(),totalWeight);
                LRJet_pt_tmatch_S_lrjlabel->Fill(sub_leading_lrj_ufo->Pt(),totalWeight);
                LRJet_eta_tmatch_S_lrjlabel->Fill(sub_leading_lrj_ufo->Eta(),totalWeight);
                LRJet_phi_tmatch_S_lrjlabel->Fill(sub_leading_lrj_ufo->Phi(),totalWeight);
              }
            }
          }//Close for truth matching for S 
          if(dR_truthH_reco_lrj < 0.5 && isSignal){//Truth matching for S and H
            //Fill histo
            if(leading_lrj_ufo_isH){
              Hcand_tmatch.SetPtEtaPhiM(leading_lrj_ufo->Pt(),leading_lrj_ufo->Eta(),leading_lrj_ufo->Phi(),leading_lrj_ufo->M());
              LRJet_mass_tmatch_H->Fill(Hcand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_H->Fill(Hcand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_H->Fill(Hcand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_H->Fill(Hcand_tmatch.Phi(),totalWeight);
              if(leading_lrj_ufo->n_b_DL1r77>=2){
                Hcand_tmatch_btag.SetPtEtaPhiM(leading_lrj_ufo->Pt(),leading_lrj_ufo->Eta(),leading_lrj_ufo->Phi(),leading_lrj_ufo->M());
                LRJet_mass_tmatch_btag_H->Fill(Hcand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_H->Fill(Hcand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_H->Fill(Hcand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_H->Fill(Hcand_tmatch_btag.Phi(),totalWeight);
              }
              if(lrj1_truthlabel!=8){
                LRJet_mass_tmatch_H_lrjlabel->Fill(leading_lrj_ufo->M(),totalWeight);
                LRJet_pt_tmatch_H_lrjlabel->Fill(leading_lrj_ufo->Pt(),totalWeight);
                LRJet_eta_tmatch_H_lrjlabel->Fill(leading_lrj_ufo->Eta(),totalWeight);
                LRJet_phi_tmatch_H_lrjlabel->Fill(leading_lrj_ufo->Phi(),totalWeight);
              }
            }else if(sub_leading_lrj_ufo_isH){
              Hcand_tmatch.SetPtEtaPhiM(sub_leading_lrj_ufo->Pt(),sub_leading_lrj_ufo->Eta(),sub_leading_lrj_ufo->Phi(),sub_leading_lrj_ufo->M());

              LRJet_mass_tmatch_H->Fill(Hcand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_H->Fill(Hcand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_H->Fill(Hcand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_H->Fill(Hcand_tmatch.Phi(),totalWeight);

              if(sub_leading_lrj_ufo->n_b_DL1r77>=2){
                Hcand_tmatch_btag.SetPtEtaPhiM(sub_leading_lrj_ufo->Pt(),sub_leading_lrj_ufo->Eta(),sub_leading_lrj_ufo->Phi(),sub_leading_lrj_ufo->M());
                LRJet_mass_tmatch_btag_H->Fill(Hcand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_H->Fill(Hcand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_H->Fill(Hcand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_H->Fill(Hcand_tmatch_btag.Phi(),totalWeight);
              }
              if(lrj2_truthlabel!=8){
                LRJet_mass_tmatch_H_lrjlabel->Fill(sub_leading_lrj_ufo->M(),totalWeight);
                LRJet_pt_tmatch_H_lrjlabel->Fill(sub_leading_lrj_ufo->Pt(),totalWeight);
                LRJet_eta_tmatch_H_lrjlabel->Fill(sub_leading_lrj_ufo->Eta(),totalWeight);
                LRJet_phi_tmatch_H_lrjlabel->Fill(sub_leading_lrj_ufo->Phi(),totalWeight);
              }
            } // if(truthlabel)

          }//Close for truth matching for H
          if(dR_truthS_reco_lrj < 0.5 && dR_truthH_reco_lrj < 0.5 ){//Truth matching for S and H
            if( (leading_lrj_ufo_isS && sub_leading_lrj_ufo_isH) || (leading_lrj_ufo_isH && sub_leading_lrj_ufo_isS) ){
              Xcand_tmatch = Hcand_tmatch + Scand_tmatch;
              Xcand_mass_tmatch->Fill(Xcand_tmatch.M(),totalWeight);
              Xcand_pT_tmatch->Fill(Xcand_tmatch.Pt(),totalWeight);
              Xcand_eta_tmatch->Fill(Xcand_tmatch.Eta(),totalWeight);
              Xcand_phi_tmatch->Fill(Xcand_tmatch.Phi(),totalWeight);
              Xcand_rapidity_tmatch->Fill(X_cand.Rapidity(),totalWeight);
              mSvsmX_tmatch->Fill(Xcand_tmatch.M(),Scand_tmatch.M(),totalWeight);
              mHvsmX_tmatch->Fill(Xcand_tmatch.M(),Hcand_tmatch.M(),totalWeight);
              mSvsmH_tmatch->Fill(Scand_tmatch.M(),Hcand_tmatch.M(),totalWeight);
              // with b-tagged
              if(leading_lrj_ufo->n_b_DL1r77>=2 && sub_leading_lrj_ufo->n_b_DL1r77>=2){
                Xcand_tmatch_btag = Hcand_tmatch_btag + Scand_tmatch_btag;
                Xcand_mass_tmatch_btag->Fill((Hcand_tmatch_btag+Scand_tmatch_btag).M(),totalWeight);
                Xcand_pT_tmatch_btag->Fill(Xcand_tmatch_btag.Pt(),totalWeight);
                Xcand_eta_tmatch_btag->Fill(Xcand_tmatch_btag.Eta(),totalWeight);
                Xcand_phi_tmatch_btag->Fill(Xcand_tmatch_btag.Phi(),totalWeight);
                Xcand_rapidity_tmatch_btag->Fill(X_cand.Rapidity(),totalWeight);
                mSvsmX_tmatch_btag->Fill(Xcand_tmatch_btag.M(),Scand_tmatch_btag.M(),totalWeight);
                mHvsmX_tmatch_btag->Fill(Xcand_tmatch_btag.M(),Hcand_tmatch_btag.M(),totalWeight);
                mSvsmH_tmatch_btag->Fill(Scand_tmatch_btag.M(),Hcand_tmatch_btag.M(),totalWeight);
              }
            }
          }//End for the dr or 
        // End for truth match!!!!! 
        #endif
    
  }//End of kinematics cuts for ufo
}//End of selecting at least 2 leading ufo lrjets

*/
  
  //if(leading_lrj != 0 ){ //Beginning of the trigger MC20E
    #ifndef processdata
      if(truth_lrj1 !=0 && truth_lrj2!=0){
        if(truth_lrj1->Pt()>450. && truth_lrj2->Pt()>250){
          truthleadfatjet_pT->Fill(truth_lrj1->Pt(),totalWeight);
          truthleadfatjet_eta->Fill(truth_lrj1->Eta(),totalWeight);
          truthleadfatjet_phi->Fill(truth_lrj1->Phi(),totalWeight);
          truthleadfatjet_mass->Fill(truth_lrj1->M(),totalWeight);
          truthsubleadfatjet_pT->Fill(truth_lrj2->Pt(),totalWeight);
          truthsubleadfatjet_eta->Fill(truth_lrj2->Eta(),totalWeight);
          truthsubleadfatjet_phi->Fill(truth_lrj2->Phi(),totalWeight);
          truthsubleadfatjet_mass->Fill(truth_lrj2->M(),totalWeight);
        }
      }
    #endif

    X_cand = leading_boson + subleading_boson;
   //2. Requiring at least 2 large-R jets  
    if(leading_lrj != 0 && sub_leading_lrj != 0 ){//At least two large-R jet
      Cutflow->Fill(2.0,totalWeight);//at least two large-R jet trigger efficiency
    
      Ratio_2m_pt = 2*leading_lrj->M()/leading_lrj->Pt();
      //3. Kinematic cuts on lrjets.
      if(leading_lrj->Pt()>450. && sub_leading_lrj->Pt()>250. && leading_lrj->M()>50. && sub_leading_lrj->M()>50.){// Kinematic cuts for large-R jets
        Cutflow->Fill(3.0,totalWeight);//At least 2 lrj with pt req.
        AtLeast2lrj->Fill(0.0,totalWeight);
// ***** Truth matching for LCTopo jets! ********* //
        int lrj1_truthlabel =-1;
        int lrj2_truthlabel =-1;
        #ifndef processdata
        // Truth matching part where not needed for data. I only care for this SH->4b signals
        isSignal = (*DSID>=801577 && *DSID<=801648) || (*DSID>=801888 && *DSID<=801911) ;
        if(isSignal){
          DeltaR_truthB_S = truthBjets[0].DeltaR(truthBjets[1]);
          Truth_b_S_deltaR->Fill(DeltaR_truthB_S,totalWeight);
          DeltaR_truthB_H = truthBjets_H[0].DeltaR(truthBjets_H[1]);
          Truth_b_H_deltaR->Fill(DeltaR_truthB_H,totalWeight);
        }
          lrj1_truthlabel = leading_lrj->truthlabel; 
          lrj2_truthlabel = sub_leading_lrj->truthlabel; 
          bool leading_lrj_isS = false;
          bool sub_leading_lrj_isS = false;
          //std::cout <<"dr:"<<leading_boson.DeltaR(truthS)<<" dr1: "<<leading_lrj->DeltaR(truthS)<<std::endl;
          if (leading_lrj->DeltaR(truthS) < sub_leading_lrj->DeltaR(truthS)){ //leading is closer to truth-S
            dR_truthS_reco_lrj = leading_lrj->DeltaR(truthS);
            leading_lrj_isS = true;
          } else {//subleading is closer to truth-S
            dR_truthS_reco_lrj = sub_leading_lrj->DeltaR(truthS);
            sub_leading_lrj_isS = true;
          }
          deltaR_truthS_reco_lrj->Fill(dR_truthS_reco_lrj,totalWeight);

          bool leading_lrj_isH = false;
          bool sub_leading_lrj_isH = false;
          //std::cout <<"dr:"<<leading_boson.DeltaR(truthS)<<std::endl;
  	  if (leading_lrj->DeltaR(truthH) < sub_leading_lrj->DeltaR(truthH)){ //leading is closer to truth-S
     	    dR_truthH_reco_lrj = leading_lrj->DeltaR(truthH);
     	    leading_lrj_isH = true;
  	  } else {//subleading is closer to truth-S
     	    dR_truthH_reco_lrj = sub_leading_lrj->DeltaR(truthH);
     	    sub_leading_lrj_isH = true;
  	  }
          deltaR_truthH_reco_lrj->Fill(dR_truthH_reco_lrj,totalWeight);
          
          TLorentzVector Scand_tmatch, Hcand_tmatch, Xcand_tmatch;
          TLorentzVector Scand_tmatch_btag, Hcand_tmatch_btag, Xcand_tmatch_btag;

          if(dR_truthS_reco_lrj < 0.5 && isSignal){//Truth matching for S
            if (leading_lrj_isS){
              Scand_tmatch.SetPtEtaPhiM(leading_lrj->Pt(),leading_lrj->Eta(),leading_lrj->Phi(),leading_lrj->M());
              LRJet_mass_tmatch_S->Fill(Scand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_S->Fill(Scand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_S->Fill(Scand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_S->Fill(Scand_tmatch.Phi(),totalWeight);

              if(leading_lrj->n_b_DL1r77>=2){
                Scand_tmatch_btag.SetPtEtaPhiM(leading_lrj->Pt(),leading_lrj->Eta(),leading_lrj->Phi(),leading_lrj->M());
                LRJet_mass_tmatch_btag_S->Fill(Scand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_S->Fill(Scand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_S->Fill(Scand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_S->Fill(Scand_tmatch_btag.Phi(),totalWeight);
              }
              //if(lrj1_truthlabel!=8){// I could not apply truth largeR jet label for S boson because, truth label has 8=qcd, 9=Hbb(with pdgid=25 and ghostbhadronfinalcount being 2) and 10=other_From_H(Hqq).
              //Therefore when label is applied for the large-R jets matched to S is empty!!!
              if(leading_lrj->truthlabel ==9 || leading_lrj->truthlabel==10){
                LRJet_mass_tmatch_S_lrjlabel->Fill(leading_lrj->truthlabel,totalWeight);
                //LRJet_mass_tmatch_S_lrjlabel->Fill(leading_lrj->M(),totalWeight);
                LRJet_pt_tmatch_S_lrjlabel->Fill(leading_lrj->Pt(),totalWeight);
                LRJet_eta_tmatch_S_lrjlabel->Fill(leading_lrj->Eta(),totalWeight);
                LRJet_phi_tmatch_S_lrjlabel->Fill(leading_lrj->Phi(),totalWeight);
              }
            }else if (sub_leading_lrj_isS){
              Scand_tmatch.SetPtEtaPhiM(sub_leading_lrj->Pt(),sub_leading_lrj->Eta(),sub_leading_lrj->Phi(),sub_leading_lrj->M());
              LRJet_mass_tmatch_S->Fill(Scand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_S->Fill(Scand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_S->Fill(Scand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_S->Fill(Scand_tmatch.Phi(),totalWeight);

              if(sub_leading_lrj->n_b_DL1r77>=2){
                Scand_tmatch_btag.SetPtEtaPhiM(sub_leading_lrj->Pt(),sub_leading_lrj->Eta(),sub_leading_lrj->Phi(),sub_leading_lrj->M());
                LRJet_mass_tmatch_btag_S->Fill(Scand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_S->Fill(Scand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_S->Fill(Scand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_S->Fill(Scand_tmatch_btag.Phi(),totalWeight);
              }
              if(lrj2_truthlabel!=8){
              //if(lrj2_truthlabel){
                LRJet_mass_tmatch_S_lrjlabel->Fill(sub_leading_lrj->M(),totalWeight);
                LRJet_pt_tmatch_S_lrjlabel->Fill(sub_leading_lrj->Pt(),totalWeight);
                LRJet_eta_tmatch_S_lrjlabel->Fill(sub_leading_lrj->Eta(),totalWeight);
                LRJet_phi_tmatch_S_lrjlabel->Fill(sub_leading_lrj->Phi(),totalWeight);
              }
            }
          }//Close for truth matching for S 
          if(dR_truthH_reco_lrj < 0.5 && isSignal){//Truth matching for S and H
            //Fill histo
            if(leading_lrj_isH){
              Hcand_tmatch.SetPtEtaPhiM(leading_lrj->Pt(),leading_lrj->Eta(),leading_lrj->Phi(),leading_lrj->M());
              LRJet_mass_tmatch_H->Fill(Hcand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_H->Fill(Hcand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_H->Fill(Hcand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_H->Fill(Hcand_tmatch.Phi(),totalWeight);
              if(leading_lrj->n_b_DL1r77>=2){
                Hcand_tmatch_btag.SetPtEtaPhiM(leading_lrj->Pt(),leading_lrj->Eta(),leading_lrj->Phi(),leading_lrj->M());
                LRJet_mass_tmatch_btag_H->Fill(Hcand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_H->Fill(Hcand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_H->Fill(Hcand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_H->Fill(Hcand_tmatch_btag.Phi(),totalWeight);
              }
              if(lrj1_truthlabel!=8){
                LRJet_mass_tmatch_H_lrjlabel->Fill(leading_lrj->M(),totalWeight);
                LRJet_pt_tmatch_H_lrjlabel->Fill(leading_lrj->Pt(),totalWeight);
                LRJet_eta_tmatch_H_lrjlabel->Fill(leading_lrj->Eta(),totalWeight);
                LRJet_phi_tmatch_H_lrjlabel->Fill(leading_lrj->Phi(),totalWeight);
              }
            }else if(sub_leading_lrj_isH){
              Hcand_tmatch.SetPtEtaPhiM(sub_leading_lrj->Pt(),sub_leading_lrj->Eta(),sub_leading_lrj->Phi(),sub_leading_lrj->M());

              LRJet_mass_tmatch_H->Fill(Hcand_tmatch.M(),totalWeight);
              LRJet_pt_tmatch_H->Fill(Hcand_tmatch.Pt(),totalWeight);
              LRJet_eta_tmatch_H->Fill(Hcand_tmatch.Eta(),totalWeight);
              LRJet_phi_tmatch_H->Fill(Hcand_tmatch.Phi(),totalWeight);

              if(sub_leading_lrj->n_b_DL1r77>=2){
                Hcand_tmatch_btag.SetPtEtaPhiM(sub_leading_lrj->Pt(),sub_leading_lrj->Eta(),sub_leading_lrj->Phi(),sub_leading_lrj->M());
                LRJet_mass_tmatch_btag_H->Fill(Hcand_tmatch_btag.M(),totalWeight);
                LRJet_pt_tmatch_btag_H->Fill(Hcand_tmatch_btag.Pt(),totalWeight);
                LRJet_eta_tmatch_btag_H->Fill(Hcand_tmatch_btag.Eta(),totalWeight);
                LRJet_phi_tmatch_btag_H->Fill(Hcand_tmatch_btag.Phi(),totalWeight);
              }
              if(lrj2_truthlabel!=8){
                LRJet_mass_tmatch_H_lrjlabel->Fill(sub_leading_lrj->M(),totalWeight);
                LRJet_pt_tmatch_H_lrjlabel->Fill(sub_leading_lrj->Pt(),totalWeight);
                LRJet_eta_tmatch_H_lrjlabel->Fill(sub_leading_lrj->Eta(),totalWeight);
                LRJet_phi_tmatch_H_lrjlabel->Fill(sub_leading_lrj->Phi(),totalWeight);
              }
            } // if(truthlabel)

          }//Close for truth matching for H
          if(dR_truthS_reco_lrj < 0.5 && dR_truthH_reco_lrj < 0.5 ){//Truth matching for S and H
            if( (leading_lrj_isS && sub_leading_lrj_isH) || (leading_lrj_isH && sub_leading_lrj_isS) ){
              Xcand_tmatch = Hcand_tmatch + Scand_tmatch;
              Xcand_mass_tmatch->Fill(Xcand_tmatch.M(),totalWeight);
              Xcand_pT_tmatch->Fill(Xcand_tmatch.Pt(),totalWeight);
              Xcand_eta_tmatch->Fill(Xcand_tmatch.Eta(),totalWeight);
              Xcand_phi_tmatch->Fill(Xcand_tmatch.Phi(),totalWeight);
              Xcand_rapidity_tmatch->Fill(X_cand.Rapidity(),totalWeight);
              mSvsmX_tmatch->Fill(Xcand_tmatch.M(),Scand_tmatch.M(),totalWeight);
              mHvsmX_tmatch->Fill(Xcand_tmatch.M(),Hcand_tmatch.M(),totalWeight);
              mSvsmH_tmatch->Fill(Scand_tmatch.M(),Hcand_tmatch.M(),totalWeight);
              // with b-tagged
              if(leading_lrj->n_b_DL1r77>=2 && sub_leading_lrj->n_b_DL1r77>=2){
                Xcand_tmatch_btag = Hcand_tmatch_btag + Scand_tmatch_btag;
                Xcand_mass_tmatch_btag->Fill((Hcand_tmatch_btag+Scand_tmatch_btag).M(),totalWeight);
                Xcand_pT_tmatch_btag->Fill(Xcand_tmatch_btag.Pt(),totalWeight);
                Xcand_eta_tmatch_btag->Fill(Xcand_tmatch_btag.Eta(),totalWeight);
                Xcand_phi_tmatch_btag->Fill(Xcand_tmatch_btag.Phi(),totalWeight);
                Xcand_rapidity_tmatch_btag->Fill(X_cand.Rapidity(),totalWeight);
                mSvsmX_tmatch_btag->Fill(Xcand_tmatch_btag.M(),Scand_tmatch_btag.M(),totalWeight);
                mHvsmX_tmatch_btag->Fill(Xcand_tmatch_btag.M(),Hcand_tmatch_btag.M(),totalWeight);
                mSvsmH_tmatch_btag->Fill(Scand_tmatch_btag.M(),Hcand_tmatch_btag.M(),totalWeight);
              }
            }
          }//End for the dr or 
        // End for truth match!!!!! 
        #endif
        Xcand_mass->Fill(X_cand.M(),totalWeight);
        Xcand_pT->Fill(X_cand.Pt(),totalWeight);
        Xcand_eta->Fill(X_cand.Eta(),totalWeight);
        Xcand_phi->Fill(X_cand.Phi(),totalWeight);
        Xcand_rapidity->Fill(X_cand.Rapidity(),totalWeight);
        
        LRJet1_mass->Fill(leading_lrj->M(),totalWeight);
        LRJet1_pt->Fill(leading_lrj->Pt(),totalWeight);
        LRJet1_eta->Fill(leading_lrj->Eta(),totalWeight);
        LRJet1_phi->Fill(leading_lrj->Phi(),totalWeight);
 
        LRJet2_mass->Fill(sub_leading_lrj->M(),totalWeight);
        LRJet2_pt->Fill(sub_leading_lrj->Pt(),totalWeight);
        LRJet2_eta->Fill(sub_leading_lrj->Eta(),totalWeight);
        LRJet2_phi->Fill(sub_leading_lrj->Phi(),totalWeight);

        // Here I did not assigned the S and H-candidates!!!!!!!!! 
        // Start of b-tagging before S or H. Note: this is not my SR!
        if (leading_lrj->n_b_DL1r77>=2 && sub_leading_lrj->n_b_DL1r77>=2){//4B region where both large-R jet required to have 2b-s
          Xcand_mass_btag->Fill(X_cand.M(),totalWeight);
          Xcand_pT_btag->Fill(X_cand.Pt(),totalWeight);
          Xcand_eta_btag->Fill(X_cand.Eta(),totalWeight);
          Xcand_phi_btag->Fill(X_cand.Phi(),totalWeight);
          //Xcand_rapidity_btag->Fill(X_cand.Rapidity(),totalWeight);
        
          LRJet1_mass_btag->Fill(leading_lrj->M(),totalWeight);
          LRJet1_pt_btag->Fill(leading_lrj->Pt(),totalWeight);
          LRJet1_eta_btag->Fill(leading_lrj->Eta(),totalWeight);
          LRJet1_phi_btag->Fill(leading_lrj->Phi(),totalWeight);
 
          LRJet2_mass_btag->Fill(sub_leading_lrj->M(),totalWeight);
          LRJet2_pt_btag->Fill(sub_leading_lrj->Pt(),totalWeight);
          LRJet2_eta_btag->Fill(sub_leading_lrj->Eta(),totalWeight);
          LRJet2_phi_btag->Fill(sub_leading_lrj->Phi(),totalWeight);
          LRJet1mass_vs_LRJet2mass_btag->Fill(leading_lrj->M(),sub_leading_lrj->M(),totalWeight);
          ratio_2m_pt_btag->Fill(Ratio_2m_pt,totalWeight);
      
          deltaR_truthS_reco_lrj_btag->Fill(dR_truthS_reco_lrj,totalWeight);
          deltaR_truthH_reco_lrj_btag->Fill(dR_truthH_reco_lrj,totalWeight);
        }// End of b-tag before s and h 
     
       //4. Assign the S or H candidate
  
       //double mass_H = -999.;
       //double mass_S = -999.;
       fatjet * Hcand = 0;
       fatjet * Scand = 0;

      if(leading_lrj->inHwindow() && !sub_leading_lrj->inHwindow()){
        Hcand = leading_lrj;
        H_cand = leading_boson;
        Scand = sub_leading_lrj;
        S_cand = subleading_boson;
      }else if(sub_leading_lrj->inHwindow() && !leading_lrj->inHwindow()){
        Hcand = sub_leading_lrj;
        H_cand = subleading_boson;
        Scand = leading_lrj;
        S_cand = leading_boson;
      }else if(leading_lrj->inHwindow() && sub_leading_lrj->inHwindow() ){
        //This following line seeds the pseudo-random number generator
        //used by rand() with the value seed! 
        srand(evtNumber);

        if((double) rand()/(RAND_MAX+1.0)>0.5){
          Hcand = leading_lrj;
          H_cand = leading_boson;
          Scand = sub_leading_lrj;
          S_cand = subleading_boson;
        }else{
          Hcand = sub_leading_lrj;
          H_cand = subleading_boson;
          Scand = leading_lrj;
          S_cand = leading_boson;
        }
        //std::cout<<"TESTING: H-cand mass: "<<Hcand->M()<< ", S mass: "<<Scand->M()<<std::endl;
      }else if(!leading_lrj->inHwindow() && !sub_leading_lrj->inHwindow() ){
       Hcand = 0;
       Scand = 0;
      }

       // End were S and H candidates assignment
       rapidity_SH= abs(S_cand.Rapidity()-H_cand.Rapidity());
       DR_SH = S_cand.DeltaR(H_cand);
       DPhi_SH = abs(S_cand.DeltaPhi(H_cand));
       DEta_SH = abs(H_cand.Eta()-S_cand.Eta());
      //Filling histograms after S and H assignment. 
      if( leading_lrj->inHwindow() || sub_leading_lrj->inHwindow() ){//Always require to have one of the large-R jets being matched to Higgs mass window
        Cutflow->Fill(4.0,totalWeight);//assignment of S and H; no b-tag. 
        Hcand_mass->Fill(Hcand->M(),totalWeight);
        Hcand_pT->Fill(Hcand->Pt(),totalWeight);
        Hcand_eta->Fill(Hcand->Eta(),totalWeight);
        Hcand_phi->Fill(Hcand->Phi(),totalWeight);

        Scand_mass->Fill(Scand->M(),totalWeight);
        Scand_pT->Fill(Scand->Pt(),totalWeight);
        Scand_eta->Fill(Scand->Eta(),totalWeight);
        Scand_phi->Fill(Scand->Phi(),totalWeight);
        //Scand_lrjlabel->Fill(Scand->truthlabel,totalWeight);

        Xcand_mass_afterSorH->Fill(X_cand.M(),totalWeight);
        Xcand_pT_afterSorH->Fill(X_cand.Pt(),totalWeight);
        Xcand_eta_afterSorH->Fill(X_cand.Eta(),totalWeight);
        Xcand_phi_afterSorH->Fill(X_cand.Phi(),totalWeight);

        mHvsmX->Fill(X_cand.M(),Hcand->M(),totalWeight);
        mSvsmX->Fill(X_cand.M(),Scand->M(),totalWeight);
        mSvsmH->Fill(Scand->M(),Hcand->M(),totalWeight);

        dR_SH_afterSorH->Fill(DR_SH,totalWeight);
        dEta_SH_afterSorH->Fill(DEta_SH,totalWeight);
        dPhi_SH_afterSorH->Fill(DPhi_SH,totalWeight);
        dRapidity_SH_afterSorH->Fill(rapidity_SH,totalWeight);

        dR_SHvsmS->Fill(DR_SH,Scand->M(),totalWeight);
        dR_SHvsmH->Fill(DR_SH,Hcand->M(),totalWeight);
        dEta_SHvsmS->Fill(DEta_SH,Scand->M(),totalWeight);
        dEta_SHvsmH->Fill(DEta_SH,Hcand->M(),totalWeight);
        dPhi_SHvsmS->Fill(DPhi_SH,Scand->M(),totalWeight);
        dPhi_SHvsmH->Fill(DPhi_SH,Hcand->M(),totalWeight);
        dRapidity_SHvsmS->Fill(rapidity_SH,Scand->M(),totalWeight);
        dRapidity_SHvsmH->Fill(rapidity_SH,Hcand->M(),totalWeight);
        ratio_2m_pt_afterSorH->Fill(Ratio_2m_pt,totalWeight);
        //Trying with 2B region where only Higgs decay to bb
        if(leading_lrj->inHwindow() && leading_lrj->n_b_DL1r77>=2){
        Cutflow->Fill(5.0,totalWeight);//at least 2 b-tag DL1r per large-R jet 
          Hcand_mass_2b->Fill(leading_lrj->M(),totalWeight);
          Hcand_pT_2b->Fill(leading_lrj->Pt(),totalWeight);
          Hcand_eta_2b->Fill(leading_lrj->Eta(),totalWeight);
          Hcand_phi_2b->Fill(leading_lrj->Phi(),totalWeight);

          Scand_mass_2b->Fill(sub_leading_lrj->M(),totalWeight);
          Scand_pT_2b->Fill(sub_leading_lrj->Pt(),totalWeight);
          Scand_eta_2b->Fill(sub_leading_lrj->Eta(),totalWeight);
          Scand_phi_2b->Fill(sub_leading_lrj->Phi(),totalWeight);

          Xcand_mass_afterSorH_2b->Fill(X_cand.M(),totalWeight);
          Xcand_pT_afterSorH_2b->Fill(X_cand.Pt(),totalWeight);
          Xcand_eta_afterSorH_2b->Fill(X_cand.Eta(),totalWeight);
          Xcand_phi_afterSorH_2b->Fill(X_cand.Phi(),totalWeight);

          mHvsmX_2b->Fill(X_cand.M(),leading_lrj->M(),totalWeight);
          mSvsmX_2b->Fill(X_cand.M(),sub_leading_lrj->M(),totalWeight);
          mSvsmH_2b->Fill(sub_leading_lrj->M(),leading_lrj->M(),totalWeight);

          dR_SH_afterSorH_2b->Fill(DR_SH,totalWeight);
          dEta_SH_afterSorH_2b->Fill(DEta_SH,totalWeight);
          dPhi_SH_afterSorH_2b->Fill(DPhi_SH,totalWeight);
          dRapidity_SH_afterSorH_2b->Fill(rapidity_SH,totalWeight);

          dR_SHvsmS_2b->Fill(DR_SH,sub_leading_lrj->M(),totalWeight);
          dR_SHvsmH_2b->Fill(DR_SH,leading_lrj->M(),totalWeight);
          dEta_SHvsmS_2b->Fill(DEta_SH,sub_leading_lrj->M(),totalWeight);
          dEta_SHvsmH_2b->Fill(DEta_SH,leading_lrj->M(),totalWeight);
          dPhi_SHvsmS_2b->Fill(DPhi_SH,sub_leading_lrj->M(),totalWeight);
          dPhi_SHvsmH_2b->Fill(DPhi_SH,leading_lrj->M(),totalWeight);
          dRapidity_SHvsmS_2b->Fill(rapidity_SH,sub_leading_lrj->M(),totalWeight);
          dRapidity_SHvsmH_2b->Fill(rapidity_SH,leading_lrj->M(),totalWeight);
        }else if(sub_leading_lrj->inHwindow() && sub_leading_lrj->n_b_DL1r77>=2){
          Cutflow->Fill(5.0,totalWeight);//at least 2 b-tag DL1r per large-R jet 
          Hcand_mass_2b->Fill(sub_leading_lrj->M(),totalWeight);
          Hcand_pT_2b->Fill(sub_leading_lrj->Pt(),totalWeight);
          Hcand_eta_2b->Fill(sub_leading_lrj->Eta(),totalWeight);
          Hcand_phi_2b->Fill(sub_leading_lrj->Phi(),totalWeight);

          Scand_mass_2b->Fill(leading_lrj->M(),totalWeight);
          Scand_pT_2b->Fill(leading_lrj->Pt(),totalWeight);
          Scand_eta_2b->Fill(leading_lrj->Eta(),totalWeight);
          Scand_phi_2b->Fill(leading_lrj->Phi(),totalWeight);

          Xcand_mass_afterSorH_2b->Fill(X_cand.M(),totalWeight);
          Xcand_pT_afterSorH_2b->Fill(X_cand.Pt(),totalWeight);
          Xcand_eta_afterSorH_2b->Fill(X_cand.Eta(),totalWeight);
          Xcand_phi_afterSorH_2b->Fill(X_cand.Phi(),totalWeight);

          mHvsmX_2b->Fill(X_cand.M(),sub_leading_lrj->M(),totalWeight);
          mSvsmX_2b->Fill(X_cand.M(),leading_lrj->M(),totalWeight);
          mSvsmH_2b->Fill(Scand->M(),sub_leading_lrj->M(),totalWeight);

          dR_SH_afterSorH_2b->Fill(DR_SH,totalWeight);
          dEta_SH_afterSorH_2b->Fill(DEta_SH,totalWeight);
          dPhi_SH_afterSorH_2b->Fill(DPhi_SH,totalWeight);
          dRapidity_SH_afterSorH_2b->Fill(rapidity_SH,totalWeight);

          dR_SHvsmS_2b->Fill(DR_SH,leading_lrj->M(),totalWeight);
          dR_SHvsmH_2b->Fill(DR_SH,sub_leading_lrj->M(),totalWeight);
          dEta_SHvsmS_2b->Fill(DEta_SH,sub_leading_lrj->M(),totalWeight);
          dEta_SHvsmH_2b->Fill(DEta_SH,sub_leading_lrj->M(),totalWeight);
          dPhi_SHvsmS_2b->Fill(DPhi_SH,leading_lrj->M(),totalWeight);
          dPhi_SHvsmH_2b->Fill(DPhi_SH,sub_leading_lrj->M(),totalWeight);
          dRapidity_SHvsmS_2b->Fill(rapidity_SH,leading_lrj->M(),totalWeight);
          dRapidity_SHvsmH_2b->Fill(rapidity_SH,sub_leading_lrj->M(),totalWeight);
        }//End of 2B region


        
        //5. B-Tagging 4B region. This is SR!!!!!!!!!!!!!!!!
        //if (leading_lrj->n_b_DL1r77==2 && sub_leading_lrj->n_b_DL1r77==2){//4B region where both large-R jet required to have 2b-s
        // I would like to define here CR and VRs because is is after b-tagging but still within S and H candidates assignment. 
        // CR, VR are needed to be orthogonal to the SR.
        //Method-I for background estimation. Inspired by the CMS analysis. Detailed explanations here: https://codimd.web.cern.ch/7VbgjV_USPKGBm_26kEuGA?both
        //CR_FP
        //else if(DEta_SH<1.1){//consider moving this after S and H assignment before 4b-tagging
         //if (leading_lrj->n_b_DL1r77>=2 && sub_leading_lrj->n_b_DL1r77>=2) {}//4B region where both large-R jet required to have 2b-s
        //Define the WPs for DL1r
        bool CR_FP = false;
        bool CR_FF = false;
        bool VR_PF = false;
        bool VR_FP = false;
        bool SR_4b = false;
        bool CR_PF = false;


        if(Hcand->n_b_DL1r77>=2 && Scand->n_b_DL1r77>=2) SR_4b = true;
        else if(Hcand->n_b_DL1r77>=2 && Scand->n_b_DL1r77==0) CR_PF = true;
        //else if((Hcand->n_b_DL1r77>=2 && Hcand->n_b_DL1r85==0) && Scand->n_b_DL1r77>=2) VR_FP = true;
        //else if((Hcand->n_b_DL1r77>=2 && Hcand->n_b_DL1r85==0) && Scand->n_b_DL1r77==0) VR_PF = true;
        else if(Hcand->n_b_DL1r85_77>=2 && Scand->n_b_DL1r77>=2) VR_FP = true;
        else if(Hcand->n_b_DL1r85_77>=2 && Scand->n_b_DL1r77==0) VR_PF = true;
        else if(Hcand->n_b_DL1r85==0 && Scand->n_b_DL1r77>=2) CR_FP = true;
        else if(Hcand->n_b_DL1r85==0 && Scand->n_b_DL1r77==0) CR_FF = true;

//This is my signal region. So I will not look at the data. Only for MC until unblinding approval!!!!!!
#ifndef processdata
       if (SR_4b){//4B region where both large-R jet required to have 2b-s
       //if (leading_lrj->n_b_DL1r77>=2 && sub_leading_lrj->n_b_DL1r77>=2){//4B region where both large-R jet required to have 2b-s
          Xcand_mass_b77->Fill(X_cand.M(),totalWeight);
          Cutflow->Fill(6.0,totalWeight);//at least 2 b-tag DL1r per large-R jet 
          AtLeast2Bs_lrj->Fill(0.0,totalWeight);
          if(leading_lrj->inHwindow() && leading_lrj->isRatio()){
            Num_bjets_Higgs->Fill(leading_lrj->n_b_DL1r77,totalWeight);
            Num_bjets_S->Fill(sub_leading_lrj->n_b_DL1r77,totalWeight);
          }else if(sub_leading_lrj->inHwindow() && sub_leading_lrj->isRatio()){
            Num_bjets_Higgs->Fill(leading_lrj->n_b_DL1r77,totalWeight);
            Num_bjets_S->Fill(sub_leading_lrj->n_b_DL1r77,totalWeight);
          }
          Hcand_mass_4b->Fill(Hcand->M(),totalWeight);
          Hcand_pT_4b->Fill(Hcand->Pt(),totalWeight);
          Hcand_eta_4b->Fill(Hcand->Eta(),totalWeight);
          Hcand_phi_4b->Fill(Hcand->Phi(),totalWeight);
          Scand_mass_4b->Fill(Scand->M(),totalWeight);
          Scand_pT_4b->Fill(Scand->Pt(),totalWeight);
          Scand_eta_4b->Fill(Scand->Eta(),totalWeight);
          Scand_phi_4b->Fill(Scand->Phi(),totalWeight);
          Xcand_mass_afterSorH_4b->Fill(X_cand.M(),totalWeight);
          Xcand_pT_afterSorH_4b->Fill(X_cand.Pt(),totalWeight);
          Xcand_eta_afterSorH_4b->Fill(X_cand.Eta(),totalWeight);
          Xcand_phi_afterSorH_4b->Fill(X_cand.Phi(),totalWeight);

          mHvsmX_4b->Fill(X_cand.M(),Hcand->M(),totalWeight);
          mSvsmX_4b->Fill(X_cand.M(),Scand->M(),totalWeight);
          mSvsmH_4b->Fill(Scand->M(),Hcand->M(),totalWeight);

          dR_SH_afterSorH_4b->Fill(DR_SH,totalWeight);
          dEta_SH_afterSorH_4b->Fill(DEta_SH,totalWeight);
          dPhi_SH_afterSorH_4b->Fill(DPhi_SH,totalWeight);
          dRapidity_SH_afterSorH_4b->Fill(rapidity_SH,totalWeight);

          dR_SHvsmS_4b->Fill(DR_SH,Scand->M(),totalWeight);
          dR_SHvsmH_4b->Fill(DR_SH,Hcand->M(),totalWeight);
          dEta_SHvsmS_4b->Fill(DEta_SH,Scand->M(),totalWeight);
          dEta_SHvsmH_4b->Fill(DEta_SH,Hcand->M(),totalWeight);
          dPhi_SHvsmS_4b->Fill(DPhi_SH,Scand->M(),totalWeight);
          dPhi_SHvsmH_4b->Fill(DPhi_SH,Hcand->M(),totalWeight);
          dRapidity_SHvsmS_4b->Fill(rapidity_SH,Scand->M(),totalWeight);
          dRapidity_SHvsmH_4b->Fill(rapidity_SH,Hcand->M(),totalWeight);

          //deltaR_truthS_reco_lrj->Fill(dR_truthS_reco_lrj,totalWeight);
          //deltaR_truthH_reco_lrj->Fill(dR_truthH_reco_lrj,totalWeight);
          ratio_2m_pt_afterSorH_4b->Fill((2*Scand->M()/Scand->Pt()),totalWeight);
          //6. #Delta#eta cut. Final selection of SR 
          if(DEta_SH<1.1){
            Cutflow->Fill(7.0,totalWeight);
            Hcand_mass_deta_4b_SR1->Fill(Hcand->M(),totalWeight);
            Hcand_pT_deta_4b_SR1->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_deta_4b_SR1->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_deta_4b_SR1->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_deta_4b_SR1->Fill(Scand->M(),totalWeight);
            Scand_pT_deta_4b_SR1->Fill(Scand->Pt(),totalWeight);
            Scand_eta_deta_4b_SR1->Fill(Scand->Eta(),totalWeight);
            Scand_phi_deta_4b_SR1->Fill(Scand->Phi(),totalWeight);
            Xcand_mass_afterSorH_deta_4b_SR1->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_deta_4b_SR1->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_deta_4b_SR1->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_deta_4b_SR1->Fill(X_cand.Phi(),totalWeight);

            mHvsmX_deta_4b_SR1->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_deta_4b_SR1->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_deta_4b_SR1->Fill(Scand->M(),Hcand->M(),totalWeight);

            dR_SH_afterSorH_deta_4b_SR1->Fill(DR_SH,totalWeight);
            dEta_SH_afterSorH_deta_4b_SR1->Fill(DEta_SH,totalWeight);
            dPhi_SH_afterSorH_deta_4b_SR1->Fill(DPhi_SH,totalWeight);
            dRapidity_SH_afterSorH_deta_4b_SR1->Fill(rapidity_SH,totalWeight);

          }//Close for dEta  cut
        }//Close for 4b, I guess

#endif     
            //if(CR_PF){
          if(CR_FP){
            Hcand_mass_CR_FP->Fill(Hcand->M(),totalWeight);
            Hcand_pT_CR_FP->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_CR_FP->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_CR_FP->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_CR_FP->Fill(Scand->M(),totalWeight);
            Scand_pT_CR_FP->Fill(Scand->Pt(),totalWeight);
            Scand_eta_CR_FP->Fill(Scand->Eta(),totalWeight);
            Scand_phi_CR_FP->Fill(Scand->Phi(),totalWeight);
            Xcand_mass_afterSorH_CR_FP->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_CR_FP->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR_FP->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR_FP->Fill(X_cand.Phi(),totalWeight);
    
            mHvsmX_CR_FP->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_CR_FP->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_CR_FP->Fill(Scand->M(),Hcand->M(),totalWeight);
          } 
          if(CR_FF){
            Hcand_mass_CR_FF->Fill(Hcand->M(),totalWeight);
            Hcand_pT_CR_FF->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_CR_FF->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_CR_FF->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_CR_FF->Fill(Scand->M(),totalWeight);
            Scand_pT_CR_FF->Fill(Scand->Pt(),totalWeight);
            Scand_eta_CR_FF->Fill(Scand->Eta(),totalWeight);
            Scand_phi_CR_FF->Fill(Scand->Phi(),totalWeight);
            Xcand_mass_afterSorH_CR_FF->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_CR_FF->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR_FF->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR_FF->Fill(X_cand.Phi(),totalWeight);
    
            mHvsmX_CR_FF->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_CR_FF->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_CR_FF->Fill(Scand->M(),Hcand->M(),totalWeight);
           }
           if(VR_FP){
            Hcand_mass_VR_FP->Fill(Hcand->M(),totalWeight);
            Hcand_pT_VR_FP->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_VR_FP->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_VR_FP->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_VR_FP->Fill(Scand->M(),totalWeight);
            Scand_pT_VR_FP->Fill(Scand->Pt(),totalWeight);
            Scand_eta_VR_FP->Fill(Scand->Eta(),totalWeight);
            Scand_phi_VR_FP->Fill(Scand->Phi(),totalWeight);
            Xcand_mass_afterSorH_VR_FP->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_VR_FP->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_VR_FP->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_VR_FP->Fill(X_cand.Phi(),totalWeight);
    
            mHvsmX_VR_FP->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_VR_FP->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_VR_FP->Fill(Scand->M(),Hcand->M(),totalWeight);
          }
           if(VR_PF){
            Hcand_mass_VR_PF->Fill(Hcand->M(),totalWeight);
            Hcand_pT_VR_PF->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_VR_PF->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_VR_PF->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_VR_PF->Fill(Scand->M(),totalWeight);
            Scand_pT_VR_PF->Fill(Scand->Pt(),totalWeight);
            Scand_eta_VR_PF->Fill(Scand->Eta(),totalWeight);
            Scand_phi_VR_PF->Fill(Scand->Phi(),totalWeight);
            Xcand_mass_afterSorH_VR_PF->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_VR_PF->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_VR_PF->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_VR_PF->Fill(X_cand.Phi(),totalWeight);
    
            mHvsmX_VR_PF->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_VR_PF->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_VR_PF->Fill(Scand->M(),Hcand->M(),totalWeight);
          }
          if(CR_PF){
            Hcand_mass_CR_PF->Fill(Hcand->M(),totalWeight);
            Hcand_pT_CR_PF->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_CR_PF->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_CR_PF->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_CR_PF->Fill(Scand->M(),totalWeight);
            Scand_pT_CR_PF->Fill(Scand->Pt(),totalWeight);
            Scand_eta_CR_PF->Fill(Scand->Eta(),totalWeight);
            Scand_phi_CR_PF->Fill(Scand->Phi(),totalWeight);
            Xcand_mass_afterSorH_CR_PF->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_CR_PF->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR_PF->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR_PF->Fill(X_cand.Phi(),totalWeight);
    
            mHvsmX_CR_PF->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_CR_PF->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_CR_PF->Fill(Scand->M(),Hcand->M(),totalWeight);
           } 
 // ******************************* Method II CR region ***************** //
          if(Hcand->inHwindow()){
          //Method-II for background estimation!!!!!!!!!!!!!!!
           if(Hcand->n_b_DL1r77 == 0 && Scand->n_b_DL1r77>=2 ){//CR0
            //Fill histos
            Hcand_mass_CR0->Fill(Hcand->M(),totalWeight);
            Hcand_pT_CR0->Fill(Hcand->Pt(),totalWeight);
            Hcand_eta_CR0->Fill(Hcand->Eta(),totalWeight);
            Hcand_phi_CR0->Fill(Hcand->Phi(),totalWeight);
            Scand_mass_CR0->Fill(Scand->M(),totalWeight);
            Scand_pT_CR0->Fill(Scand->Pt(),totalWeight);
            Scand_eta_CR0->Fill(Scand->Eta(),totalWeight);
            Scand_phi_CR0->Fill(Scand->Phi(),totalWeight);

            Xcand_mass_afterSorH_CR0->Fill(X_cand.M(),totalWeight);
            Xcand_pT_afterSorH_CR0->Fill(X_cand.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR0->Fill(X_cand.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR0->Fill(X_cand.Phi(),totalWeight);
    
            mHvsmX_CR0->Fill(X_cand.M(),Hcand->M(),totalWeight);
            mSvsmX_CR0->Fill(X_cand.M(),Scand->M(),totalWeight);
            mSvsmH_CR0->Fill(Scand->M(),Hcand->M(),totalWeight);
           }//End of CR0
          }//end of Hwindowi
      
      }//Close for S and H assignment with histo fill. End of SR1
      // Here I would like to define Method-II of the background estimation. Because here we wanted to select the S and H candidate slightly different.
      // Do not forgert to include deta cut!!!!!!!
       fatjet * Higgscand = 0;
       fatjet * ScalarScand = 0;
      //Assignment of the S and H candidates
      //Finding which large-R jet's mass is closer to real mH. From the fit of the reco. mH is about 121 GeV.
      double mH = 121.4;
      bool leading_lrj_isHiggs = false;
      bool subleading_lrj_isHiggs = false;
      TLorentzVector XS,HC,SC;
      //Find the large-R jets that closest to Higgs mass
      if( abs(mH-leading_lrj->M()) < abs(mH-sub_leading_lrj->M()) ){//if the leading large-R jet mass is the closest
        Higgscand = leading_lrj;
        ScalarScand = sub_leading_lrj;
        leading_lrj_isHiggs = true;
        //std::cout<<"when lj1 is Higgs: "<<Higgscand->M()<<" S cand mass: "<<ScalarScand->M()<<std::endl;
        //Hcand_ScalarScalarScandR2->Fill(Higgscand->M(),totalWeight);
        //Scand_ScalarScalarScandR2->Fill(ScalarScand->M(),totalWeight);
      }else {
        Higgscand = sub_leading_lrj;
        ScalarScand = leading_lrj;
        subleading_lrj_isHiggs = true;
        //std::cout<<"when lj2 is Higgs: "<<Higgscand->M()<<" S cand mass: "<<ScalarScand->M()<<std::endl;
        //Hcand_ScalarScalarScandR2->Fill(Higgscand->M(),totalWeight);
        //Scand_ScalarScalarScandR2->Fill(ScalarScand->M(),totalWeight);
      } 
        HC.SetPtEtaPhiM(Higgscand->Pt(),Higgscand->Eta(),Higgscand->Phi(),Higgscand->M());
        SC.SetPtEtaPhiM(ScalarScand->Pt(),ScalarScand->Eta(),ScalarScand->Phi(),ScalarScand->M());
        XS = HC + SC;
      
        // In this method S candidate will always required to be double-b-tagged with DL1r 77% WP.
        //
        //bool VR = Higgscand->M() > 50. && Higgscand->M() < 105. ;
        //bool SR = Higgscand->M() > 105. && Higgscand->M() < 140. ;
        //bool CR = Higgscand->M() > 140. && Higgscand->M() < 200. ;
          //if(SR){
          /*
          if(Higgscand->inHwindow()){
          //Method-II for background estimation!!!!!!!!!!!!!!!
           if(Higgscand->n_b_DL1r77 == 0 && ScalarScand->n_b_DL1r77>=2 ){//CR0
            //Fill histos
            Hcand_mass_CR0->Fill(Higgscand->M(),totalWeight);
            Hcand_pT_CR0->Fill(Higgscand->Pt(),totalWeight);
            Hcand_eta_CR0->Fill(Higgscand->Eta(),totalWeight);
            Hcand_phi_CR0->Fill(Higgscand->Phi(),totalWeight);
            Scand_mass_CR0->Fill(ScalarScand->M(),totalWeight);
            Scand_pT_CR0->Fill(ScalarScand->Pt(),totalWeight);
            Scand_eta_CR0->Fill(ScalarScand->Eta(),totalWeight);
            Scand_phi_CR0->Fill(ScalarScand->Phi(),totalWeight);

            Xcand_mass_afterSorH_CR0->Fill(XS.M(),totalWeight);
            Xcand_pT_afterSorH_CR0->Fill(XS.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR0->Fill(XS.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR0->Fill(XS.Phi(),totalWeight);
    
            mHvsmX_CR0->Fill(XS.M(),Higgscand->M(),totalWeight);
            mSvsmX_CR0->Fill(XS.M(),ScalarScand->M(),totalWeight);
            mSvsmH_CR0->Fill(ScalarScand->M(),Higgscand->M(),totalWeight);
           }//End of CR0
          }//end of Hwindowi
          */
          
          //if(VR){
         if(Higgscand->inLSB()){
           if ( ScalarScand->n_b_DL1r77>=2 && Higgscand->n_b_DL1r77>=2){//VR_LSB2
            Hcand_mass_VR_LSB2->Fill(Higgscand->M(),totalWeight);
            Hcand_pT_VR_LSB2->Fill(Higgscand->Pt(),totalWeight);
            Hcand_eta_VR_LSB2->Fill(Higgscand->Eta(),totalWeight);
            Hcand_phi_VR_LSB2->Fill(Higgscand->Phi(),totalWeight);
            Scand_mass_VR_LSB2->Fill(ScalarScand->M(),totalWeight);
            Scand_pT_VR_LSB2->Fill(ScalarScand->Pt(),totalWeight);
            Scand_eta_VR_LSB2->Fill(ScalarScand->Eta(),totalWeight);
            Scand_phi_VR_LSB2->Fill(ScalarScand->Phi(),totalWeight);
            Xcand_mass_afterSorH_VR_LSB2->Fill(XS.M(),totalWeight);
            Xcand_pT_afterSorH_VR_LSB2->Fill(XS.Pt(),totalWeight);
            Xcand_eta_afterSorH_VR_LSB2->Fill(XS.Eta(),totalWeight);
            Xcand_phi_afterSorH_VR_LSB2->Fill(XS.Phi(),totalWeight);
    
            mHvsmX_VR_LSB2->Fill(XS.M(),Higgscand->M(),totalWeight);
            mSvsmX_VR_LSB2->Fill(XS.M(),ScalarScand->M(),totalWeight);
            mSvsmH_VR_LSB2->Fill(ScalarScand->M(),Higgscand->M(),totalWeight);
          }//End of VR_LSB2 
          else if(Higgscand->n_b_DL1r77==0 && ScalarScand->n_b_DL1r77>=2 ){//VR_LSB1
            Hcand_mass_VR_LSB1->Fill(Higgscand->M(),totalWeight);
            Hcand_pT_VR_LSB1->Fill(Higgscand->Pt(),totalWeight);
            Hcand_eta_VR_LSB1->Fill(Higgscand->Eta(),totalWeight);
            Hcand_phi_VR_LSB1->Fill(Higgscand->Phi(),totalWeight);
            Scand_mass_VR_LSB1->Fill(ScalarScand->M(),totalWeight);
            Scand_pT_VR_LSB1->Fill(ScalarScand->Pt(),totalWeight);
            Scand_eta_VR_LSB1->Fill(ScalarScand->Eta(),totalWeight);
            Scand_phi_VR_LSB1->Fill(ScalarScand->Phi(),totalWeight);
            Xcand_mass_afterSorH_VR_LSB1->Fill(XS.M(),totalWeight);
            Xcand_pT_afterSorH_VR_LSB1->Fill(XS.Pt(),totalWeight);
            Xcand_eta_afterSorH_VR_LSB1->Fill(XS.Eta(),totalWeight);
            Xcand_phi_afterSorH_VR_LSB1->Fill(XS.Phi(),totalWeight);
    
            mHvsmX_VR_LSB1->Fill(XS.M(),Higgscand->M(),totalWeight);
            mSvsmX_VR_LSB1->Fill(XS.M(),ScalarScand->M(),totalWeight);
            mSvsmH_VR_LSB1->Fill(ScalarScand->M(),Higgscand->M(),totalWeight);
           }//End of VR_LSB1 
          }//end of LSB
         //else if(CR){
         if(Higgscand->inHSB()){
           if(ScalarScand->n_b_DL1r77>=2 && Higgscand->n_b_DL1r77>=2){//CR_HSB2
            Hcand_mass_CR_HSB2->Fill(Higgscand->M(),totalWeight);
            Hcand_pT_CR_HSB2->Fill(Higgscand->Pt(),totalWeight);
            Hcand_eta_CR_HSB2->Fill(Higgscand->Eta(),totalWeight);
            Hcand_phi_CR_HSB2->Fill(Higgscand->Phi(),totalWeight);
            Scand_mass_CR_HSB2->Fill(ScalarScand->M(),totalWeight);
            Scand_pT_CR_HSB2->Fill(ScalarScand->Pt(),totalWeight);
            Scand_eta_CR_HSB2->Fill(ScalarScand->Eta(),totalWeight);
            Scand_phi_CR_HSB2->Fill(ScalarScand->Phi(),totalWeight);

            Xcand_mass_afterSorH_CR_HSB2->Fill(XS.M(),totalWeight);
            Xcand_pT_afterSorH_CR_HSB2->Fill(XS.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR_HSB2->Fill(XS.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR_HSB2->Fill(XS.Phi(),totalWeight);
    
            mHvsmX_CR_HSB2->Fill(XS.M(),Higgscand->M(),totalWeight);
            mSvsmX_CR_HSB2->Fill(XS.M(),ScalarScand->M(),totalWeight);
            mSvsmH_CR_HSB2->Fill(ScalarScand->M(),Higgscand->M(),totalWeight);
          }//End of CR_HSB2 
          else if(Higgscand->n_b_DL1r77==0 && ScalarScand->n_b_DL1r77>=2 ){//CR_LSB1
            Hcand_mass_CR_HSB1->Fill(Higgscand->M(),totalWeight);
            Hcand_pT_CR_HSB1->Fill(Higgscand->Pt(),totalWeight);
            Hcand_eta_CR_HSB1->Fill(Higgscand->Eta(),totalWeight);
            Hcand_phi_CR_HSB1->Fill(Higgscand->Phi(),totalWeight);
            Scand_mass_CR_HSB1->Fill(ScalarScand->M(),totalWeight);
            Scand_pT_CR_HSB1->Fill(ScalarScand->Pt(),totalWeight);
            Scand_eta_CR_HSB1->Fill(ScalarScand->Eta(),totalWeight);
            Scand_phi_CR_HSB1->Fill(ScalarScand->Phi(),totalWeight);
            Xcand_mass_afterSorH_CR_HSB1->Fill(XS.M(),totalWeight);
            Xcand_pT_afterSorH_CR_HSB1->Fill(XS.Pt(),totalWeight);
            Xcand_eta_afterSorH_CR_HSB1->Fill(XS.Eta(),totalWeight);
            Xcand_phi_afterSorH_CR_HSB1->Fill(XS.Phi(),totalWeight);
    
            mHvsmX_CR_HSB1->Fill(XS.M(),Higgscand->M(),totalWeight);
            mSvsmX_CR_HSB1->Fill(XS.M(),ScalarScand->M(),totalWeight);
            mSvsmH_CR_HSB1->Fill(ScalarScand->M(),Higgscand->M(),totalWeight);
           }//End of CR_HSB1 
          }//End of inHSB

    }//Close for pt cut for largeR jets
  }//Close for at least large-R jets
}//Close for trigger mc20e
//if(leading_lrj!=0) {
  //          Jet_mass_prw->Fill(leading_lrj->M(),totalWeight);}
 //leading_lrj->M()  
//here was event selection !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//std::cout<<"runnumber: "<<*RunNumber<<std::endl;
  fReader.SetEntry(entry);
  return kTRUE;
} // Close Nominal

void Nominal::Terminate(TString filename,int i)
{
  TFile *f = new TFile(filename+".root","RECREATE");
  
  for (uint i=0;i<histlist.size();++i) {
#ifndef processdata 
    //std::cout<<"dsid_weights in terminate: "<<dsid_weights<<std::endl;
    //std::cout<<"normalization: "<<lumi*mc_weighted/dsid_weights<<std::endl;
    (histlist.at(i))->Scale(lumi*mc_weighted/dsid_weights);
    //(histlist.at(i))->Write();
#endif
    (histlist.at(i))->Write();
    delete histlist.at(i);
  }
  histlist.clear();
  f->Close();
}


TH1D * Nominal::makeHist(TString name,int nbins,double x1, double x2) {
  TH1D * hist = new TH1D(name,name,nbins,x1,x2);
  if ( name.Contains("comp") ) {
    hist->GetXaxis()->SetBinLabel(1,">=2b,>=1c");
    hist->GetXaxis()->SetBinLabel(2,">=2b,0c");
    hist->GetXaxis()->SetBinLabel(3,"1b,>=1c");
    hist->GetXaxis()->SetBinLabel(4,"1b,0c");
    hist->GetXaxis()->SetBinLabel(5,"1c");
    hist->GetXaxis()->SetBinLabel(6,"0b,0c");
  }
  hist->Sumw2();
  histlist.push_back(hist);
  return hist;
}

TH1F * Nominal::makeHist_F(TString name,int nbins,double x1, double x2) {
  TH1F * hist = new TH1F(name,name,nbins,x1,x2);
  hist->Sumw2();
  histlist.push_back(hist);
  return hist;
}

TH2D * Nominal::make2DHist(TString name,int nbins,double x1,double x2,int nbinsy,double y1,double y2) {
  TH2D * hist = new TH2D(name,name,nbins,x1,x2,nbinsy,y1,y2);
  hist-> Sumw2();
  histlist.push_back(hist);
  return hist;
}
    
std::pair<std::map<std::string, double>, bool> GetMCWeightMap() {
  std::map<std::string, double> MCWeightMap;
  bool Success;
  PyObject *pName;
  PyObject *pModule;
  PyObject *pFunc;

  PyObject *pDict;

  Py_Initialize();
  PyRun_SimpleString("import sys");
  PyRun_SimpleString("sys.path.append(\".\")");
  pName = PyUnicode_FromString("read_json");
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);

  if (pModule != nullptr) {
    pFunc = PyObject_GetAttrString(pModule,"read_file");

    if (pFunc && PyCallable_Check(pFunc)) {
      pDict = PyObject_CallObject(pFunc, nullptr);
      if (pDict != nullptr) {
	PyObject *pKeys = PyDict_Keys(pDict);
	PyObject *pValues = PyDict_Values(pDict);
	for (Py_ssize_t i = 0; i < PyDict_Size(pDict); ++i) { 
	  auto pair = std::make_pair(std::string(PyUnicode_AsUTF8(PyList_GetItem(pKeys, i))), PyFloat_AsDouble(PyList_GetItem(pValues, i)));
	  MCWeightMap.insert( pair );
	}
	Success = true;
	Py_DECREF(pDict);
      }
      else {
	Py_DECREF(pFunc);
	Py_DECREF(pModule);
	PyErr_Print();
	fprintf(stderr,"Call failed\n");
	Success = false;
	return std::make_pair(MCWeightMap,Success);
      }
    }
    else {
      if (PyErr_Occurred())
	PyErr_Print();
      fprintf(stderr,"Cannot find function \"%s\"\n", "read_file");
    }
    Py_XDECREF(pFunc);
    Py_DECREF(pModule);
  }
  else {
    PyErr_Print();
    fprintf(stderr, "Failed to load \"%s\"\n", "read_json");
    Success = false;
    return std::make_pair(MCWeightMap,Success);
  }
  Py_Finalize();
  Success = true;

  return std::make_pair(MCWeightMap,Success);
}

std::vector<TString> list_files(const char *dirname="C:/root/folder/", const char *ext=".root") {
  std::vector<TString> result;
  TSystemDirectory dir(dirname, dirname);
  TList *files = dir.GetListOfFiles();
  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file=(TSystemFile*)next())) {
      fname = file->GetName();
      if (!file->IsDirectory() && fname.EndsWith(ext)) {
	std::cout << "list_files: " <<fname.Data() << std::endl;
	result.push_back(fname);
      }
    }
  }
  return result;
}


//void dogen(TString path,TString fname,TString campaign) {
//void dogen(TString path,TString fname) {
void dogen(TString filelist, TString mc_dsid) {

  Nominal * selector = new Nominal();

  std::vector<TChain*> chains;  
  //TChain * chain = new TChain("Nominal");
  TChain * chain = new TChain ("AnalysisMiniTree");
  chains.push_back(chain);

  // if MC, create chains for weights
#ifndef processdata

  for (uint i=1; i<chains.size();++i) {
    chains[i]->AddFriend(chain);
  }
#endif

  // list of files in each folder
  //std::vector<TString> list = list_files(path);
  std::vector<TString> list;
  std::ifstream fin(filelist); std::string sin; std::string s1 = "";
  while (getline(fin,sin)) {
    for (std::string::iterator is = sin.begin(); is!=sin.end(); ++is) {
      if (*is==',') {
	list.push_back(s1);
	s1 = "";
      } else {
	s1 += *is;
      }
    }
    list.push_back(s1);
  }

  std::cout<<"dogen: Found "<<list.size()<<" files"<<std::endl;
  if (list.size() == 0) return;

  //float sumweights = 0.;
  
  for (uint i=0;i<list.size();++i) {
    std::cout<<"dogen: Adding "<<list[i]<<std::endl;
    for (auto it=chains.begin();it!=chains.end();++it) { 
      (*it)->Add(list[i]);
    }
#ifndef processdata  // this is test for data, remove it if it fails!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  float sumweights = 0.;
        std::unique_ptr<TFile> openfile(TFile::Open(list[i]));
        if(!openfile || openfile->IsZombie()){
          std::cout << "Error opening file" << std::endl;
          exit(-1);
        }
        ////openfile->ls();
        TList *li = openfile->GetListOfKeys();
        TIter iter1(li->MakeIterator());
        while(TObject* obj = iter1()){
          TKey *thekey = (TKey*)obj;
          TString name = thekey->GetName();
          if (name.Contains("CutBookkeeper")){
            //std::cout<<"print list:"<<li<<std::endl;
            ////std::cout<< "name of the key: "<<thekey->GetName()<<std::endl;  
            TH1F * sumweights_h = (TH1F*) openfile->Get(name);
            ////std::cout<<"list key: "<<li<<" sumweight: "<<sumweights_h->GetBinContent(2)<<std::endl;
            sumweights=sumweights_h->GetBinContent(2);
          }   
        } 
        openfile->Close();
           selector->dsid_weights+=sumweights;
            std::cout<<"dsid_weights : " << selector->dsid_weights<<std::endl;
   #endif  // this is test for data, remove it if it fails!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // Only for MC
#ifndef processdata
    //chain_2->Add(list[i]);
#endif
  }//Close for list.size()
  
  for (auto it =chains.begin();it!=chains.end();++it) { 
    (*it)->Lookup(true);
  }

  //TString filename = path;
  TString filename = "output";

// Only MC
#ifndef processdata
  /*Ssiz_t date_pos;
  TString mc_dsid;
  
  if(filename.Contains("251122") ) {
    date_pos = filename.Index("251122");
    mc_dsid = filename(date_pos+7,6);
  }else if(filename.Contains("201022")){
    date_pos = filename.Index("201022");
    mc_dsid = filename(date_pos+7,6);
  }else if(filename.Contains("24022023")){
    date_pos = filename.Index("24022023");
    mc_dsid = filename(date_pos+9,6);
  }else{
  std::cout<<"Could not find dataset number of the sample,\n So could not normalize correctly ... "<<mc_dsid<<std::endl;
  } 
  //Ssiz_t date_pos = filename.Index("251122");
  //Ssiz_t date_pos = filename.Index("201022");
  //TString mc_dsid = filename(date_pos+7,6);
  std::cout<<filename<<std::endl;*/

  // Identifying the mc campaign and setting the correspoding luminosity
#ifdef trig13167
  const float luminosity = 36.1e+06;
#endif
#ifdef trig13144
  const float luminosity = 44.3e+06;
#endif
#ifdef trig13145
  const float luminosity = 58.45e+06;
#endif

  std::map<std::string, double> weight_map = (GetMCWeightMap()).first;
   for (auto const& pair: weight_map) { // This part is for name of the samples matching to the mc_weight= Xsec*genfilt*kFactor

   //std::cout<<"{" <<(pair.first) << ":"<<(pair.second)<<"}"<<std::endl;

 }
  for (auto iter = weight_map.begin(); iter != weight_map.end(); ++iter) {
    if (iter->first.find(mc_dsid)!=std::string::npos){ 
      // Now giving the mcweight to the input sample that you are running over
      selector->mc_weighted = iter->second; // this is XS*GenFiltEff*KFactor
      std::cout << iter->first << " : "<< "mc_weighted"<< " : " << selector->mc_weighted <<std::endl;
    }
  }
#endif
  
  for (auto it=chains.begin();it!=chains.end();++it) {
// if mc, reset dsid_weights to 0 each time when doing systematics
#ifndef processdata
    //selector->dsid_weights = 0;
    selector->lumi = luminosity;
#endif

    selector->Init(*it);
    selector->Begin(0);
    selector->Notify();

    std::cout<<"dogen: Will use "<<(*it)->GetEntries()<<" entries"<<std::endl;
    
// Only for MC
    int idx = it-chains.begin();
    for (int i=0;i<(*it)->GetEntries();++i) {
      selector->Process(i,idx);
    }
    std::cout<<"Process finished"<<std::endl;
/*
    TString outfile = fname;

    TString systName = (*it)->GetName();

// For MC, include systematic name in outfile
std::cout<<"outfile: "<<outfile<<std::endl;
#ifndef processdata
    if (outfile.Contains("test")){
      Ssiz_t pos = outfile.Index("/");
      outfile.Insert(pos+1,systName+TString("_"));
    }else{
      Ssiz_t pos = outfile.Index("_TREE");
      outfile.Insert(pos+1,systName+TString("_")); 
    } 
#endif
*/
    selector->Terminate(filename,idx);
/*
    std::cout<<"Will execute Exec(mv "<<filename<<".root "<<outfile<<")"<<std::endl;
    gSystem->Exec("mv "+filename+".root "+outfile);
    std::cout<<"Move executed. Starting next tree."<<std::endl;
*/
  }

  selector->Delete();
  std::cout<<"selector has been deleted"<<std::endl;

}

int main(int argc, char **argv) {

  if (argc < 2) {
    std::cout<<"main called with only "<<argc<<" arguments; quit"<<std::endl;
    return 1;
  }

  TString filename = argv[1];
  TString mc_dsid = argv[2];
  dogen(filename, mc_dsid);

/*
  TString opdir = argv[1];
  TString infile = argv[2];
  TString outfile = argv[3];
  //TString campaign = argv[4];
  TString base = "/lustre/fs24/group/atlas/dbattulga/ntup_SH_Feb2023/Data/";
#ifndef processdata 
  base = "/lustre/fs24/group/atlas/dbattulga/ntup_SH_Feb2023/MC/";
  //base = "/lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/MC/";
#endif
  if (argc > 4) base = argv[4];

  std::cout<<"Nominal: opdir "<<opdir<<std::endl;
  std::cout<<"Nominal: infile "<<infile<<std::endl;
  std::cout<<"Nominal: outfile "<<outfile<<std::endl;
  std::cout<<"Nominal: base "<<base<<std::endl;

  if (!gSystem->Exec("ls "+opdir)) {
    std::cout<<"pathname " <<opdir<<" exists"<<std::endl;
  } else {
    std::cout<<"pathname "<<opdir<<" does not exist. It will be made"<<std::endl;
    gSystem->Exec("mkdir "+opdir);
  }
  //dogen(base+infile+"/",opdir+"/"+outfile,campaign);
  dogen(base+infile+"/",opdir+"/"+outfile);*/
  return 0;
}

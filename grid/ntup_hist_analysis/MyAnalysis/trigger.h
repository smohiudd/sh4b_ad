#ifndef __TRIGGER__
#define __TRIGGER__

#include "TTreeFormula.h"
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TSystem.h"

class trigger {

public:
  trigger();
  trigger(const TString& expression);
  virtual double getValue() const override();
  virtual TObjArray* getBranchNames() const override;

protected:
  TString fExpression = "";
  TTreeFormula* HLT_j360_a10_lcw_sub_L1J100 = NULL;
  TTreeFormula* HLT_j420_a10_lcw_L1J100 = NULL;
  TTreeFormula* HLT_j420_a10t_lcw_jes_40smcINF_L1J100 = NULL;
  TTreeFormula* HLT_j390_a10t_lcw_jes_30smcINF_L1J100 = NULL;
  TTreeFormula* HLT_j420_a10t_lcw_jes_35smcINF_L1J100 = NULL;

virtual ~trigger();

ClassDefOverride(Trigger, 1);
}

#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class fatjet+;
#pragma link C++ class truthfatjet+;
#pragma link C++ class smallrjet+;
#pragma link C++ class truth_b+;
#pragma link C++ class Nominal+;
#pragma link C++ class R22+;

#endif

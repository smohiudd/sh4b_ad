#bin/sh

date=`date +%Y%m%d`
dir=Nominal"_"${date}_SH4b_mc16e_trigEff

if [[ -e $dir ]] ; then
   echo dir $dir already exists
   echo I quit
   exit
fi

for thisfile in `ls -d /lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/MC/user.dabattul.*r13145*_TREE` ; do

    # let counter=counter+1 
    # if [ $counter -eq 3 ]; then
    # 	exit
    # fi

    infile=`echo $thisfile | awk -F/ '{print $9}'`
    outfile="Nominal-"`echo $thisfile | awk -F. '{print $5"."$6".root"}'`

    echo ${infile}
    echo ${outfile}
    filename=${outfile}"_"${date}"_mc.sh"
    echo filename=$filename
    rm -fr $filename
touch $filename
cat <<EOF >$filename

#!bin/zsh
#
#(otherwise the default shell would be used)

#$ -S /bin/bash
#(the running time for this job)
#$ -l h_rt=05:30:00
#
#(the maximum memory usage of this job)
#$ -l h_rss=2G
#
#(stderr and stdout are merged together to stdout)
#$ -j y
#
#(send mail on job's end and abort)
#$ -m ae
#
#(execute the job from the current directory and not relative to your home directory)
#$ -cwd

# Submitting for ${outfiles[$c]}
# on $date
export ROOTSYS=$HOME/root
export PATH=$PATH:$ROOTSYS/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib
./mc_nosysts_R22 ${dir} ${infile} ${outfile} /lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/MC/

rm -f $PWD/$filename

EOF
      chmod +x $filename
      #qsub -cwd -j y -V $PWD/$filename -e ${outfile}"_err.txt" -o ${outfile}"_log.txt" $PWD/$filename
          (( c++ ))
done

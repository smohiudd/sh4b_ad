// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME Dictionary_dat
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "../MyAnalysis/fatjet.h"
#include "../MyAnalysis/smallrjet.h"
#include "../MyAnalysis/Nominal.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static void *new_fatjet(void *p = nullptr);
   static void *newArray_fatjet(Long_t size, void *p);
   static void delete_fatjet(void *p);
   static void deleteArray_fatjet(void *p);
   static void destruct_fatjet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::fatjet*)
   {
      ::fatjet *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::fatjet >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("fatjet", ::fatjet::Class_Version(), "../MyAnalysis/fatjet.h", 10,
                  typeid(::fatjet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::fatjet::Dictionary, isa_proxy, 4,
                  sizeof(::fatjet) );
      instance.SetNew(&new_fatjet);
      instance.SetNewArray(&newArray_fatjet);
      instance.SetDelete(&delete_fatjet);
      instance.SetDeleteArray(&deleteArray_fatjet);
      instance.SetDestructor(&destruct_fatjet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::fatjet*)
   {
      return GenerateInitInstanceLocal((::fatjet*)nullptr);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::fatjet*)nullptr); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_smallrjet(void *p = nullptr);
   static void *newArray_smallrjet(Long_t size, void *p);
   static void delete_smallrjet(void *p);
   static void deleteArray_smallrjet(void *p);
   static void destruct_smallrjet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::smallrjet*)
   {
      ::smallrjet *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::smallrjet >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("smallrjet", ::smallrjet::Class_Version(), "../MyAnalysis/smallrjet.h", 9,
                  typeid(::smallrjet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::smallrjet::Dictionary, isa_proxy, 4,
                  sizeof(::smallrjet) );
      instance.SetNew(&new_smallrjet);
      instance.SetNewArray(&newArray_smallrjet);
      instance.SetDelete(&delete_smallrjet);
      instance.SetDeleteArray(&deleteArray_smallrjet);
      instance.SetDestructor(&destruct_smallrjet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::smallrjet*)
   {
      return GenerateInitInstanceLocal((::smallrjet*)nullptr);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::smallrjet*)nullptr); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_Nominal(void *p = nullptr);
   static void *newArray_Nominal(Long_t size, void *p);
   static void delete_Nominal(void *p);
   static void deleteArray_Nominal(void *p);
   static void destruct_Nominal(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Nominal*)
   {
      ::Nominal *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Nominal >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Nominal", ::Nominal::Class_Version(), "../MyAnalysis/Nominal.h", 34,
                  typeid(::Nominal), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Nominal::Dictionary, isa_proxy, 4,
                  sizeof(::Nominal) );
      instance.SetNew(&new_Nominal);
      instance.SetNewArray(&newArray_Nominal);
      instance.SetDelete(&delete_Nominal);
      instance.SetDeleteArray(&deleteArray_Nominal);
      instance.SetDestructor(&destruct_Nominal);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Nominal*)
   {
      return GenerateInitInstanceLocal((::Nominal*)nullptr);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Nominal*)nullptr); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr fatjet::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *fatjet::Class_Name()
{
   return "fatjet";
}

//______________________________________________________________________________
const char *fatjet::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::fatjet*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int fatjet::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::fatjet*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *fatjet::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::fatjet*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *fatjet::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::fatjet*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr smallrjet::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *smallrjet::Class_Name()
{
   return "smallrjet";
}

//______________________________________________________________________________
const char *smallrjet::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::smallrjet*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int smallrjet::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::smallrjet*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *smallrjet::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::smallrjet*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *smallrjet::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::smallrjet*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Nominal::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Nominal::Class_Name()
{
   return "Nominal";
}

//______________________________________________________________________________
const char *Nominal::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Nominal*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Nominal::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Nominal*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Nominal::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Nominal*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Nominal::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Nominal*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void fatjet::Streamer(TBuffer &R__b)
{
   // Stream an object of class fatjet.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(fatjet::Class(),this);
   } else {
      R__b.WriteClassBuffer(fatjet::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_fatjet(void *p) {
      return  p ? new(p) ::fatjet : new ::fatjet;
   }
   static void *newArray_fatjet(Long_t nElements, void *p) {
      return p ? new(p) ::fatjet[nElements] : new ::fatjet[nElements];
   }
   // Wrapper around operator delete
   static void delete_fatjet(void *p) {
      delete ((::fatjet*)p);
   }
   static void deleteArray_fatjet(void *p) {
      delete [] ((::fatjet*)p);
   }
   static void destruct_fatjet(void *p) {
      typedef ::fatjet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::fatjet

//______________________________________________________________________________
void smallrjet::Streamer(TBuffer &R__b)
{
   // Stream an object of class smallrjet.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(smallrjet::Class(),this);
   } else {
      R__b.WriteClassBuffer(smallrjet::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_smallrjet(void *p) {
      return  p ? new(p) ::smallrjet : new ::smallrjet;
   }
   static void *newArray_smallrjet(Long_t nElements, void *p) {
      return p ? new(p) ::smallrjet[nElements] : new ::smallrjet[nElements];
   }
   // Wrapper around operator delete
   static void delete_smallrjet(void *p) {
      delete ((::smallrjet*)p);
   }
   static void deleteArray_smallrjet(void *p) {
      delete [] ((::smallrjet*)p);
   }
   static void destruct_smallrjet(void *p) {
      typedef ::smallrjet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::smallrjet

//______________________________________________________________________________
void Nominal::Streamer(TBuffer &R__b)
{
   // Stream an object of class Nominal.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Nominal::Class(),this);
   } else {
      R__b.WriteClassBuffer(Nominal::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Nominal(void *p) {
      return  p ? new(p) ::Nominal : new ::Nominal;
   }
   static void *newArray_Nominal(Long_t nElements, void *p) {
      return p ? new(p) ::Nominal[nElements] : new ::Nominal[nElements];
   }
   // Wrapper around operator delete
   static void delete_Nominal(void *p) {
      delete ((::Nominal*)p);
   }
   static void deleteArray_Nominal(void *p) {
      delete [] ((::Nominal*)p);
   }
   static void destruct_Nominal(void *p) {
      typedef ::Nominal current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Nominal

namespace {
  void TriggerDictionaryInitialization_libMyAnalysis_Impl() {
    static const char* headers[] = {
"../MyAnalysis/fatjet.h",
"../MyAnalysis/smallrjet.h",
"../MyAnalysis/Nominal.h",
nullptr
    };
    static const char* includePaths[] = {
"/cvmfs/atlas.cern.ch/repo/sw/software/22.2/AnalysisBaseExternals/22.2.106/InstallArea/x86_64-centos7-gcc11-opt/include/",
"/afs/cern.ch/work/j/jcrosby/public/analysis/sh4b/grid/ntup-histo-analysis-sh4b/run/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libMyAnalysis dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/fatjet.h")))  fatjet;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/smallrjet.h")))  smallrjet;
class __attribute__((annotate("$clingAutoload$../MyAnalysis/Nominal.h")))  Nominal;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libMyAnalysis dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "../MyAnalysis/fatjet.h"
#include "../MyAnalysis/smallrjet.h"
#include "../MyAnalysis/Nominal.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"Nominal", payloadCode, "@",
"fatjet", payloadCode, "@",
"smallrjet", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libMyAnalysis",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libMyAnalysis_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libMyAnalysis_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libMyAnalysis() {
  TriggerDictionaryInitialization_libMyAnalysis_Impl();
}

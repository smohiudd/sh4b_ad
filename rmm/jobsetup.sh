#!/bin/bash
# systematics 0: events with at least one good lepton (electron non-isolated)
# systematics 1: events with at least one good lepton (default cuts)  
echo "Setting up job enviroment"
SYSTEMATICS=0
TYPE=0
EVENTS=-1
DIR_DATA=/mnt/shared/jcrosby/eos/sh4b/data/full/data/18/one/two/data18/
DIR_MC=/mnt/shared/jcrosby/eos/sh4b/signal/x750_s250/

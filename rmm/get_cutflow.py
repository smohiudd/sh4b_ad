import ROOT
import sys

inFile = sys.argv[1]
print("reading from ", inFile)

ff = ROOT.TFile.Open(inFile,"READ")
cutflow = ff.Get("cutflow_jets")
debug = ff.Get("debug")
nbins = cutflow.GetNbinsX()
events = []
ldebug = []

for xbin in range(0,nbins):
  nevents = cutflow.GetBinContent(xbin)
  events.append(nevents)
  nbug = debug.GetBinContent(xbin)
  ldebug.append(nbug)

print("total events: ",events[1])
print("min pT cut: ", events[2])
print("eta cut: ", events[3])
print("420 pT LO cut and 250 pT NLO", events[4])

print("Good events: ", ldebug[1])
print("Trigger passed: ", ldebug[2])
print("Event out: ", ldebug[3])
print("Lead jet-pt: ", ldebug[4])
print("rapidty: ", ldebug[5])
print("1bb: ", ldebug[6])
print("2bb: ",ldebug[7])

nentries = ff.inputNN.GetEntries()

print("total NN entries: ", nentries)


## Module of plotting scripts for normalizing flow analysis
import tensorflow.compat.v1 as tf
tf.disable_eager_execution()
import tensorflow_probability as tfp
import tensorflow_hub as hub

import pandas
import math
import matplotlib.pyplot as plt
import numpy as np
import yaml
import json

from pae.bijector_init import get_prior
from pae.plotting_module import single_scatter, multi_scatter,histogram, histogram2D,plot_pdf, bijector_slices

print("-----> Analyzing Flow Model & Printing Plots <-----")
with open('pae/pae_config.yaml','r') as file:
    config = yaml.safe_load(file)

with open(config['param_path']['path']+config['param_path']['file'],'r') as f:
        params = json.load(f)

emb = np.load(config['latent_file']['path'] + config['latent_file']['file'])
figsDir = config['figure_directory']

# splitting into training and validation data
train_size = int(len(emb)*0.3)
print('sample size of training sample', train_size)
z_sample   = emb[:train_size]
z_valid = emb[train_size:,] 
valid_size = len(z_valid)
print('sample size of validation sample', valid_size)
z_train = z_sample[:]

latent_size   = params['latent_layer']

data          = tf.placeholder(shape=[None,params['latent_layer']],dtype=tf.float32)
# input data, u-space
latent_sample = tf.placeholder(shape=[None,params['latent_layer']],dtype=tf.float32)

bs           = tf.placeholder_with_default(16,shape=[])

flow_model    = config['NF_output_model']['path']

batchsize            = config['batchsize']

prior         = get_prior(params['latent_layer'])

prior_sample  = prior.sample(bs)

flow = hub.Module(flow_model, trainable=False)
#project into Gaussian space
bwd            = flow({'z_sample':data,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['bwd_pass']
#draw from the Gaussian 
# fwd model these draws back into the embedding space
fwd            = flow({'z_sample':np.zeros((1,params['latent_layer'])),'sample_size':1, 'u_sample':latent_sample},as_dict=True)['fwd_pass']

sess = tf.Session()
sess.run(tf.global_variables_initializer())

#raw random sample from normal of size 1024
u_sample = sess.run(prior_sample, feed_dict={bs:1024})
# bring this sanmple into the embedded space of the auto-enocder
z_sample = sess.run(fwd,  feed_dict={latent_sample: u_sample})

bwd_train = sess.run(bwd,  feed_dict={data:z_train})
bwd_valid = sess.run(bwd,  feed_dict={data:z_valid})
Gaussian_sample = sess.run(prior_sample,  feed_dict={bs:len(z_sample)})

data_list =[bwd_train,bwd_valid,Gaussian_sample]
label_list = ['encoded train','encoded valid','prior']

multi_scatter(data_list,latent_size,figsDir,label_list,"u_space_dist_after")

########## compare distributions in Z-space
sc_plots = config['scatter_comparison']
sc_len = len(sc_plots)
data_list =[]
label_list = []
for plots in sc_plots:
    if plots == 'data':
        input_data = sess.run(data,  feed_dict={data:emb})
        input_data = input_data[train_size:,]
        data_list.append(input_data)
        label_list.append("data")
    if plots == 'forward':
        fwd_sample = sess.run(fwd, feed_dict={latent_sample:Gaussian_sample})
        data_list.append(fwd_sample)
        label_list.append("generated data forward")
    if plots == 'backward':
        bwd_sample = sess.run(bwd, feed_dict={data:z_valid})
        data_list.append(bwd_sample)
        label_list.append("generated data backward")

multi_scatter(data_list,latent_size,figsDir, label_list,"z_space_dist_after")

print("Plotting PDFs")
meana, stda = plot_pdf(fwd_sample,latent_size,figsDir,"pdf_after")
 
print("starting latent layer prior PDFs")
meanb, stdb = plot_pdf(input_data,latent_size,figsDir,"pdf_before")

params['mean_before'] = meanb
params['std_before'] = stdb

if config['use_test_gaussian']:
    rng = np.random.default_rng(19680801)
    if config['inject_signal']:
         input_data_len = len(input_data)+len(input_data)//10
    else:
         input_data_len = len(input_data)
    bwd_valid =np.random.normal(loc=0, scale=1.0 ,size=(input_data_len,latent_size)).astype(np.float32)

# For fun, let's inject signal
if config['inject_signal']:
    signal = np.random.normal(loc=1,scale=0.1,size=(len(input_data)//10,latent_size)).astype(np.float32)
    input_data = np.concatenate((input_data,signal),axis=0)


print("Plotting Histograms")
histogram(input_data,latent_size,figsDir,"initial_hist")
histogram(bwd_valid,latent_size,figsDir,"after_hist")

print("Plotting 2D Histogram")
histogram2D(bwd_valid,input_data,latent_size,figsDir,"target_dist_2dhist",sort=True)

with open(config['param_path']['path']+config['param_path']['file'],'r+') as f:
     json.dump(params,f)
print("#--------> Fin <--------#")
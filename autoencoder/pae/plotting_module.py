## Module of plotting scripts for normalizing flow analysis
import tensorflow.compat.v1 as tf
tf.disable_eager_execution()
import tensorflow_probability as tfp
import tensorflow_hub as hub

import pandas
import math
import matplotlib.pyplot as plt
import numpy as np
import yaml
import json
import random
from matplotlib import colors

from pae.bijector_init import get_nvp, nvp_module_spec, get_prior
from pae.flow_class import RealNVP, SplineParams
from pae.pae_functions import nll, train_density_estimation,sanity_check,PDF


with open('pae/pae_config.yaml','r') as file:
    config = yaml.safe_load(file)


plot_mod = config['plot_modulus']
gaus_size = config['guassian_fig_size']

def single_scatter(data, latent_size, dir,string):
    yy=latent_size//plot_mod
    mod = latent_size%yy
    plt.figure()
    for ii in range(latent_size-1):
        if (ii%yy==mod or ii == 59 or ii == 61 or ii==62):
            plt.scatter(data[:,ii],data[:,ii+1])
            plt.savefig(dir+"/"+string+"_"+str(ii)+".png")
            plt.close()
    plt.close('all')

def multi_scatter(data_list, latent_size, dir, label_list, string):
    plt.figure(figsize=(gaus_size,gaus_size))
    yy=latent_size//plot_mod
    mod = latent_size%yy
    for nn in range(latent_size-1):
        if (nn%yy==mod):
            plt.figure(2)
            for i in range(len(data_list)):
                plt.scatter(data_list[i][:,nn],data_list[i][:,nn+1], label=label_list[i])
            plt.xlim(-12,12)
            plt.ylim(-12,12)    
            plt.legend()
            plt.savefig(dir+"/"+string+"_"+str(nn)+".png")
            plt.close()
    plt.close('all')


def histogram(data,latent_size,dir,string,bins=100):
    yy=latent_size//plot_mod
    mod = latent_size%yy
    if len(data.shape) != 1:
        for nn in range(latent_size-1):
            if (nn%yy==mod):
                plt.hist(data[:,nn], bins=bins)
                plt.savefig(dir+"/"+string+"_"+str(nn)+".png")
                plt.close()
    else:
        plt.hist(data, bins=bins)
        plt.savefig(dir+"/"+string+".png")
    plt.close('all')



def histogram2D(datax,datay,latent_size,dir,string,bins=(50,50),sort=False):
    x_len = len(datax[0])
    y_len = len(datay[0])
    if x_len != y_len:
        if x_len > y_len:
            datax[0] = random.sample(datax,y_len) 
        else:
            datay[0] = random.sample(datay,x_len)
    if sort==True:
        for i in range(len(datax[0])):
            datax[i].sort()
            datay[i].sort()
    yy=latent_size//plot_mod
    mod = latent_size%yy
    xmax = np.amax(datax)
    xmin = np.amin(datax)
    ymax = np.amax(datay)
    ymin = np.amin(datay)
    for nn in range(latent_size-1):
        if (nn%yy==mod ):
            plt.hist2d(datax[:,nn],datay[:,nn],bins,[[xmin,xmax],[ymin,ymax]],norm=colors.LogNorm())
            plt.savefig(dir+"/"+string+"_"+str(nn)+".png")
            plt.close()
    plt.close('all')




def pdf(x,mean,stdev):
        return (1.0 / (stdev * math.sqrt(2*math.pi))) * math.exp(-0.5*((x - mean) / stdev) ** 2)

def plot_pdf(data,latent_size,dir,string,save=True):
    yy=latent_size//plot_mod
    mod = latent_size%yy

    meanl = []
    stdl = []
    plt.figure(figsize=(10,10))
    for nn in range(latent_size):
        if (nn%yy==mod ):
            p = PDF(data[:,nn])
            mean = p.calculate_mean()
            std = p.calculate_stdev(mean=mean)
            meanl.append(mean)
            stdl.append(std)
            plt.figure()
            f1 = np.vectorize(pdf)

            x = np.linspace(-4,4,100)
            plt.plot(x,f1(x,mean,std))
            # plt.legend()
            if save:
                plt.savefig(dir+"/"+string+"_"+str(nn)+".png")
    plt.close('all')
    return meanl, stdl

def bijector_slices(flow,bijector,arrayx,X,Xt,latent_size,ss,dir,string):
    ss = tf.Session()
    print("Evaluate prior done! Numpy array made")
    samples = [arrayx]
    names = [get_prior(latent_size).name]
    print("Evaluate bijectors")
    for bijector in reversed(flow.bijector.bijectors):
        Xt = bijector.forward(Xt)
        arrayx = X.eval(session=ss)
        samples.append(arrayx)
        names.append(bijector.name)
    
    ss.close()
    print("Evaluate bijectors done! Saving bijector plots")
    """
    """ 
    #samples = np.asarray(samples,dtype=np.float32)
    X0 = samples[0]
    f, arr = plt.subplots(1, len(samples), figsize=(4 * (len(samples)), 4))
    #print(samples)
    for i in range(len(samples)):
        X1 = samples[i]
        idx = np.logical_and(X0[:, 0] < 0, X0[:, 1] < 0)
        idx =np.invert(idx)
        m0 = np.ma.masked_where(idx,X1[:,0])
        m1 = np.ma.masked_where(idx,X1[:,1])
        arr[i].scatter(m0, m1, s=10, color='red')

        idx = np.logical_and(X0[:, 0] > 0, X0[:, 1] < 0)
        idx =np.invert(idx)
        s0 = np.ma.masked_where(idx,X1[:,0])
        s1 = np.ma.masked_where(idx,X1[:,1])
        arr[i].scatter(s0, s1, s=10, color='green')

        idx = np.logical_and(X0[:, 0] < 0, X0[:, 1] > 0)
        idx =np.invert(idx)
        q0 = np.ma.masked_where(idx,X1[:,0])
        q1 = np.ma.masked_where(idx,X1[:,1])
        arr[i].scatter(q0, q1, s=10, color='blue')
    
        idx = np.logical_and(X0[:, 0] > 0, X0[:, 1] > 0)
        idx =np.invert(idx)
        w0 = np.ma.masked_where(idx,X1[:,0])
        w1 = np.ma.masked_where(idx,X1[:,1])
        arr[i].scatter(w0, w1, s=10, color='black')

        

        arr[i].set_xlim((-10, 10))
        arr[i].set_ylim((-10, 10))
        arr[i].set_title(names[i])
    plt.savefig(dir+"/"+string+".png")
    plt.close('all')






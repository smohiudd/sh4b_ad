
import matplotlib
import yaml
import json
#import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_eager_execution()
#tf.config.run_functions_eagerly(True)
from pae.bijector_init import get_nvp, nvp_module_spec, get_prior
from pae.plotting_module import plot_pdf
import math
import numpy as np
import pandas as pd
from scipy.stats import norm
pd.set_option('display.max_columns', None)
pd.set_option('display.max_row', None)
import matplotlib.pyplot as plt
plt.rcdefaults()
import datetime
matplotlib.use('Agg') # set the backend before importing pyplo. Fix Invalid DISPLAY variable 
from matplotlib import pyplot as plt

import sys

import tensorflow_probability as tfp
import tensorflow_hub as hub
tfd = tfp.distributions
tfb = tfp.bijectors

print("Tensorflow Version:       ", tf.__version__)
print("Tensorflow Prob version:  ", tfp.__version__)
print("Tensorflow hub version:   ", hub.__version__)

from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard

from sklearn.model_selection import train_test_split


print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Input Data:', str(sys.argv))
n = len(sys.argv)
if (n != 2):
      print ("No arguments!. Need 1 input file in csv.zip")
      sys.exit()

inputData = sys.argv[1]

with open('pae/pae_config.yaml','r') as file:
    config = yaml.safe_load(file)

with open(config['param_path']['path']+config['param_path']['file'],'r') as f:
        params = json.load(f)

figsDir = config['figure_directory']
modelName = config['autoencoder_model_path']
## Get and prepare data

print("Reading=",inputData)
df = pd.read_csv(inputData, compression='gzip', header=0, sep=',')
print("DF size=",df.size," DF shape=",df.shape," DF dimension=",df.ndim)
#df = df.astype(np.float16)
#print(" Type=",df.dtypes)
#print("Convert to range= Min",numpy.finfo(numpy.float16).min," Max=",numpy.finfo(numpy.float16).max)


print("Skip run, event and weight columns..") 
#df.drop(['Run', 'Event', 'Weight'], axis = 1)
del df['Run']
del df['Event']
del df['Weight']
print("DF size=",df.size," DF shape=",df.shape," DF dimension=",df.ndim)


# do you want to drop columns based on common  data?
IsReadCommonEmptyColumns=1

# 1 drop colums based on common vector
# 2 drop colums as found by the current dataframe
# 0 do not drop anything

if (IsReadCommonEmptyColumns==1):
   file0="columns_with_0.txt"
   print("Read common 0 columns from ",file0)
   dcol0=pd.read_csv(file0,header = None)
   print ("-> Experimental: Drop columns with 0")
   col0=dcol0[dcol0.columns[0]]
   df=df.drop(col0, axis = 1)
   print("Total zero-cells removed=",len(dcol0))
elif (IsReadCommonEmptyColumns==2):
   print ("Experimental: find all columns with 0")
   col0 = df.columns[(df == 0).all()]
   print("COL=0 size=",col0.size," DF shape=",col0.shape," DF dimension=",col0.ndim)
   print(col0)
   #print ("Experimental: Drop columns with 0")
   #df=df.drop(col0, axis = 1)
   file0=modelName+"/columns_with_0.txt"
   print ("Experimental: Save columns with 0 in ",file0)
   print(type(col0))
   pd.Series(col0,index=col0).to_csv(file0, header=False, index=False)
   print ("Experimental: Restore columns with 0 from ",file0)
   dcol0=pd.read_csv(file0,header = None)
   col0=dcol0[dcol0.columns[0]]
   print ("-> Experimental: Drop columns with 0")
   df=df.drop(col0, axis = 1)
else:
   pass

#print(" -> Shuffle the DataFrame rows")
#df = df.sample(frac = 1)

#xhead=df.head()
#print(xhead)

print("")
SplitSize=0.3
print("## Data Preprocessing:") 
print("-> Validation fraction=",SplitSize," Training fraction=",1-SplitSize)
X_train, X_valid = train_test_split(df, test_size=SplitSize, random_state = config['seed'], shuffle=True)

# If you want to remove rows with some label (0) 
# X_train = X_train[X_train['Label'] == 0]
full = df.drop(['Label'], axis=1)
full = full.values
X_train = X_train.drop(['Label'], axis=1)
y_test  = X_valid['Label']
X_valid  = X_valid.drop(['Label'], axis=1)
X_train = X_train.values
X_valid  = X_valid.values
print('Training data size   :', X_train.shape)
print('Validation data size :', X_valid.shape)

input_dim = X_train.shape[1]

#### Load and initiate encoder, decoder and flow

encoder_path = config['ae_encoder']['path']+config['ae_encoder']['model']
decoder_path = config['ae_decoder']['path']+config['ae_decoder']['model']
autoencoder_path = config['ae_decoder']['path']
print("Loaded the encoder model from: ",encoder_path)
print("Loaded the decoder model from: ",decoder_path)
print("Loaded the autoencoder model from: ",autoencoder_path)

encoder = tf.keras.models.load_model( encoder_path )
decoder = tf.keras.models.load_model( decoder_path )
autoencoder = tf.keras.models.load_model( autoencoder_path )


print("-----> Initiate Encoder <-----")
encoder.summary()
encoder.compile( optimizer='adam', loss='mse') #, run_eagerly=True )

print("-----> Initiate Decoder <-----")
decoder.summary()
decoder.compile( optimizer='adam', loss='mse') #, run_eagerly=True )

print("-----> Initiate Autoencoder <-----")
autoencoder.summary()
autoencoder.compile( optimizer='adam', loss='mse') #, run_eagerly=True )


def get_likelihood():
  
    def likelihood(meanb,stdb):
        #mean = tf.reduce_mean(posterior) #({'z':z},as_dict=True)['x']
        #tf.print(mean)
        #mean = np.mean(posterior)
        tff = (tfd.MultivariateNormalDiag(loc=meanb,scale_diag=stdb))
        return tff #tfd.Independent

    return likelihood

### initiate flows in tensorflow hub
latent_size    = params['latent_layer']
batchsize      = config['batchsize']

data           = tf.placeholder(shape=[None,params['latent_layer']],dtype=tf.float32)
# input data, u-space
latent_sample  = tf.placeholder(shape=[None,params['latent_layer']],dtype=tf.float32)

MAP            = tf.placeholder_with_default(tf.zeros((batchsize,latent_size),tf.float32),shape=(batchsize,latent_size))

bs             = tf.placeholder_with_default(16,shape=[])

likelihood       = get_likelihood()
flow_model     = config['NF_output_model']['path']

prior          = get_prior(params['latent_layer'])
# Posterior prior to using flow given by input data into encoder
posterior      = encoder.predict(full,verbose=0)

#MAP            = tf.Variable(MAP_ini)
#MAP_reset      = tf.stop_gradient(MAP.assign(MAP_ini))

flow           = hub.Module(flow_model, trainable=False)

log_prob       = flow({'z_sample':data,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['log_prob']
#evaluate log prob of z'
MAP_prior      = flow({'z_sample':MAP,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['log_prob']
#this is the Gaussian probability in u space (used to isolate log det J)
#Only need backward projection into gaussian space for input into decoder
bwd_MAP            = flow({'z_sample':MAP,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['bwd_pass']
bwd                = flow({'z_sample':data,'sample_size':1, 'u_sample':np.zeros((1,params['latent_layer']))},as_dict=True)['bwd_pass']


### FLow Loss is defined by PDF of prior * jacobian of transformation
### PDF of prior is estimated by a dirac delta distributions. Approximation of the Maximum a Posteriori (MAP) solution with the AE encoded position 
### Let's define several loss functions to evaluate

### NLL (negative log likelihood)                                                done
### MSE between input and pae                                                    done
### MSE*sigma^-2    MSE divided by average per-feature reconstruction error      done
### for comparison MSE of original AE                                            done
### PAE estimate:  -1/2*||x-x'||^2*sigma^-2 -1/2*b(z)^2 + ln|det(jacobian)|      


# Let's do the easy MSE losses
mse = tf.keras.losses.MeanSquaredError()

sess = tf.Session()
sess.run(tf.global_variables_initializer())

decoder_input = sess.run(bwd,  feed_dict={data:posterior})

dec_predictions = decoder.predict(decoder_input,verbose=0)

# MSE between input and pae
train_loss_mse_t = mse(full,dec_predictions)
train_loss_mse = train_loss_mse_t.eval(session=sess)
avg_reco_error = tf.reduce_mean(dec_predictions)

# MSE*sigma^-2
train_lose_mse_sigma_t= (train_loss_mse / tf.math.square(avg_reco_error))
train_lose_mse_sigma = train_lose_mse_sigma_t.eval(session=tf.Session())
# MSE of AE
ae_predictions = autoencoder.predict(full)
train_loss_mse_ae_t = mse(full,ae_predictions)
train_loss_mse_ae = train_loss_mse_ae_t.eval(session=tf.Session())

# NLL
log_prob_of_data = sess.run(log_prob, feed_dict={data:posterior})
train_loss_nll_t = -tf.reduce_mean(log_prob_of_data)
train_loss_nll = train_loss_nll_t.eval(session=tf.Session())
# PAE loss:  -1/2*||x-x'||^2*sigma^-2 -1/2*b(z)^2 + ln|det(jacobian)|
MAP_prior = sess.run(MAP_prior,feed_dict={data:posterior})
bwd_MAP   = sess.run(bwd_MAP, feed_dict={data:posterior})

MAP_Gauss = prior.log_prob(bwd_MAP)

NF_Jac = MAP_prior - MAP_Gauss

meand, stdd = plot_pdf(dec_predictions,latent_size,figsDir,"pdf_decoded")

meanb = params['mean_before']
stdb = params['std_before']


MAP_likelihood = likelihood(meand,stdd).log_prob(full)

train_loss_pae_t = ((-1/2)*train_lose_mse_sigma - (1/2)*tf.math.square(MAP_likelihood) + tf.math.log(NF_Jac))
train_loss_pae = train_loss_pae_t.eval(session=sess)


### NLL (negative log likelihood)                                                done
### MSE between input and pae                                                    done
### MSE*sigma^-2    MSE divided by average per-feature reconstruction error      done
### for comparison MSE of original AE                                            done
### PAE estimate:  -1/2*||x-x'||^2*sigma^-2 -1/2*b(z)^2 + ln|det(jacobian)|      

print("MSE between input and pae", train_loss_mse)
print("MSE*sigma^-2", train_lose_mse_sigma)
print("MSE autoencoder: ",train_loss_mse_ae)
print("NLL loss: ",train_loss_nll)
print("PAE loss: ", train_loss_pae)

import ROOT
import sys

inFile = sys.argv[1]
print("reading from ", inFile)

ff = ROOT.TFile.Open(inFile,"READ")

histo = ff.Get("Loss_data_2015")
print(histo)
lossh = histo.Clone()

lossh.SetName("Loss")
lossh.Write("Loss")

ff.Close()
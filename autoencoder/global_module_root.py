import random
import sys
sys.path.append("modules/")

# import atlas styles
from AtlasStyle import *
from AtlasUtils import *
from array import *
from math import *
from ROOT import TH1D,TF1,TColor,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import math,sys,os 
import shapiro
import ROOT
from array import array
from decimal import Decimal
import numpy
import random
import sys,zipfile,json,math
from ROOT import gRandom

CMS=13000.0
Xmin=300
Xmax=9000

lumi2015=3244.54  # error 3.21 +-0.07 
Lumi2015=" %.1f fb^{-1}" % (lumi2015/1000.)
intLUMI2015="#int L dt = "+Lumi2015

lumi2016=33402.2  # error 3.21 +-0.07 
Lumi2016=" %.1f fb^{-1}" % (lumi2016/1000.)
intLUMI2016="#int L dt = "+Lumi2016

lumi2017=44630.6   # error 3.21 +-0.07 
Lumi2017=" %.1f fb^{-1}" % (lumi2017/1000.)
intLUMI2017="#int L dt = "+Lumi2017

# Nov
# lumi2018=43003.7 # error 3.21 +-0.07 
# Dec
lumi2018=58791.6 # error 3.21 +-0.07 
Lumi2018=" %.1f fb^{-1}" % (lumi2018/1000.)
intLUMI2018="#int L dt = "+Lumi2018

# lumi  in pb
lumi2015_2018=lumi2015+lumi2016+lumi2017+lumi2018
lumi=lumi2015_2018 # take into account missing files 

# new recomendation
# https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LuminosityForPhysics#2015_2018_13_TeV_proton_proton_f

Lumi=" %.0f fb^{-1}" % (lumi/1000.)
intLUMI="#int L dt = "+Lumi

# this is ROOT file after using the model with 1% (nominal)
root_model2use="root/analysis_median.root"


# lumi  in pb
lumi2015_2018=lumi2015+lumi2016+lumi2017+lumi2018
lumi=lumi2015_2018 # take into account missing files 

Lumi=" %.0f fb^{-1}" % (lumi/1000.)
intLUMI="#int L dt = "+Lumi


Lumi10=" %.0f fb^{-1}" % (0.1*lumi/1000.)
intLUMI10="#int L dt = "+Lumi10

# 10% of data. Remove this line for all data
# Lumi=" %.0f fb^{-1}" % (13.8)

UsedData0="#sqrt{s}=13 TeV, "+Lumi

print ("Lumi=",Lumi) 


#  dir with limit results
Limit_DIR="../stat/bayfram_2020/output"

#########################################################
# cut to select outlier events

# 20 pb WP
CutOutlier_20PB=-9.39

# MC region for 10 pb working point ("data limit") 
CutOutlier_10PB=-9.10

# WP region for 1 pb working point 
CutOutlier_1PB=-8.0

# WP region 0.1 pb
CutOutlier_01PB=-6.50



# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/XsecSummaryWjetsPowPy8Incl
# 361100-361104
# 2.65 + 2.65  + 4.83 + 2.41
powheg_lumi=(2.65+2.65+4.83+2.41)*1000  # pb-1
powheg_kfactor=1.0172
powheg_scale=(lumi/powheg_lumi)*powheg_kfactor

# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummaryTTbar
ttbar_cross=729.0 # pb
# 97% grid efficiency
ttbar_events=49874000*0.97
# ttbar_lumi=ttbar_file.Get("cutflow").GetBinContent(1) /(ttbar_cross*0.543) # pb
ttbar_lumi=ttbar_events /(ttbar_cross*0.543) # pb
ttbar_kfactor=1.195
ttbar_scale=(lumi/ttbar_lumi)*ttbar_kfactor


# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XsecSummarySingleTop
# Run 410659 and  410647 
stop_cross=22.175+36.996 # pb
# stop_events=5968000+6226000+6226000+6226000.. Rough number of input 
stop_events=9968000*6
# ttbar_lumi=ttbar_file.Get("cutflow").GetBinContent(1) /(ttbar_cross*0.543) # pb
stop_lumi=stop_events /stop_cross # pb
stop_kfactor=1.195
stop_scale=(lumi/stop_lumi)*stop_kfactor


# Dijet QCD
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetEtmissMC15
# in nb-1
pythia_lumi={}

# events/cros*eff 
# in nb
factors={}
factors[0]=2000000 / (7.8420E+07*1.0240E+0)
factors[1]=2000000 / (7.8420E+07*6.7198E-04)
factors[2]=1992000  / (2.4334E+06*3.3264E-04)
factors[3]=1767000  / (2.6454E+04*3.1953E-04)
factors[4]=1997000 /  (2.5464E+02*5.3009E-04)
factors[5]=1995000  / (4.5536E+00*9.2325E-04)
factors[6]=1997000 / (2.5752E-01*9.4016E-04)
factors[7]=1990000  / (1.6214E-02*3.9282E-04)
factors[8]=2000000 / (6.2505E-04*1.0162E-02)
factors[9]=2000000 / (1.9640E-05*1.2054E-02)
factors[10]=2000000 / (1.1961E-06*5.8935E-03)



DataLab="Data #sqrt{s}=13 TeV"
KinemCuts="E_{T}^{jet}>410 GeV  |#eta^{#gamma}|<2.5";
ATLASprel="ATLAS internal"
mcBkg="PYTHIA8"
mcSig="PYTHIA t#bar{t}"
mcPowheg="W+jet POWHEG"
mcPowhegTTbar="t#bar{t} POWHEG"
mcSTop="s-top POWHEG"
mcBkgHrw="HERWIG++ QCD"
mcBkgWJ="Multijets PYTHIA8"

DataLab2015="Data 2015 #sqrt{s}=13 TeV"
DataLab2016="Data 2016 #sqrt{s}=13 TeV"
DataLab2017="Data 2017 #sqrt{s}=13 TeV"
DataLab2018="Data 2018 #sqrt{s}=13 TeV"


mjjBinsL = [99,112,125,138,151,164,177,190, 203, 216, 229, 243, 257, 272, 287, 303, 319, 335, 352, 369, 387, 405, 424, 443, 462, 482, 502, 523, 544, 566, 588, 611, 634, 657, 681, 705, 730, 755, 781, 807, 834, 861, 889, 917, 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 13156];

mjjBins = array("d", mjjBinsL)



### Read configuration of the RMM matrix
### This is common for all

from os.path import exists

confile='data/config.json'
file_exists = exists( confile )

if (file_exists):
 with open( confile ) as json_file:
    data = json.load(json_file)
    maxNumber=int(data['maxNumber'])
    maxTypes=int(data['maxTypes'])
    mSize=int(data['mSize'])
    print("Read from file ",confile)
else:
    maxNumber=10
    maxTypes=5
    mSize=5

print ("maxNumber=",maxNumber," maxTypes=",maxTypes," mSize=",mSize) 
mSize=maxTypes*maxNumber+1;

# dijet invariant mass
x=1+0*maxNumber+1  # X position  
y=1+0*maxNumber    # Y position 
mjj=(x,y) #  index of Mjj  matrix ellement 

# PT of first jet
x=1+0*maxNumber  # X position  
y=1+0*maxNumber  # Y position 
pT=(x,y) #  index of Mjj  matrix ellement 


#  bb mass 
x=1+1*maxNumber+1
y=1+1*maxNumber
mbb=(x,y)

#  bj mass 
x=1+1*maxNumber+1
y=1+0*maxNumber
mbj=(x,y) 

# e+e- 
x=1+2*maxNumber+1
y=1+2*maxNumber
mee=(x,y)

# mu+mu 
x=1+3*maxNumber+1
y=1+3*maxNumber
mmumu=(x,y)


### This list contains excluded values for Z-score calculation
### We excluding pT of leading jet, Mjj and mbb
# excluded_val= ( pT, mjj, mbb)
excluded_val= (mjj, mbb)
print ("Excluded cells=",excluded_val) 

#### Exclusion values for RMM matrix #############
###################################


# dijet invariant mass
x=2 # X position  
y=1 # Y position 
inx1=x*mSize+y; #  index of hxw matrix ellement 

# pT1 
x=1 # X position  
y=1 # Y position 
inx2=x*mSize+y; #  index of hxw matrix ellement 

# Mjj for for light-jet + b-jets
x=1+maxNumber # X position  
y=1 # Y position 
inx3=x*mSize+y; #  index of hxw matrix ellement 

# pT1 for for b-jets
x=1+maxNumber # X position  
y=1+maxNumber # Y position 
inx4=x*mSize+y; #  index # pT for for b-jets

# Mjj for for 2-b jets 
x=2+maxNumber # X position  
y=1+maxNumber # Y position 
inx5=x*mSize+y; #  index of hxw matrix ellement 


# exlusion matrix for RMM in terms of indexes (how it is packed) 
excluded=(inx1,inx2,inx3,inx4,inx5)

## draw axis
def drawXAxis(sf,gPad,XMIN,YMIN,XMAX,YMAX,nameX,nameY,showXAxis=True, showYAxis=True):
 h=gPad.DrawFrame(XMIN,YMIN,XMAX,YMAX);
 ay=h.GetYaxis();
 ay.SetLabelFont(42)

 if (sf==1):
             ay.SetLabelSize(0.05)
             ay.SetTitleSize(0.06)

 if (sf==2 or sf==3):
             ay.SetLabelSize(0.10)
             ay.SetTitleSize(0.3)
 if (sf==20):
             ay.SetLabelSize(0.18)
 if (sf==30):
             ay.SetLabelSize(0.12)

# ay.SetTitleSize(0.1)
 ay.SetNdivisions(505);
 if (sf==1): ay.SetTitle( nameY )
 # ay.Draw("same")
 ax=h.GetXaxis();
 if (sf==1 or sf==2): ax.SetTitle( nameX );
 if (sf==30): ax.SetTitle( nameX );
 ax.SetTitleOffset(1.18)
 ay.SetTitleOffset(0.8)

 ax.SetLabelFont(42)
 # ax.SetTitleFont(42)
 ay.SetLabelFont(42)
 # ay.SetTitleFont(42)
 ax.SetLabelSize(0.12)
 ax.SetTitleSize(0.14)

 if (showXAxis==False):
         ax.SetLabelSize(0)
         ax.SetTitleSize(0)
 if (showYAxis):
          ay.SetLabelSize(0)
          ay.SetTitleSize(0)

 #ay.SetTitleSize(0.14)
 if (sf==30):
          ax.SetLabelSize(0.12)
          ax.SetTitleSize(0.12)
 if (sf==2 or sf==3):
             ay.SetLabelSize(0.12)
             ay.SetTitleSize(0.2)

 ax.Draw("same");
 ay.Draw("same");
 return h


# get acceptance as graph
def getAcceptance(h1, h2, color):
  """ h1 - accepted events, h2 - all events 
  """
  cross1=TGraphAsymmErrors() 
  cross1.SetLineColor( color )
  cross1.SetMarkerColor(1)
  cross1.SetMarkerSize(0.8)
  cross1.SetMarkerStyle(21)
  cross1.SetLineWidth(3)
  cross1.SetLineStyle(1)
  kk=0
  for i in range(1,h1.GetNbinsX()):
     center=h1.GetBinCenter(i)
     D = h1.GetBinContent(i);
     Derr = h1.GetBinError(i);
     D2 = h2.GetBinContent(i);
     Derr2 = h2.GetBinError(i);
     if (D2>0 and D>0):
       rat=D/float(D2)
       err=rat*sqrt(D)/float(D2)
       d=1-rat
       if (err>d): err=1-rat
       #print ("ratio for =", center, rat, err, " data=",D, D2) 
       cross1.SetPoint(kk,float(center), rat)
       dx1l=0
       dx1h=0
       dy1l=err
       dy1h=err
       if (rat+dy1h>0.999): dy1h=0
       cross1.SetPointError(kk,dx1l,dx1h,dy1l,dy1h)
       kk=kk+1
  return cross1

# calculate significance 
def signif(S,B):
     return math.sqrt(2* ( (S+B)*math.log(1+ float(S)/B)-S ) )


# calculate improvement in significance (in %) 
# This is bin-by-bin significance
def getDeltaSignificance(signal, signal_after, data, data_after, color = 2):
  """ 
  signal - signal histogram before AE
  signa_after -  signal histogram after AE
  data - data before AE
  data_after - data after AE
  """
  cross1=TGraphAsymmErrors()
  cross1.SetLineColor( color )
  cross1.SetMarkerColor(1)
  cross1.SetMarkerSize(0.8)
  cross1.SetMarkerStyle(21)
  cross1.SetLineWidth(3)
  cross1.SetLineStyle(1)
  kk=0
  for i in range(1,signal.GetNbinsX()):
     center=signal.GetBinCenter(i)
     # before AE
     D = data.GetBinContent(i);
     #Derr = data.GetBinError(i);
     # after AE
     D2 = data_after.GetBinContent(i);
     #Derr2 = data_after.GetBinError(i);
     # signal beore and after
     S= signal.GetBinContent(i);
     S2= signal_after.GetBinContent(i);
     if (D2>0 and D>0 and S>0):
       sig=signif(S,D)
       sig_after=signif(S2,D2)
       impr=( sig_after / sig - 1 )*100.0
       #print("Sig before=",sig," Sig after=",sig_after, "Improvement=",impr)
      
       cross1.SetPoint(kk,float(center), impr)
       dx1l=0
       dx1h=0
       dy1l=0
       dy1h=0
       cross1.SetPointError(kk,dx1l,dx1h,dy1l,dy1h)
       kk=kk+1
  #cross1.Print("All")
  return cross1

# calculate improvement in significance (in %) 
# This is significance for max of histogram and RMS 
def getDeltaSignificanceMeanRMS(signal, signal_after, data, data_after, mass=400, color = 1, isTeV=False):
  """ 
  signal - signal histogram before AE
  signa_after -  signal histogram after AE
  data - data before AE
  data_after - data after AE
  """
  cross1=TGraphAsymmErrors()
  cross1.SetLineColor( color )
  cross1.SetMarkerColor( color )
  cross1.SetMarkerSize(1.5)
  cross1.SetMarkerStyle(21)
  cross1.SetLineWidth(3)
  cross1.SetLineStyle(1)

  #print("Mass=",mass)

  #meanSignal=mass
  #minSignal=meanSignal- 0.2*mass
  #maxSignal=meanSignal+ 0.2*mass

  meanSignal=signal.GetMean() 
  minSignal=meanSignal-signal.GetRMS()
  maxSignal=meanSignal+signal.GetRMS()

  # redefine mass
  meanSignal=mass

  # this is the range in terms of bins
  binMin=signal.FindBin(minSignal)
  binMax=signal.FindBin(maxSignal)

  signalEvents=signal.Integral(binMin, binMax)
  signalEvents_after=signal_after.Integral(binMin, binMax)

  dataEvents=data.Integral(binMin, binMax)
  dataEvents_after=data_after.Integral(binMin, binMax)

  sig=signif(signalEvents, dataEvents)
  sig_after=signif(signalEvents_after, dataEvents_after)

  if (signalEvents<2): return cross1;

  impr=( sig_after / sig - 1 )*100.0
  print("mass=",mass, "sig=",sig," after=",sig_after, "improvement=",impr)

  factor=1.0
  if (isTeV): factor=0.001

  kk=0
  cross1.SetPoint(kk,float(meanSignal)*factor, impr)
  dx1l=0
  dx1h=0
  dy1l=0
  dy1h=0
  cross1.SetPointError(kk,dx1l*factor,dx1h*factor,dy1l,dy1h)
  return cross1




# get full predictions
# inpput: histogram name
# bins for histograms
# returns: total prediction and components
def getStandardModel(xfile, name,inLabel,bins=None):
  global ttbar_scale, powheg_scale, stop_scale

  # xfile.ls()
  ttot=xfile.Get(name+"_"+inLabel[0])
  ptot=xfile.Get(name+"_"+inLabel[1])
  stop=xfile.Get(name+"_"+inLabel[2])

  ttot.Scale(ttbar_scale)
  ptot.Scale(powheg_scale)
  stop.Scale(stop_scale)

  ptot.SetTitle("")
  ptot.SetStats(0)
  ptot.SetLineWidth(2)
  ptot.SetLineColor(1 )
  ptot.SetMarkerColor( 1 )
  ptot.SetFillColor(42);

  ttot.SetTitle("")
  ttot.SetStats(0)
  ttot.SetLineWidth(2)
  ttot.SetLineColor(1 )
  ttot.SetMarkerColor( 1 )
  ttot.SetFillColor(35);

  stop.SetTitle("")
  stop.SetStats(0)
  stop.SetLineWidth(2)
  stop.SetLineColor(1 )
  stop.SetMarkerColor( 1 )
  stop.SetFillColor(32);

  nameX=name+"_qcd_jz2"
  mqcd=(xfile.Get(nameX)).Clone()
  for i in range(3,12):
       nameX=name+"_qcd_jz"+str(i)  
       fqcd=xfile.Get(nameX)
       mqcd.Add( fqcd )


  MCTOT=ttot.Clone()

  MCTOT.SetLineColor(2)
  MCTOT.Add(ptot)
  MCTOT.Add(stop)
  # No QCD
  # MCTOT.Add(mqcd)

  # counting experiments
  countingErrors(MCTOT,None)
  countingErrors(mqcd,None)
  countingErrors(ptot,None)
  countingErrors(ttot,None)

  # divide by bins if needed
  if bins is not None: MCTOT.Divide(bins)
  if bins is not None: mqcd.Divide(bins)
  if bins is not None: ptot.Divide(bins)
  if bins is not None: ttot.Divide(bins)


  return MCTOT,mqcd,ptot,ttot,stop 


# residual plots: input histoogram, function, file name 
# http://sdittami.altervista.org/shapirotest/ShapiroTest.html
from module_functions import  Gauss
def showResiduals(hh,func,fname, MyMinX=-12,  MyMaxX=12, isKS=True):
   print ("showResiduals: Calculate residuals.") 
   MyBins=100
   res=TH1D("Residuals","Residuals",MyBins,MyMinX,MyMaxX);
   res.SetTitle("")
   res.SetStats(1)
   res.SetLineWidth(2)
   res.SetMarkerColor( 1 )
   res.SetMarkerStyle( 20 )
   res.SetMarkerSize( 0.8 )
   res.SetFillColor(42)
   nameX="D_{i} - F_{i} / #Delta D_{i}"
   nameY="Entries"
   FitMin=func.GetXmin()
   FitMax=func.GetXmax()
   print ("Fit min=",FitMin,"  max=",FitMax) 
   nres=0.0
   residuals=[]
   for i in range(1,hh.GetNbinsX()):
     center=hh.GetBinCenter(i)
     if (hh.GetBinContent(i)>0 and center>FitMin and center<FitMax):
       center=hh.GetBinCenter(i)
       x=hh.GetBinCenter(i)
       D = hh.GetBinContent(i);
       Derr = hh.GetBinError(i);
       B = func.Eval(center);
       frac=0
       if Derr>0:
          frac = (D-B)/Derr
       residuals.append(frac)
       res.Fill(frac)
       nres=nres+1.0
   res.SetStats(1)
   back=TF1("back",Gauss(),MyMinX,MyMaxX,3);
   back.SetNpx(200); back.SetLineColor(4); back.SetLineStyle(1)
   back.SetLineWidth(2)
   back.SetParameter(0,10)
   back.SetParameter(1,0)
   back.SetParameter(2,1.0)
   back.SetParLimits(2,0.1,1000)
   #back.SetParLimits(0,0.01,10000000)
   #back.SetParLimits(1,-5.0,5.0)
   #back.SetParLimits(2,0.0,5.0)

   #back.FixParameter(1,0)
   #back.FixParameter(2,1.0)


   nn=0
   chi2min=10000
   parbest=[]
   for i in range(10):
     fitr=res.Fit(back,"SMR0")
     print ("Status=",int(fitr), " is valid=",fitr.IsValid())
     if (fitr.IsValid()==True):
             chi2=back.GetChisquare()/back.GetNDF()
             if chi2<chi2min:
                    nn=nn+1
                    if nn>3:
                           break;
                    back.SetParameter(0,random.randint(0,10))
                    back.SetParameter(1,random.randint(-1,1))
                    back.SetParameter(2,random.randint(0,2.0))
                    par = back.GetParameters()

   #fitr=res.Fit(back,"SMR0")
   fitr.Print()
   print ("Is valid=",fitr.IsValid())

   par = back.GetParameters()
   err=back.GetParErrors()
   chi2= back.GetChisquare()
   ndf=back.GetNDF()
   print ("Chi2=", chi2," ndf=",ndf, " chi2/ndf=",chi2/ndf) 
   prob=fitr.Prob();
   print ("Chi2 Probability=",fitr.Prob());

   # make reference for normal
   norm_mean=0
   norm_width=1
   normal=TH1D("Normal with sig=1","Reference normal",MyBins,MyMinX,MyMaxX);
   normal.SetLineWidth(3)
   normal.SetLineColor(2)
   # normal.SetFillColor( 5 )

   maxe=5000
   r = TRandom3()
   for i in range (maxe) :
          xA = r.Gaus(norm_mean, norm_width)
          normal.Fill(xA)
   norM=nres/maxe
   normal.Scale(norM)
   pKSbinned = res.KolmogorovTest(normal)
   KSprob="KS prob ="+"{0:.2f}".format(pKSbinned)
   if (isKS): print (KSprob) 

   shapiro_prob=shapiro.ShapiroWilkW(residuals)
   Shapiro="ShapiroWilk ={0:.2f}".format(shapiro_prob)
   print (Shapiro) 

   gROOT.SetStyle("ATLAS");
   gStyle.SetOptStat(220002210);
   gStyle.SetStatW(0.32)
   c2=TCanvas("c","BPRE",10,10,600,540);
   c2.Divide(1,1,0.008,0.007);
   c2.SetBottomMargin(0.1)
   c2.SetTopMargin(0.05)
   c2.SetRightMargin(0.02)
   c2.SetLeftMargin(0.10)

   binmax = normal.GetMaximumBin();
   Ymax=normal.GetBinContent(normal.FindBin(0));
   for i in xrange(1,res.GetNbinsX()):
      if res.GetBinContent(i)>Ymax: Ymax=res.GetBinContent(i);
   Ymax=1.15*Ymax;

   #h=gPad.DrawFrame(MyMinX,0,MyMaxX,Ymax)

   ps2 = TPostScript( fname,113)

   res.SetStats(1)
   gStyle.SetOptStat(220002200);
   gStyle.SetStatW(0.32)

   res.SetAxisRange(0, Ymax,"y");
   res.Draw("histo")
   back.Draw("same")
   if (isKS): normal.Draw("histo same")
   leg2=TLegend(0.11, 0.6, 0.39, 0.90);
   leg2.SetBorderSize(0);
   leg2.SetTextFont(62);
   leg2.SetFillColor(10);
   leg2.SetTextSize(0.04);
   leg2.AddEntry(res,"Residuals","f")
   leg2.AddEntry(back,"Gauss fit","l")
   mean= "mean="+"{0:.2f}".format(par[1])
   mean_err= "#pm "+"{0:.2f}".format(err[1])
   sig= "#sigma="+"{0:.2f}".format(par[2])
   sig_err= "#pm "+"{0:.2f}".format(err[2])
   leg2.AddEntry(back,mean+mean_err,"")
   leg2.AddEntry(back,sig+sig_err,"")
   leg2.AddEntry(back,"#chi^{2}/ndf="+"{0:.2f}".format(chi2/ndf)+"(p="+"{0:.2f})".format(prob),"")
   leg2.AddEntry(back,Shapiro,"")
   if (isKS): leg2.AddEntry(normal,"Normal (#sigma=1)","l")
   if (isKS): leg2.AddEntry(back,KSprob,"")

   leg2.Draw("same");
   ax=res.GetXaxis();
   ax.SetTitle( nameX );
   ay=res.GetYaxis();
   ay.SetTitle( nameY );
   ax.SetTitleOffset(1.0); ay.SetTitleOffset(1.0)
   ax.Draw("same")
   ay.Draw("same")
   gPad.RedrawAxis()
   c2.Update()
   ps2.Close()
   c2.Close();
   print (fname, " done") 


def style3par(back):
     back.SetNpx(100); back.SetLineColor(4); back.SetLineStyle(1)
     back.SetLineWidth(2)
     back.SetParameter(0,4.61489e-02)
     back.SetParameter(1,1.23190e+01)
     back.SetParameter(2,3.65204e+00)

     #back.SetParameter(3,-6.81801e-01)
     #back.SetParLimits(0,0,100)
     # back.SetParLimits(1,0,12)
     #back.SetParLimits(2,-100,100)
     return back

def style5par(back):
     back.SetNpx(200); back.SetLineColor(4); back.SetLineStyle(1)
     back.SetLineWidth(2)
     back.SetParameter(0,6.0e+25)
     back.SetParameter(1,80)
     back.SetParameter(2, 40)
     back.SetParameter(3,11)
     back.SetParameter(4, 1.0)
     # back.SetParLimits(0,0,10000)
     back.SetParLimits(1,0,10000)
     back.SetParLimits(2,-100,100)
     back.SetParLimits(3,-40,40)
     return back

# get width of the bin near the mass
def getBinWidth(bins,peak):
    imean = bins.FindBin(peak)
    return bins.GetBinCenter(imean+1) - bins.GetBinCenter(imean);

def countingErrors(hhh,bins=None):
  for i in range(1, hhh.GetNbinsX()):
     D = hhh.GetBinContent(i);
     Berr=0
     if (D>0): Berr = sqrt(D);
     hhh.SetBinError(i,Berr)

def GetZVal (p, excess) :
  #the function normal_quantile converts a p-value into a significance,
  #i.e. the number of standard deviations corresponding to the right-tail of 
  #a Gaussian
  if excess :
    zval = ROOT.Math.normal_quantile(1-p,1);
  else :
    zval = ROOT.Math.normal_quantile(p,1);

  return zval



ffc=1 # convert nb to pb (already in pb!) 
# Charged Higgs H+t cross sections for tan(beta)=1
# run,mass,xcross(pb),events

#systematics
sysname={}

# 2021 analysis. Need to update 
sysname[0]="Nominal"
sysname[1]="JET_EffectiveNP_Detector1__1up"
sysname[2]="JET_EffectiveNP_Detector1__1down"
sysname[3]="JET_EffectiveNP_Detector2__1up"
sysname[4]="JET_EffectiveNP_Detector2__1down"
sysname[5]="JET_EffectiveNP_Mixed1__1up"
sysname[6]="JET_EffectiveNP_Mixed1__1down"
sysname[7]="JET_EtaIntercalibration_TotalStat__1up"
sysname[8]="JET_EtaIntercalibration_TotalStat__1down"
sysname[9]="JET_EtaIntercalibration_NonClosure_highE__1up"
sysname[10]="JET_EtaIntercalibration_NonClosure_highE__1down"
sysname[11]="JET_EtaIntercalibration_NonClosure_negEta__1up"
sysname[12]="JET_EtaIntercalibration_NonClosure_negEta__1down"
sysname[13]="JET_EtaIntercalibration_NonClosure_posEta__1up"
sysname[14]="JET_EtaIntercalibration_NonClosure_posEta__1down"
sysname[15]="JET_JER_DataVsMC_AFII__1up"
sysname[16]="JET_JER_DataVsMC_AFII__1down"
sysname[17]="JET_RelativeNonClosure_AFII__1up"
sysname[18]="JET_RelativeNonClosure_AFII__1down"
sysname[19]="JET_PunchThrough_AFII__1up"
sysname[20]="JET_PunchThrough_AFII__1down"
sysname[21]="JET_JER_EffectiveNP_1__1up"
sysname[22]="JET_JER_EffectiveNP_1__1down"
sysname[23]="JET_JER_EffectiveNP_2__1up"
sysname[24]="JET_JER_EffectiveNP_2__1down"
sysname[25]="JET_JER_EffectiveNP_3__1up"
sysname[26]="JET_JER_EffectiveNP_3__1down"
sysname[27]="JET_JER_EffectiveNP_4__1up"
sysname[28]="JET_JER_EffectiveNP_4__1down"
sysname[29]="JET_JER_EffectiveNP_5__1up"
sysname[30]="JET_JER_EffectiveNP_5__1down"
sysname[31]="JET_JER_EffectiveNP_6__1up"
sysname[32]="JET_JER_EffectiveNP_6__1down"
sysname[33]="JET_JER_EffectiveNP_7__1up";
sysname[34]="JET_JER_EffectiveNP_7__1down";
sysname[35]="JET_JER_EffectiveNP_8__1up";
sysname[36]="JET_JER_EffectiveNP_8__1down";
sysname[37]="JET_JER_EffectiveNP_9__1up";
sysname[38]="JET_JER_EffectiveNP_9__1down";
sysname[39]="JET_JER_EffectiveNP_10__1up";
sysname[40]="JET_JER_EffectiveNP_10__1down";
sysname[41]="JET_JER_EffectiveNP_11__1up";
sysname[42]="JET_JER_EffectiveNP_11__1down";
sysname[43]="JET_BJES_Response__1up";
sysname[44]="JET_BJES_Response__1down";
sysname[45]="JET_Flavor_Composition__1up";
sysname[46]="JET_Flavor_Composition__1down";
sysname[47]="JET_Pileup_OffsetNPV__1up";
sysname[48]="JET_Pileup_OffsetNPV__1down";
sysname[49]="JET_Pileup_OffsetMu__1up";
sysname[50]="JET_Pileup_OffsetMu__1down";
sysname[51]="JET_SingleParticle_HighPt__1up";
sysname[52]="JET_SingleParticle_HighPt__1down";
sysname[53]="MUON_SAGITTA_RESBIAS__1up";
sysname[54]="MUON_SAGITTA_RESBIAS__1down";
sysname[55]="MUON_SCALE__1up";
sysname[56]="MUON_SCALE__1down";
sysname[57]="MUON_ID__1up";
sysname[58]="MUON_ID__1down";
sysname[59]="EG_SCALE_ALL__1up";
sysname[60]="EG_SCALE_ALL__1down";
sysname[61]="EG_RESOLUTION_ALL__1up";
sysname[62]="EG_RESOLUTION_ALL__1down";

# NEW 2019!
# include more mass points for mhmodm. pb for xSec*BR
# run,mass,xcross(pb),events, accepted (truth)
HPlusTtanb05_mhmodm=[
     [450597,600, 1.33184498853*ffc,56076,0,0],
     [450001,700, 0.733653118278*ffc,67642,0,0],
     [450002,800, 0.422458344*ffc,14891,0,0],
     [450003,900, 0.2509698854*ffc,13416,0,0],
     [450004,1000, 0.15352390059*ffc,18490,0,0],
     [450598,1200, 0.0615319788762*ffc,81794,0,0],
     [450599,1400, 0.0265022945008*ffc,20921,0,0],
     [450600,1600, 0.012104770414*ffc,30215,0,0],
     [450601,1800, 0.0057522212784*ffc,20921,0,0],
     [450602,2000, 0.00305527*ffc,55382,0,0]
]

DHPlusEvents={}
HPlus=[]
# recalculate input events
for j in range(len(HPlusTtanb05_mhmodm)):
       mmm=HPlusTtanb05_mhmodm[j]
       run=mmm[0]
       mass=mmm[1]
       xsec=mmm[2]
       inputevents=mmm[3]
       #inputevents=tt.Get("cutflow").GetBinContent(1)
       #print run, mass, xsec, inputevents
       HPlus.append([run,mass,xsec,inputevents])
       effacc=1
       DHPlusEvents[mmm[0]] = mmm[2]*lumi * effacc  # events*eff*acc 


############ Composite Lepton ##########################################
ffc=1 # convert nb to pb (already in pb?!) 
# run,mass,massX,xcross(pb),events, Mjjll, Mjjl accepted 
ComLep=[
   [506946, 250, 500, 16270, 14622, 1.39E+00],
   [506947, 250, 1000, 17670, 16375, 6.05E-02],
   [506948, 500, 1000, 18254, 17729, 6.00E-02],
   [506949, 750, 1000, 18522, 17669, 2.81E-02],
   [506950, 250, 2000, 17544, 16401, 7.84E-04],
   [506951, 500, 2000, 18331, 17999, 1.07E-03],
   [506952, 750, 2000, 18484, 18231, 9.22E-04],
   [506953, 1000, 2000, 18623, 18358, 7.25E-04],
   [506954, 1250, 2000, 18488, 18215, 5.03E-04],
   [506955, 1500, 2000, 18200, 17925, 2.68E-04],
   [506956, 1750, 2000, 17850, 17343, 8.56E-05],
   [506957, 500, 3000, 18187, 17680, 5.83E-05],
   [506958, 1000, 3000, 18443, 18220, 3.40E-05],
   [506959, 1500, 3000, 18370, 18212, 2.01E-05],
   [506960, 2000, 3000, 17924, 17761, 9.05E-06],
   [506961, 2500, 3000, 17411, 17161, 2.48E-06],
   [506962, 500, 4000, 18123, 17423, 9.86E-06],
   [506963, 1000, 4000, 18455, 18140, 3.41E-06],
   [506964, 1500, 4000, 18226, 18068, 1.53E-06],
   [506965, 2000, 4000, 17947, 17819, 7.91E-07],
   [506966, 2500, 4000, 17399, 17286, 3.79E-07],
   [506967, 3000, 4000, 17060, 16893, 1.60E-07],
   [506968, 3500, 4000, 16611, 16379, 4.68E-08]
  ]


# Nr of events for this lumi
DComLep={}
for j in range(len( ComLep  )):
       mmm=ComLep[j]
       ngen=float(mmm[2])
       effacc=(mmm[3]/ngen) * (mmm[3]/ngen) # eff*accep
       effacc=1
       DComLep[mmm[0]] = mmm[5]*lumi * effacc  # events*eff*acc 


from os import path
# #####################  charged Higgs
HPlus_file=[]
for i in range(len(HPlus)):
  xf=HPlus[i]
  xrun=xf[0]
  xxx=[]
  sss=[]
  for nn in sysname:
     yyy=[]
     yyy.append(nn)          # order 
     yyy.append(sysname[nn]) # name of systematics
     rootfile="../analysis/out/sys"+str(nn)+"/mc_hplus2021/"+str(xrun)+".root"
     if (path.exists(rootfile)):
          tt=TFile(rootfile) 
          yyy.append( tt  )
          sss.append(yyy)
  xxx.append(xrun)
  xxx.append(sss)
  HPlus_file.append(xxx)


# ##################### composite leptons 
ComLep_file=[]
for i in range(len(ComLep)):
  xf=ComLep[i]
  xrun=xf[0]
  xxx=[]
  sss=[]
  for nn in sysname:
     yyy=[]
     yyy.append(nn)          # order 
     yyy.append(sysname[nn]) # name of systematics
     rootfile="../analysis/out/sys"+str(nn)+"/mc_complep2021/"+str(xrun)+".root"
     if (path.exists(rootfile)):
          tt=TFile(rootfile)
          yyy.append( tt  )
          sss.append(yyy)
  xxx.append(xrun)
  xxx.append(sss)
  ComLep_file.append(xxx)


###### Radion model #################### 
ffc=1 # convert nb to pb (already in pb?!) 
# run,mass,xcross(pb),events, Mjjll, Mjjl accepted 
RadionM=[
         [506939,500, 2.26*ffc,20000, 14,  7961],
         [506940,1000, 0.0658*ffc,20000, 145, 10438],
         [506941,2000, 0.0005266*ffc,20000, 463, 11769],
         [506942,3000, 0.0000153837*ffc,20000, 813, 12493],
         [506943,4000, 0.0000006825*ffc,20000, 1147, 13106],
         [506944,5000, 0.0000000305549*ffc,20000, 1430, 13850],
         [506945,6000, 0.0000000012557*ffc,20000, 1767, 14454]
        ]

# Nr of events for this lumi
DRadionEvents={}
for j in range(len( RadionM  )):
       mmm=RadionM[j]
       ngen=float(mmm[3])
       effacc=(mmm[4]/ngen) * (mmm[5]/ngen) # eff*accep
       effacc=1
       DRadionEvents[mmm[0]] = mmm[2]*lumi * effacc  # events*eff*acc 


# SSM
WPrime=[
         [801123,750,4.50E-03*1000,10000,4289,3756],
         [801124,1250,3.41E-04*1000,10000,4833,4397],
         [801125,2250,1.07E-05*1000,10000,5471,5111],
         [801126,3250,7.27E-07*1000,10000,5766,5437],
         [801127,4250,6.49E-08*1000,10000,6090,5732],
         [801128,5250,5.51E-09*1000,10000,6205,5833],
         [801129,6250,4.61E-10*1000,10000,6359,6024]
       ]

# Nr of events for this lumi
WPrimeEvents={}
for j in range(len( WPrime )):
       mmm=WPrime[j]
       ngen=float(mmm[3])
       effacc=(mmm[4]/ngen) * (mmm[5]/ngen) # eff*accep
       effacc=1
       WPrimeEvents[mmm[0]] = mmm[2]*lumi * effacc


########### Simplified DM models
ffc=1 # convert nb to pb (already in pb?!) 
# run,mass,xcross(pb),events, Mjjll, Mjjl accepted 
WZsim=[
         [502946,700,0.1119509*ffc,20000, 824,  9799],
         [502947,1200,0.024642*ffc,20000, 1300, 11997],
         [502948,2000,0.003264*ffc,20000, 1688, 13402],
         [502949,3000,0.0003579*ffc,20000, 1866, 14050],
         [502950,4000,0.0000463*ffc,20000, 1988, 14521],
         [502951,5000,0.000006583*ffc,20000, 2147, 14804],
         [502952,6000,0.000001031*ffc,20000, 2285, 14827]
        ]

# Nr of events for this lumi
DMsimEvents={}
for j in range(len( WZsim )):
       mmm=WZsim[j]
       ngen=float(mmm[3])
       effacc=(mmm[4]/ngen) * (mmm[5]/ngen) # eff*accep
       effacc=1
       DMsimEvents[mmm[0]] = mmm[2]*lumi * effacc  # events*eff*acc 


# Return events after full simulation visible after the
# acceptance and efficiency cuts
# it also applies trigger inefficiency correction
# Note we shift by one bin since fits always use +1 bin
def getVisibleEvents(h,trigger_eff_corr=0.99):
         sumx=h.Integral(h.FindBin(Xmin)-1,1000000)*trigger_eff_corr
         return sumx


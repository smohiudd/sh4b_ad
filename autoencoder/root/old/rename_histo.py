import ROOT
import sys

inFile = sys.argv[1]
print("reading from ", inFile)

ff = ROOT.TFile.Open(inFile,"READ")
histo = ff.Get("Loss_data_2016")
b = histo.GetNbinsX()
print(b)
print(histo)
lossh = histo.Clone()

ff.Close()

ff = ROOT.TFile.Open(inFile,"WRITE")
#lossh.SetName("Loss")
lossh.Write("Loss")
ff.Close()


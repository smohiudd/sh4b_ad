# Anomaly detection https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.10.3542&rep=rep1&type=pdf
# Stouffer's Z-score

import sys
sys.path.append("modules/")
from AtlasStyle import *
from AtlasUtils import *
from global_module import *

from ROOT import TH1D,TF1,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import ROOT


print ('Number of arguments:', len(sys.argv), 'arguments.') 
print ('Argument List:', str(sys.argv))
print ('Use as: script.py -b 0 (or 1,2)') 
myinput="interactive"
if (len(sys.argv) ==2):
   myinput = sys.argv[1]
print ("Mode=",myinput) 


gROOT.Reset()
figdir="figs/"
name=os.path.basename(__file__)
epsfig=figdir+name.replace(".py",".eps")

nameX="Event type"
nameY="Events"
Ymin=0.0001
Ymax=35000000 
Xmin=1
Xmax=8

######################################################
gROOT.SetStyle("Plain");
c1=TCanvas("c_massjj","BPRE",10,10,700,700);
c1.Divide(3,3,0.008,0.007);
ps1 = TPostScript( epsfig,113)

c1.cd(1);
gPad.SetLogy(0)
gPad.SetLogx(0)
#gPad.SetTopMargin(0.05)
#gPad.SetBottomMargin(0.15)
gPad.SetLeftMargin(0.14)
#gPad.SetRightMargin(0.04)


#h=gPad.DrawFrame(Xmin,Ymin,Xmax,Ymax);
#h.Draw()


hhall=[]

labels=["met ", "1 \ell ", "2 \ell ", "1 #gamma ", "2 #gamma ", "1j ", "4j "]

for i in range( len(labels) ):
   #file1="../analysis/out/t"+str(i)+"/sys0/data/data_2015_2016_2017_2018.root"
   # file1="../analysis/out/t"+str(i)+"/sys0/data/data_2015_2016_2017_2018.root"
   file1="../analysis/out/t"+str(i+1)+"/sys0/data2022/data.root"
   xfile1=TFile(file1)
   hh=xfile1.Get("triggers_selected")
   hh.SetDirectory(0)
   hhall.append(hh)
   xfile1.Close()

#for j in range(len(labels)):
# h.GetXaxis().SetBinLabel(h.GetXaxis().FindBin(j), labels[j]);

for i in range(len(labels)):
     c1.cd(i+1)
     c1.SetTopMargin(0.02)
     c1.SetBottomMargin(0.08)
     c1.SetRightMargin(0.08)
     gPad.SetLeftMargin(0.15)

     ymax=Ymax
     if (i==0): ymax=6000000
     if (i==3): ymax=30000000
     if (i==4): ymax=2000000
     if (i==1): ymax=50000000
 
     h=gPad.DrawFrame(Xmin,Xmax,Xmax,ymax);
     hh=hhall[i]
     hh.SetTitle("")
     hh.SetStats(0)
     hh.SetFillColor(i+2)
     if (i==0): ATLASLabel(0.6,0.8,0.16,0.03)

     ax=h.GetXaxis(); ax.SetTitleOffset(1.1)
     #ax.SetTitle( nameX );
     ay=h.GetYaxis();
     ay.SetTitle( nameY );
     ay.SetTitleOffset(1.6)
     ay.Draw("same")
     ax.SetLabelSize( 0.30 )

     for j in range(len(labels)):
       h.GetXaxis().SetBinLabel(h.GetXaxis().FindBin(j+1+0.5), labels[j]);
       h.GetXaxis().SetLabelSize(0.075)
     h.Draw()
     gr=TH1toTGraphError(hh)
     gr.SetFillColor(40);
     #gr.Draw("AEB same")

     hh.SetFillColor(40)
     hh.Draw("same histo")
     myText(0.29,0.8,1,0.06, labels[i]+" (HLT+selection)")
      



#myText(0.69,0.6,1,0.04,"14 fb^{1} x 10")
#myText(0.69,0.5,1,0.04,intLUMI)
#ATLASLabel(0.19,0.89,0.14,0.03)


print (epsfig) 
gPad.RedrawAxis()
c1.Update()
ps1.Close()
if (myinput != "-b"):
              if (input("Press any key to exit") != "-9999"):
                         c1.Close(); sys.exit(1);





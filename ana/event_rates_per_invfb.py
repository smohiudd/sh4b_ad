# Anomaly detection https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.10.3542&rep=rep1&type=pdf
# Stouffer's Z-score

import sys
sys.path.append("modules/")
from AtlasStyle import *
from AtlasUtils import *
from global_module import *

from ROOT import TH1D,TF1,TProfile2D,TEllipse, THStack,TRandom3,TFile,TLatex,TLegend,TPaveText,TGraphErrors,kRed,kBlue,kGreen,kCyan,kAzure,kYellow,kTRUE
import ROOT


print ('Number of arguments:', len(sys.argv), 'arguments.') 
print ('Argument List:', str(sys.argv))
print ('Use as: script.py -b 0 (or 1,2)') 
myinput="interactive"
if (len(sys.argv) ==2):
   myinput = sys.argv[1]
print ("Mode=",myinput) 


gROOT.Reset()
figdir="figs/"
name=os.path.basename(__file__)
epsfig=figdir+name.replace(".py",".eps")

nameX="Event type"
nameY="Event rate [1/fb^{1}]"
Ymin=0.0001
Ymax=2000000 
Xmin=-0.1 
Xmax=7

######################################################
gROOT.SetStyle("Plain");
c1=TCanvas("c_massjj","BPRE",10,10,600,500);
c1.Divide(1,1,0.008,0.007);
ps1 = TPostScript( epsfig,113)

c1.cd(1);
gPad.SetLogy(0)
gPad.SetLogx(0)
gPad.SetTopMargin(0.05)
gPad.SetBottomMargin(0.12)
gPad.SetLeftMargin(0.14)
gPad.SetRightMargin(0.04)

labels=["met ", "1 \ell ", "2 \ell ", "1 #gamma ", "2 #gamma ", "1j ", "4j "]

h=gPad.DrawFrame(Xmin,Ymin,Xmax,Ymax);

for j in range(len(labels)):
       h.GetXaxis().SetBinLabel(h.GetXaxis().FindBin(j), labels[j]);
       h.GetXaxis().SetLabelSize(0.075)
h.Draw()

ax=h.GetXaxis(); 
ax.SetTitleOffset(1.1)
ay=h.GetYaxis();
ay.SetTitle( nameY );
#ay.SetTitleSize( 0.1 );
ay.SetTitleOffset(1.6)
ay.Draw("same")

cross2=TGraphErrors()
cross2.SetLineColor( 4 )
cross2.SetMarkerColor( 4 )
cross2.SetMarkerSize(1)
cross2.SetMarkerStyle(20)
cross2.SetLineWidth(3)
cross2.SetLineStyle(1)

cross3=TGraphErrors()
cross3.SetLineColor( 2 )
cross3.SetMarkerColor( 2 )
cross3.SetMarkerSize(1)
cross3.SetMarkerStyle(21)
cross3.SetLineWidth(3)
cross3.SetLineStyle(1)


LumiRUN2=140;
LumiRUN2error=0.82; # 0.82%

LumiRUN3=26;
LumiRUN3error=2.2; # 2.2%

for i in range(7):
   #file1="../analysis/out/t"+str(i)+"/sys0/data2015/data.root"
   file1="../analysis/out/t"+str(i+1)+"/sys0/data/data_2015_2016_2017_2018.root"
   xfile1=TFile(file1)
   events=(xfile1.inputNN).GetEntries()
   print (i,") RMM events=",(xfile1.inputNN).GetEntries()  )
   xfile1.Close()
   cross2.SetPoint(i,i, float(events)/LumiRUN2 )
   D1=float(events)/LumiRUN2
   D2=float(events)/(LumiRUN2+LumiRUN2*LumiRUN2error*0.01)
   Err=D1-D2 
   cross2.SetPointError(i, 0, Err )

   file2="../analysis/out/t"+str(i+1)+"/sys0/data2022/data.root"
   xfile2=TFile(file2)
   events=(xfile2.inputNN).GetEntries()
   print (i,") RMM events RUN3=",(xfile2.inputNN).GetEntries()  )
   xfile2.Close()
   cross3.SetPoint(i,i+0.05, float(events)/LumiRUN3 )
   D1=float(events)/LumiRUN3
   D2=float(events)/(LumiRUN3+LumiRUN3*LumiRUN3error*0.01)
   Err=D1-D2
   cross3.SetPointError(i, 0, Err )



for i in range(7):
   cross2.GetXaxis().SetBinLabel(cross2.GetXaxis().FindBin(i+1), labels[i]);  # Find out which bin on the x-axis the point co

cross2.Draw("same pe")
cross2.Print("all")

cross3.Draw("same pe")

"""
ax=h.GetXaxis(); ax.SetTitleOffset(0.8)
ax.SetTitle( nameX );
ay=h.GetYaxis(); ay.SetTitleOffset(0.8)
ay.SetTitle( nameY );
ax.SetTitleOffset(1.1); ay.SetTitleOffset(1.8)
ax.Draw("same")
ay.Draw("same")
"""

ax=cross2.GetXaxis(); ax.SetTitleOffset(0.8)
ax.SetTitle( "" );
ay=cross2.GetYaxis(); ay.SetTitleOffset(0.8)
ay.SetTitle( nameY );
ax.SetTitleOffset(1.3); ay.SetTitleOffset(1.8)

ax.SetLabelSize( 0.08 );
ax.Draw("same")
ay.Draw("same")


leg2=TLegend(0.32, 0.8, 0.5, 0.91);
leg2.SetBorderSize(0);
leg2.SetTextFont(62);
leg2.SetFillColor(10);
leg2.SetTextSize(0.04);
if (1==1):
         leg2.AddEntry(cross2,"Run2","pl")
         leg2.AddEntry(cross3,"Run3","pl")
leg2.Draw("same")

#myText(0.69,0.6,1,0.04,"14 fb^{1} x 10")
#myText(0.69,0.5,1,0.04,intLUMI)
ATLASLabel(0.69,0.89,0.14,0.03)


print(epsfig) 
gPad.RedrawAxis()
c1.Update()
ps1.Close()
if (myinput != "-b"):
              if (input("Press any key to exit") != "-9999"):
                         c1.Close(); sys.exit(1);





#!/bin/bash
echo "Setup ROOT, PyROOT tensorflow"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "views LCG_103 x86_64-centos7-gcc11-opt"
# lsetup "views LCG_104 x86_64-centos7-gcc11-opt"

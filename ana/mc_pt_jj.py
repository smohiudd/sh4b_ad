import sys
sys.path.append("modules/")
import math

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
print 'Use as: script.py -b 0 (or 1,2)'
myinput="interactive"
channel="jj"
# trigger type
trig_type=0

if (len(sys.argv) ==2):
   channel =sys.argv[1]
if (len(sys.argv) ==3):
   channel =sys.argv[1]
   trig_type=sys.argv[2]
if (len(sys.argv) == 4):
   channel =sys.argv[1]
   trig_type=sys.argv[2]
   myinput = sys.argv[3]

print "Mode=",myinput
print "channel=",channel


Xmin=100
Xmax=7000
Ymin=0.11
Ymax=1000000000000-10000


############# Configs ##############
nameX="pT  [GeV]"
MLabel="jj"
NoBottom=False
NoLeft=False
nameY="Events"
namdYL="Sign."

## defaults 
#name="Mjj_data_LECR"
name="Mjj"

Position="1x3" #position 
# 1st column...
# channel by channel 
if channel == "jj":
  Position="1x1" #position 
  MLabel="j+j"
  name="Mjj_data100percent"  

# channel by channel 
if channel == "je":
  Position="1x2" #position 
  MLabel="j + e"
  name="Mje_data100percent" 

# channel by channel 
if channel == "jm":
  Position="1x3" #position 
  MLabel="j + \mu"
  name="Mjm_data100percent" 

# 2nd colums..################################
if channel == "jb":
  Position="2x1" #position 
  MLabel="j + b\mbox{-}jet"
  name="Mjb_data100percent"

# channel by channel 
if channel == "be":
  Position="2x2" #position 
  MLabel="b\mbox{-}jet + e"
  name="Mbe_data100percent"

# channel by channel 
if channel == "bm":
  Position="2x3" #position 
  MLabel="b\mbox{-}jet + \mu"
  name="Mbm_data100percent"

## 3rd column
if channel == "bb":
  Position="3x1" #position 
  MLabel="2 b\mbox{-}jet"
  name="Mbb_data100percent"

if channel == "jg":
  Position="3x2" #position 
  MLabel="j + \gamma"
  name="Mjg_data100percent"

if channel == "bg":
  Position="3x3" #position 
  MLabel="b\mbox{-}jet + \gamma"
  name="Mbg_data100percent"


if (Position =="1x1"):
    NoBottom=True
    NoLeft=False
    nameX=""

if (Position =="1x2"):
    NoBottom=True
    NoLeft=True 
    nameX=""

if (Position =="1x3"):
    NoBottom=True
    NoLeft=True
 
if (Position =="2x1"):
    NoBottom=True
    NoLeft=True 
    nameX=""
    nameY=""
    namdYL=""

if (Position =="2x2"):
    NoBottom=True
    NoLeft=True
    nameX=""
    nameY=""
    namdYL=""

if (Position =="2x3"):
    NoBottom=True
    NoLeft=True
    nameY=""
    namdYL=""

if (Position =="3x1"):
    NoBottom=True
    NoLeft=True
    nameX=""
    nameY=""
    namdYL=""

if (Position =="3x2"):
    NoBottom=True
    NoLeft=True 
    nameX=""
    nameY=""
    namdYL=""

if (Position =="3x3"):
    NoBottom=False
    NoLeft=True 
    nameY=""
    namdYL=""

print("Position:",Position)
print("No bottom:",NoBottom)
print("No left:",NoLeft)

##################################

# overwite to make it correct 
NoBottom=False
NoLeft=False


## no bottom
if (NoBottom):
    nameX="";


# import atlas styles
from AtlasStyle import *
from AtlasUtils import *
from initialize  import *
from global_module import *
from functions import *
from module_functions import *

gROOT.Reset()
figdir="figs/"

name=os.path.basename(__file__)
name=name.replace(".py","")
name=name+"_"+channel
epsfig=figdir+name+".eps"


YRMIN=-4.999 
YRMAX=4.999 

######################################################
gROOT.SetStyle("Plain");


xwin=600
ywin=600
#if (NoLeft==True): xwin=600-int(600*0.13) 
#if (NoBottom==True): ywin=600-int(600*0.1)

c1=TCanvas("c","Mass",10,10,xwin,ywin);

c1.SetFrameBorderMode(0);
ps1 = TPostScript( epsfig,113)
#c1.Divide(1,1,0.0,0.0);
c1.SetTickx()
c1.SetTicky()
c1.SetTitle("")
c1.SetLineWidth(3)
c1.SetBottomMargin(0.1)
#if (NoBottom): c1.SetBottomMargin(0.01)
c1.SetTopMargin(0.0)
c1.SetRightMargin(0.01)
c1.SetFillColor(0)

#### the main plot
pad1 = TPad("pad1","pad1",0,0.0,1,0.99)
#pad1.SetBottomMargin(0.0)
pad1.SetLeftMargin(0.14)
if (NoLeft==True): pad1.SetLeftMargin(0.0)

pad1.SetRightMargin(0.01)
pad1.SetTopMargin(0.01)
pad1.Draw()
pad1.cd()
pad1.SetLogy(1)
pad1.SetLogx(1)
#if (NoLeft):
#    ch=drawXAxis(1,pad1,Xmin, Ymin, Xmax, Ymax,nameX,nameY,showXAxis=False, showYAxis=False)
#else:
#   ch=drawXAxis(1,pad1,Xmin, Ymin, Xmax, Ymax,nameX,nameY,showXAxis=False, showYAxis=True)

ch=drawXAxis(1,pad1,Xmin, Ymin, Xmax, Ymax,nameX,nameY,showXAxis=True, showYAxis=True)

ax=ch.GetXaxis();
ay=ch.GetYaxis();
ay.SetTitleOffset(0.9)
ay.SetLabelSize(0.03)
ay.SetTitleSize(0.04)
ax.SetLabelSize(0.03)
ax.SetTitleSize(0.04)
gPad.SetTickx()
gPad.SetTicky()

ax.Draw("same")
if (NoLeft): ay.Draw("same")

# get bin width
xfile="../analysis/out/t0/sys0/data/data_2015_2016_2017_2018.root"
xfile=TFile( xfile )
#xfile.ls()
histo_name="LeadingJetPt"
systematics=0
hall, hjets=StandardModelPredictionJZ(systematics, trig_type, histo_name)

for h in hjets:
       print(h.GetTitle() ) 
       h.Draw("same pe ][") 

hall.Draw("same pe ][")

#hall.Print("All")


leg2=TLegend(0.7, 0.4, 0.9, 0.90);
leg2.SetBorderSize(0);
leg2.SetTextFont(42);
leg2.SetFillColor(10);
leg2.SetTextSize(0.04);
for h in hjets:
          leg2.AddEntry(h, h.GetTitle(),"pl")
leg2.Draw("same");


"""
if (Position =="1x1"):
  ATLASLabel6(0.19,0.89,0.15,0.07)
  myText(0.19,0.81,1,0.07,UsedData0)
  myTextIt(0.2,0.12,1,0.11,MLabel)
else:
  myTextIt(0.2,0.8,1,0.11,MLabel)
"""

#myText(0.75,0.4,1,0.04,"pp #sqrt{s}=13 TeV")
# myText(0.75,0.61,1,0.04,intLUMI)


gPad.RedrawAxis()
c1.Update()



if (myinput != "-b"):
              if (raw_input("Press any key to exit") != "-9999"): 
                         c1.Close(); sys.exit(1);


